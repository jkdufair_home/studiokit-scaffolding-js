# studiokit-scaffolding-js

**studiokit-scaffolding-js** provides a common scaffolding for react apps.

1. [Installation](#installation)
1. [Usage](#usage)

# Installation

## Install this library and call startup methods
1. `yarn add studiokit-scaffolding-js`
1. (Optional) Call configurable extensions in your main entry file
	* `redux/sagas/rootSaga`
		* `setOtherDependentSagas` to add more sagas
	* `redux/sagas/postLoginDataSaga`
		* `setOnPostLogin` to load more data after login
	* `redux/configureReducers`
		* `updatePersistBlacklist` and `setOtherReducers` to add more reducers
1. In your main entry file call `startup`
	```js
	import { startup, endpointMappings } from 'studiokit-scaffolding-js'

	const containerElementId = 'root'
	const appConfig = {/* AppConfiguration */}
	const endpointMappings = {...endpointMappings, .../* App EndpointMappings */}
	const { history, store, persistor } = startup(appConfig, endpointMappings, containerElementId)

	// render using history, store, persistor
	ReactDOM.render(
		...
	)
	```

# Usage

Components and utils can be imported from `studiokit-scaffolding-js/lib/...`
