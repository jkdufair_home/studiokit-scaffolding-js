export enum BASE_ACTIVITY {
	USER_ROLE_READ_ANY = 'UserRoleReadAny',
	USER_ROLE_MODIFY_ANY = 'UserRoleModifyAny',

	USER_READ_ANY = 'UserReadAny',
	USER_UPDATE_ANY = 'UserUpdateAny',

	GROUP_READ = 'GroupRead',
	GROUP_CREATE = 'GroupCreate',
	GROUP_UPDATE = 'GroupUpdate',
	GROUP_DELETE = 'GroupDelete',
	GROUP_USER_ROLE_READ = 'GroupUserRoleRead',
	GROUP_USER_ROLE_MODIFY = 'GroupUserRoleModify',
	EXTERNAL_GROUP_READ = 'ExternalGroupRead',
	EXTERNAL_GROUP_CONNECT_ANY = 'ExternalGroupConnectAny',
	EXTERNAL_GROUP_CONNECT_OWN = 'ExternalGroupConnectOwn',
	LTI_LAUNCH_READ_OWN = 'LtiLaunchReadOwn',
	LTI_LAUNCH_READ_ANY = 'LtiLaunchReadAny',
	GROUP_ROSTER_SYNC_ALL = 'GroupRosterSyncAll',
	GROUP_ROSTER_SYNC = 'GroupRosterSync',

	EXTERNAL_PROVIDER_MODIFY = 'ExternalProviderModify',
	CONFIGURATION_MODIFY = 'ConfigurationModify',
	IDENTITY_PROVIDER_MODIFY = 'IdentityProviderModify'
}

export default BASE_ACTIVITY
