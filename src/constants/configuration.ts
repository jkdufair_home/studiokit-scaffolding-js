import { EndpointMappings } from 'studiokit-net-js'
import { AppConfiguration, Configuration } from '../types'

let appConfig: AppConfiguration
let shardConfig: Configuration
let endpointMappings: EndpointMappings

export const setAppConfig = (value: AppConfiguration) => {
	appConfig = value
}

export const getAppConfig = () => {
	if (!appConfig) {
		throw new Error('`appConfig` is not defined')
	}
	return appConfig
}

export const setShardConfig = (value: Configuration) => {
	shardConfig = value
}

export const getShardConfig = () => {
	if (!shardConfig) {
		throw new Error('`shardConfig` is not defined')
	}
	return shardConfig
}

export const setEndpointMappings = (value: EndpointMappings) => {
	endpointMappings = value
}

export const getEndpointMappings = () => {
	if (!endpointMappings) {
		throw new Error('`endpointMappings` is not defined')
	}
	return endpointMappings
}
