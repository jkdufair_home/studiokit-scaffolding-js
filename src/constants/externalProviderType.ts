export enum EXTERNAL_PROVIDER_TYPE {
	UNITIME = 'UniTimeExternalProvider',
	LTI = 'LtiExternalProvider'
}

export default EXTERNAL_PROVIDER_TYPE
