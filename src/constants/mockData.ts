import _ from 'lodash'
import { ModelCollection } from 'studiokit-net-js'
import EXTERNAL_PROVIDER_TYPE from '../constants/externalProviderType'
import {
	AppConfiguration,
	ExternalProvider,
	ExternalTerm,
	Group,
	GroupUserRole,
	SimpleLocation,
	UserInfo,
	UserRole
} from '../types'
import ROLE from './role'
import TIER from './tier'

//#region General

export const defaultAppConfiguration: AppConfiguration = {
	NODE_ENV: 'test',
	TIER: TIER.LOCAL,
	API_BASE_URL: '',
	ROOT_DOMAIN: 'studiokit.org',
	CLIENT_ID: 'web',
	CLIENT_SECRET: '****',
	IS_DOWNTIME: false
}

export const defaultLocation: SimpleLocation = {
	hash: '',
	host: 'purdue.dev.studiokit.org',
	hostname: 'purdue.dev.studiokit.org',
	href: 'https://purdue.dev.studiokit.org/',
	pathname: '/',
	port: '',
	protocol: 'https:',
	search: ''
}

export function mockHistory(): History {
	return {
		length: 0,
		scrollRestoration: 'auto',
		state: {},
		back: jest.fn(),
		forward: jest.fn(),
		go: jest.fn(),
		pushState: jest.fn(),
		replaceState: jest.fn()
	}
}

//#endregion General

//#region Dates

/** 5/15/18 - 8/14/18 */
const pastDates = {
	startDate: '2018-05-15T04:00:00.000Z',
	endDate: '2018-08-15T03:59:59.999Z'
}

/** 8/15/18 - 12/31/18: */
const currentDates = {
	startDate: '2018-08-15T04:00:00.000Z',
	endDate: '2019-01-01T04:59:59.999Z'
}

/**
 * The default date and time for all tests. Used by the 'mockdate' package. One month into `currentDates`.
 */
export const defaultDate = '2018-09-15T04:00:00.000Z'

//#endregion Dates

//#region Users

const defaultUser = {
	userInfo: {
		id: '1',
		activities: [],
		roles: [],
		dateDemoGenerationSucceeded: null
	} as UserInfo
}

export const defaultNamelessUserInfo = {
	...defaultUser.userInfo,
	...{
		email: 'nicki@minaj.superbass',
		uid: 'nm-nameless'
	}
}

export const defaultNamedUserInfo = {
	...defaultUser.userInfo,
	...{
		firstName: 'Nicki',
		lastName: 'Minaj',
		uid: 'nm-named'
	}
}

export function mockUser(activities: string[] = []): { userInfo: UserInfo } {
	return _.merge({ userInfo: { activities } }, defaultUser)
}

export const defaultGroupOwner: GroupUserRole = {
	id: '2',
	firstName: 'Jane',
	lastName: 'Doe',
	email: 'jane@gmail.com',
	roles: [ROLE.GROUP_OWNER],
	isExternal: false,
	groupId: 1
}

export const defaultNamelessGroupLearner: GroupUserRole = {
	id: '3',
	email: 'nicki@minaj.superbass',
	isExternal: false,
	uid: 'nm-nameless',
	roles: [ROLE.GROUP_LEARNER],
	groupId: 1
}

export const defaultNamedGroupLearner: GroupUserRole = {
	id: '4',
	firstName: 'Nicki',
	lastName: 'Minaj',
	isExternal: false,
	uid: 'nm-named',
	roles: [ROLE.GROUP_LEARNER],
	groupId: 1
}

export const defaultGroupLearner: GroupUserRole = {
	id: '3',
	firstName: 'Joe',
	lastName: 'Schmoe',
	email: 'nope@gmail.com',
	roles: [ROLE.GROUP_LEARNER],
	isExternal: false,
	groupId: 1
}

export const defaultExternalGroupLearner: GroupUserRole = {
	id: '4',
	firstName: 'Bob',
	lastName: 'Loblaw',
	email: 'bobloblaw@lawblog.com',
	roles: [ROLE.GROUP_LEARNER],
	isExternal: true,
	groupId: 1
}

export const defaultGroupUsers = [defaultGroupOwner, defaultGroupLearner, defaultExternalGroupLearner]

export const defaultAdmins: UserRole[] = [
	{
		id: '1',
		firstName: 'Joe',
		lastName: 'Schmo',
		roles: [ROLE.ADMIN]
	},
	{
		id: '2',
		email: 'carlysimon@clouds.in.coffee',
		roles: [ROLE.ADMIN]
	}
]

//#endregion Users

//#region Groups

export const defaultGroup: Group = {
	roles: [],
	activities: [],
	owners: [
		{
			id: '1',
			groupId: 1,
			firstName: 'Instructor',
			lastName: 'McInstructor',
			isExternal: false,
			roles: [ROLE.GROUP_OWNER]
		}
	],
	id: 1,
	name: 'McGroup',
	externalTermId: null,
	startDate: currentDates.startDate,
	endDate: currentDates.endDate,
	isDeleted: false,
	isRosterSyncEnabled: false,
	externalGroups: []
}

export function mockGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	return { '1': _.merge({ activities, roles }, defaultGroup) }
}

export function mockSyncedGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const rosterSyncEnabledGroup: Group = _.merge({ activities, roles }, defaultGroup)
	rosterSyncEnabledGroup.isRosterSyncEnabled = true
	return { '1': rosterSyncEnabledGroup }
}

export function mockLtiGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const ltiGroup: Group = _.merge({ activities, roles }, defaultGroup)
	ltiGroup.isRosterSyncEnabled = true
	ltiGroup.externalGroups = [
		{
			id: 1,
			externalProviderId: 2,
			groupId: 1,
			userId: '1',
			name: 'LTI ExternalGroup',
			description: 'LTI 101',
			gradesUrl: 'www.lti.org',
			isAutoGradePushEnabled: false
		}
	]
	return { '1': ltiGroup }
}

export function mockTermGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const group: Group = _.merge({ activities, roles }, defaultGroup)
	group.startDate = undefined
	group.endDate = undefined
	group.externalTermId = 1
	return { '1': group }
}

export function mockTermAndUniTimeGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const group: Group = _.merge({ activities, roles }, defaultGroup)
	group.startDate = undefined
	group.endDate = undefined
	group.externalTermId = 1
	group.externalGroups = [
		{
			id: 1,
			externalProviderId: 1,
			groupId: 1,
			userId: '1',
			name: 'UniTime ExternalGroup',
			description: 'UniTime 101',
			isAutoGradePushEnabled: false
		}
	]
	return { '1': group }
}

export function mockPastGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const pastGroup: Group = _.merge({ activities, roles }, defaultGroup, pastDates)
	return { '1': pastGroup }
}

export function mockPastAndCurrentGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const pastGroup: Group = _.merge({ activities, roles }, defaultGroup, pastDates)
	const currentGroup: Group = _.merge(
		{ activities, roles },
		defaultGroup,
		{
			id: '2'
		},
		currentDates
	)
	return { '1': pastGroup, '2': currentGroup }
}

//#endregion Groups

//#region ExternalTerms + ExternalProviders

export const defaultExternalProviders: ModelCollection<ExternalProvider> = {
	'1': {
		id: 1,
		typename: EXTERNAL_PROVIDER_TYPE.UNITIME,
		termSyncEnabled: true,
		rosterSyncEnabled: true,
		gradePushEnabled: false
	},
	'2': {
		id: 2,
		typename: EXTERNAL_PROVIDER_TYPE.LTI,
		termSyncEnabled: false,
		rosterSyncEnabled: false,
		gradePushEnabled: true
	}
}

export const defaultExternalTerms: ModelCollection<ExternalTerm> = {
	'1': {
		id: 1,
		externalProviderId: 1,
		externalId: 'foo',
		name: 'UniTime Fall Term',
		startDate: currentDates.startDate,
		endDate: currentDates.endDate
	}
}

//#endregion ExternalTerms + ExternalProviders
