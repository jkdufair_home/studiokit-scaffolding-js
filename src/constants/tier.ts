export enum TIER {
	LOCAL = 'local',
	DEV = 'dev',
	PROD = 'prod'
}

export default TIER
