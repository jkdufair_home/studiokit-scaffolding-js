export enum SHARD {
	PURDUE = 'purdue',
	DEMO = 'demo',
	ROOT = 'root-shard'
}

export default SHARD
