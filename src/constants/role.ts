export enum ROLE {
	SUPER_ADMIN = 'SuperAdmin',
	ADMIN = 'Admin',
	CREATOR = 'Creator',
	GROUP_OWNER = 'GroupOwner',
	GROUP_LEARNER = 'GroupLearner',
	GROUP_GRADER = 'GroupGrader'
}

export default ROLE
