import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
// import MockDate from 'mockdate'
// import { defaultDate } from './constants/mockData'

jest.mock('./redux/actionCreator')

jest.mock('./utils/timezone', () => {
	return {
		getDefaultTimeZone: () => 'America/Indianapolis',
		guessTimeZoneId: () => 'America/Indianapolis'
	}
})

// globally mock the date
// beforeEach(() => {
// 	console.log('test')
// 	MockDate.set(defaultDate)
// })
// afterEach(() => {
// 	MockDate.reset()
// })

configure({ adapter: new Adapter() })
