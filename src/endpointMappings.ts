import { EndpointMapping, EndpointMappings } from 'studiokit-net-js'

export const entityUserRoleEndpointMappings = (typename: string): EndpointMappings => {
	return {
		entityUserRoles: {
			_config: {
				isCollection: true,
				fetch: {
					path: '/api/entityUserRoles',
					// for GET
					queryParams: {
						typename
					},
					// for DELETE
					body: {
						typename
					}
				}
			}
		},
		swapUserRoles: {
			_config: {
				fetch: {
					path: '/api/entityUserRoles/swapUserRoles',
					method: 'POST',
					body: {
						typename
					}
				}
			}
		},
		// use separate model for transient POSTs
		addUserRoles: {
			_config: {
				fetch: {
					path: '/api/entityUserRoles',
					method: 'POST',
					body: {
						typename
					}
				}
			}
		}
	}
}

export const groupEndpointMapping: EndpointMapping = {
	_config: {
		isCollection: true,
		fetch: {
			path: '/api/groups'
		}
	},
	syncRoster: {
		_config: {
			fetch: {
				method: 'POST'
			}
		}
	},
	...entityUserRoleEndpointMappings('Group')
}

export const endpointMappings: EndpointMappings = {
	urlChecker: {
		_config: {
			fetch: {
				path: '/api/url/headers'
			}
		}
	},
	caliperToken: {
		_config: {
			fetch: {
				path: '/api/caliper-eventstore/token'
			}
		}
	},
	configuration: {
		_config: {
			fetch: {
				path: '/api/configuration'
			}
		}
	},
	client: {
		_config: {
			fetch: {
				path: '/api/appClients/web'
			}
		}
	},
	identityProviders: {
		_config: {
			isCollection: true
		}
	},
	externalTerms: {
		_config: {
			isCollection: true
		}
	},
	externalProviders: {
		_config: {
			isCollection: true
		}
	},
	userRoles: {
		_config: {
			isCollection: true
		}
	},
	// use separate model for transient POSTs
	addUserRoles: {
		_config: {
			fetch: {
				path: '/api/userRoles',
				method: 'POST'
			}
		}
	},
	uniTimeInstructorSchedule: {
		_config: {
			fetch: {
				path: '/api/uniTime/instructorSchedule'
			}
		}
	},
	ltiLaunches: {
		_config: {
			isCollection: true
		}
	},
	search: {
		_config: {
			fetch: {
				path: ''
			}
		},
		users: {
			_config: {
				isCollection: true,
				fetch: {
					path: '/api/users'
				}
			}
		}
	}
}
