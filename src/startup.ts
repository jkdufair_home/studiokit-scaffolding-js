import * as Sentry from '@sentry/browser'
import { AppInsights } from 'applicationinsights-js'
import { Action, Location, LocationListener } from 'history'
import { createBrowserHistory } from 'history'
import ga from 'react-ga'
import { EndpointMappings } from 'studiokit-net-js'
import { setAppConfig, setEndpointMappings } from './constants/configuration'
import configureStore from './redux/configureStore'
import { AppConfiguration, BaseReduxState } from './types'

/**
 * The main startup method for your app. Should be called before rendering your app or importing other scaffolding modules.
 *
 * Optional startup methods are available to provide additional configuration. Call these **before** `startup`. These methods can all be imported from `studiokit-scaffolding-js`.
 * * `redux/sagas/rootSaga`
 *   * `setOtherDependentSagas` to add more sagas
 * * `redux/sagas/postLoginDataSaga`
 *   * `setOnPostLogin` to load more data after login
 * * `redux/configureReducers`
 *   * `updatePersistBlacklist` and `setOtherReducers` to add more reducers
 *
 * @template TReduxState The type of state to be stored in redux
 *
 * @param appConfig The static configuration for the application.
 * @param endpointMappings The mapping of redux models to api endpoints.
 * @param containerElementId The #id of the app container, used for hash link scrolling.
 *
 * @returns An object containing the created `history`, `store`, and `persistor` objects.
 */
export const startup = <TReduxState extends BaseReduxState>(
	appConfig: AppConfiguration,
	endpointMappings: EndpointMappings,
	containerElementId: string
) => {
	setAppConfig(appConfig)
	setEndpointMappings(endpointMappings)

	// Errors
	window.onunhandledrejection = evt => {
		Sentry.captureException(evt.reason)
	}

	// Google Analytics
	if (appConfig.GOOGLE_ANALYTICS_TRACKING_ID) {
		ga.initialize(appConfig.GOOGLE_ANALYTICS_TRACKING_ID)
		// track initial page
		ga.pageview(window.location.pathname)
	}

	// Application Insights
	if (appConfig.APP_INSIGHTS_KEY && AppInsights.downloadAndSetup) {
		AppInsights.downloadAndSetup({
			instrumentationKey: appConfig.APP_INSIGHTS_KEY
		})
	}

	// History and Listener
	const history = createBrowserHistory()
	const locationListener: LocationListener = (location: Location, action: Action) => {
		// send pageview to Google Analytics
		if (appConfig.GOOGLE_ANALYTICS_TRACKING_ID) {
			ga.pageview(location.pathname)
		}
		// send pageview to Application Insights
		if (appConfig.APP_INSIGHTS_KEY) {
			AppInsights.trackPageView(location.pathname)
		}
		// handle anchor link scrolling
		if (action === 'PUSH') {
			const hash = location.hash.substring(1) || ''
			const element =
				hash.length > 0
					? document.querySelectorAll(`[name=${hash}]`)[0]
					: document.getElementById(containerElementId)
			if (element) {
				element.scrollIntoView()
			}
		}
	}
	history.listen(locationListener)

	// Redux Store and Persistor
	const { store, persistor } = configureStore<TReduxState>(history)

	return { history, store, persistor }
}
