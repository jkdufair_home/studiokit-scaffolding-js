import { RouterState } from 'connected-react-router'
import { PersistPartial } from 'redux-persist'
import { AuthState } from 'studiokit-auth-js'
import { ModelCollection, ModelsState } from 'studiokit-net-js'
import { ModalsState } from '../redux/reducers/modalsReducer'
import { NotificationState } from '../redux/reducers/notificationsReducer'
import { SearchState } from '../redux/reducers/searchReducer'
import { Client } from './Client'
import { Configuration } from './Configuration'
import { ExternalProvider, ExternalTerm } from './External'
import { Group } from './Group'
import { IdentityProvider } from './IdentityProvider'
import { LtiLaunch } from './LtiLaunch'
import { User, UserInfo } from './User'
import { UserRole } from './UserRole'

/**
 * Extends the base object in redux at `state.models`, with all scaffolding related collections and models.
 *
 * @template TGroup The type of group stored in `state.models.group`. Defaults to `Group`.
 */
export interface BaseModelsState<TGroup extends Group = Group> extends ModelsState {
	configuration?: Configuration
	client?: Client
	identityProviders?: ModelCollection<IdentityProvider>
	externalTerms?: ModelCollection<ExternalTerm>
	externalProviders?: ModelCollection<ExternalProvider>
	groups?: ModelCollection<TGroup>
	userRoles?: ModelCollection<UserRole>
	ltiLaunches?: ModelCollection<LtiLaunch>
	search?: {
		users?: ModelCollection<User>
	}
	user?: {
		userInfo?: UserInfo
	}
}

/**
 * Defines the base redux state of the application.
 *
 * @template TModels The type of state to be stored at `state.models`, allowing for extension.
 */
export interface BaseReduxState<TModels extends BaseModelsState = BaseModelsState> {
	auth: AuthState & PersistPartial
	notifications: NotificationState & PersistPartial
	modals: ModalsState
	models: TModels
	router: RouterState
	search: SearchState
}
