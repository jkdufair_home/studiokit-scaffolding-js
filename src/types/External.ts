import { Model } from 'studiokit-net-js'
import EXTERNAL_PROVIDER_TYPE from '../constants/externalProviderType'

export interface ExternalProvider extends Model {
	id: number
	typename: EXTERNAL_PROVIDER_TYPE
	termSyncEnabled: boolean
	rosterSyncEnabled: boolean
	gradePushEnabled: boolean
}

export interface ExternalTerm extends Model {
	id: number
	externalProviderId: number
	externalId: string
	name: string
	startDate: string
	endDate: string
}

export interface ExternalGroup extends Model {
	id: number
	groupId: number
	externalProviderId: number
	userId: string
	name: string
	description: string
	rosterUrl?: string
	gradesUrl?: string
	isAutoGradePushEnabled: boolean
}
