import { Model } from 'studiokit-net-js'

export interface Client extends Model {
	clientId: string
	currentVersion?: string
	minVersion?: string
}
