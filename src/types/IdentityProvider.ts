import { Model } from 'studiokit-net-js'

export interface IdentityProvider extends Model {
	id: number
	name: string
	loginUrl: string
	logoutUrl: string
	mobileLoginUrl?: string
	text?: string
	/** Base64 encoded image string */
	image?: string
}
