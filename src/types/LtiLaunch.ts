import { Model } from 'studiokit-net-js'

export interface LtiLaunch extends Model {
	id: number
	typename: string
	userId: string
	externalProviderId: number
	externalId: string
	name: string
	description?: string
	roles?: string
	rosterUrl?: string
	gradesUrl?: string
	ltiVersion?: string
	returnUrl?: string
	isAutoGradePushEnabled?: boolean
}
