export interface Search {
	keywords?: string
	invalidKeywords?: boolean
	hasSearched?: boolean
	dateString?: string
	date?: Date
	queryAll?: boolean
	requiredMessage?: string | null
	selectedTab?: number
}
