import { Model } from 'studiokit-net-js'
import { ExternalGroup } from './External'
import { GroupUserRole } from './UserRole'

export interface Group extends Model {
	id: number
	name: string
	externalTermId?: number | null
	startDate?: string
	endDate?: string
	isDeleted: boolean
	isRosterSyncEnabled: boolean
	roles: string[]
	activities: string[]
	owners: GroupUserRole[]
	externalGroups: ExternalGroup[]
}
