import TIER from '../constants/tier'

export interface AppConfiguration {
	NODE_ENV: 'development' | 'production' | 'test'

	// App Version + Name
	VERSION?: string
	VERSION_NAME?: string

	// Tier and URLs
	TIER: TIER
	API_BASE_URL: string
	ROOT_DOMAIN: string
	LOCALHOST_SHARD_KEY?: string
	/** Custom Storage URL for local development against vm */
	STORAGE_URL?: string

	// Caliper
	CALIPER_EXTENSIONS_NAMESPACE?: string
	CALIPER_SENSOR_ID?: string
	CALIPER_SENSOR_APP_ID?: string
	CALIPER_SENSOR_APP_NAME?: string
	CALIPER_EVENT_STORE_OPTIONS?: {
		protocol: string
		port: string
		path: string
		method: string
	}

	// Google Analytics
	GOOGLE_ANALYTICS_TRACKING_ID?: string

	// Sentry
	SENTRY_DSN?: string

	// Application Insights
	APP_INSIGHTS_KEY?: string

	// OAuth
	CLIENT_ID: string
	CLIENT_SECRET: string

	// Downtime
	IS_DOWNTIME: boolean
}
