import { Model } from 'studiokit-net-js'
/**
 * Simple User object for with general data for displaying users
 */
export interface User extends Model {
	/**
	 * App Id of User
	 */
	id: string
	/**
	 * User's given name
	 */
	firstName?: string
	/**
	 * User's family name
	 */
	lastName?: string
	/**
	 * User's email
	 */
	email?: string
	/**
	 * External email with domain removed. Equivalent to careerAccountAlias for Purdue
	 */
	uid?: string
}

/**
 * More detailed user object from UserInfo api
 */
export interface UserInfo extends User {
	/**
	 * Equivalent to email
	 */
	userName?: string
	/**
	 * Purdue user's career account alias
	 */
	careerAccountAlias?: string
	/**
	 * External User Id. Equivalent to puid for Purdue
	 */
	employeeNumber?: string
	/**
	 * User's Purdue Id
	 */
	puid?: string
	/**
	 * Array of global roles for the User
	 */
	roles: string[]
	/**
	 * Array of global activities for the User
	 */
	activities: string[]
	/**
	 * Date string when user's demo started. Null when not on demo shard
	 */
	dateDemoGenerationSucceeded: string | null
}
