import { User } from './User'

export interface UserRole extends User {
	/**
	 * Array of all the roles the user has
	 */
	roles: string[]
}

export interface GroupUserRole extends UserRole {
	groupId: number
	isExternal: boolean
}

export interface RoleDescription {
	[role: string]: React.ReactNode
}

export interface UserWithDefaultRole extends UserRole {
	/**
	 * The currently displayed role
	 */
	defaultRole: string
	/**
	 * This is used to display to the user what the role is
	 */
	defaultRoleText: string
}
