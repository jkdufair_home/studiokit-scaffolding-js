import { Model } from 'studiokit-net-js'

export interface Configuration extends Model {
	id: number
	name: string
	userSupportEmail: string
	allowedDomains: string
	caliperEnabled: boolean
	caliperEventStoreHostname?: string
	caliperPersonNamespace?: string
	defaultTimeZoneId?: string
	isInstructorSandboxEnabled: boolean
}
