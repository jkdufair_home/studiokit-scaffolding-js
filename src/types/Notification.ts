import { NOTIFICATION_TYPE } from '../constants'

export interface Notification {
	id: string
	text: string
	type: NOTIFICATION_TYPE
}
