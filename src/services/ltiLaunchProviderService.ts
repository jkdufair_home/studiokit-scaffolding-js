import _ from 'lodash'
import { parse, stringify } from 'query-string'
import windowService from './windowService'

export const getResponse = () => {
	const location = windowService.getLocation()
	const parsedSearch = parse(location.search)
	const ltiLaunchId = parsedSearch.ltiLaunchId
	const groupId = parsedSearch.groupId
	if (_.isNil(ltiLaunchId) && _.isNil(groupId)) {
		return null
	}
	return {
		ltiLaunchId,
		groupId
	}
}

export const removeResponse = () => {
	const location = windowService.getLocation()
	const history = windowService.getHistory()
	const parsedSearch = parse(location.search)
	const { groupId, ltiLaunchId, ...newSearch } = parsedSearch
	let newSearchString = stringify(newSearch)
	if (newSearchString) {
		newSearchString = `?${newSearchString}`
	}
	history.pushState('', '', `${location.protocol}//${location.host}${location.pathname}${newSearchString}`)
}

export const getRedirectUrl = () => {
	const response = getResponse()
	if (_.isNil(response)) {
		return null
	}
	removeResponse()
	return !_.isNil(response.ltiLaunchId) ? `/lti-launch/${response.ltiLaunchId}` : `/courses/${response.groupId}`
}

export default {
	getResponse,
	removeResponse,
	getRedirectUrl
}
