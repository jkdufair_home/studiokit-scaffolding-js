import { defaultLocation, mockHistory } from '../constants/mockData'
import codeProviderService from './codeProviderService'
import locationProviderService from './windowService'

describe('codeProviderService', () => {
	describe('getCode', () => {
		it('returns code if exists', () => {
			locationProviderService.setLocation({ ...defaultLocation, ...{ search: '?code=foo' } })
			expect(codeProviderService.getCode()).toEqual('foo')
		})
		it('returns nothing if not exists', () => {
			locationProviderService.setLocation(defaultLocation)
			expect(codeProviderService.getCode()).toEqual(undefined)
		})
	})
	describe('removeCode', () => {
		it('removes code from search and calls pushState with new url', () => {
			locationProviderService.setLocation({ ...defaultLocation, ...{ search: '?code=foo', pathname: '/home' } })
			const history = mockHistory()
			locationProviderService.setHistory(history)
			codeProviderService.removeCode()
			expect(history.pushState).toHaveBeenCalledWith('', '', `${defaultLocation.href}home`)
		})
	})
})
