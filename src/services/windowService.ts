import { SimpleLocation } from '../types'

export interface WindowService {
	setLocation: (value?: SimpleLocation) => void
	getLocation: () => SimpleLocation | Location
	setHistory: (value?: History) => void
	getHistory: () => History
}

let location: SimpleLocation | undefined
let history: History | undefined

const windowService: WindowService = {
	setLocation: (value?: SimpleLocation) => {
		location = value
	},
	getLocation: () => location || window.location,
	setHistory: (value?: History) => {
		history = value
	},
	getHistory: () => history || window.history
}

export default windowService
