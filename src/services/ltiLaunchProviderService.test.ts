import { defaultLocation, mockHistory } from '../constants/mockData'
import { getRedirectUrl, getResponse, removeResponse } from './ltiLaunchProviderService'
import windowService from './windowService'

let history: History
beforeEach(() => {
	history = mockHistory()
	windowService.setHistory(history)
})
afterEach(() => {
	windowService.setHistory()
})

describe('getResponse', () => {
	test('should return `null` if no params', () => {
		const response = getResponse()
		expect(response).toEqual(null)
	})

	test('should return `ltiLaunchId`', () => {
		windowService.setLocation({ ...defaultLocation, ...{ search: '?ltiLaunchId=202' } })
		const response = getResponse()
		expect(response).toEqual({
			ltiLaunchId: '202',
			groupId: undefined
		})
	})

	test('should return `groupId`', () => {
		windowService.setLocation({ ...defaultLocation, ...{ search: '?groupId=101' } })
		const response = getResponse()
		expect(response).toEqual({
			ltiLaunchId: undefined,
			groupId: '101'
		})
	})
})

describe('removeResponse', () => {
	test('should remove `groupId` and `ltiLaunchId`', () => {
		windowService.setLocation({ ...defaultLocation, ...{ search: '?groupId=101&ltiLaunchId=202' } })
		removeResponse()
		expect(history.pushState).toHaveBeenCalledWith('', '', defaultLocation.href)
	})

	test('should preserve other search params', () => {
		windowService.setLocation({ ...defaultLocation, ...{ search: '?groupId=101&ltiLaunchId=202&code=something' } })
		removeResponse()
		expect(history.pushState).toHaveBeenCalledWith('', '', `${defaultLocation.href}?code=something`)
	})

	test('should not fail if no search', () => {
		windowService.setLocation(defaultLocation)
		removeResponse()
		expect(history.pushState).toHaveBeenCalledWith('', '', defaultLocation.href)
	})
})

describe('getRedirectUrl', () => {
	test('should return `null` if no params', () => {
		windowService.setLocation(defaultLocation)
		const response = getRedirectUrl()
		expect(response).toEqual(null)
	})

	test('should return url for `ltiLaunchId`', () => {
		windowService.setLocation({ ...defaultLocation, ...{ search: '?ltiLaunchId=202' } })
		const response = getRedirectUrl()
		expect(response).toEqual('/lti-launch/202')
	})

	test('should return url for `groupId`', () => {
		windowService.setLocation({ ...defaultLocation, ...{ search: '?groupId=101' } })
		const response = getRedirectUrl()
		expect(response).toEqual('/courses/101')
	})
})
