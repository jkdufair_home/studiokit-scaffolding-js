import { parse, stringify } from 'query-string'
import { CodeProviderService } from 'studiokit-auth-js'
import windowService from './windowService'

const codeProviderService: CodeProviderService = {
	getCode: () => {
		const location = windowService.getLocation()
		return parse(location.search).code as string
	},
	removeCode: () => {
		const location = windowService.getLocation()
		const history = windowService.getHistory()
		const parsedSearch = parse(location.search)
		const { code, ...newSearch } = parsedSearch
		let newSearchString = stringify(newSearch)
		if (newSearchString) {
			newSearchString = `?${newSearchString}`
		}
		history.pushState('', '', `${location.protocol}//${location.host}${location.pathname}${newSearchString}`)
	}
}

export default codeProviderService
