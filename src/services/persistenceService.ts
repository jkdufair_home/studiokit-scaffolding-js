import storage from 'local-storage-fallback'
import { TokenPersistenceService } from 'studiokit-auth-js'
import { OAuthTokenResponse } from 'studiokit-net-js'

const tokenKey = '@authStore:oauthToken'
const caliperTokenKey = 'studioKit:caliperService:token'
export const returnUrlKey = 'studioKit:login:returnUrl'
export const identityProviderKey = 'studioKit:auth:identityProvider'
export const isCasGatewayRequestedKey = 'studioKit:auth:isCasGatewayRequested'

const getItem = async (key: string) => {
	const itemValue = await storage.getItem(key)
	if (!itemValue) {
		return null
	}
	return JSON.parse(itemValue)
}

const setItem = async (key: string, item: any) => {
	await storage.setItem(key, JSON.stringify(item))
}

const removeItem = async (key: string) => {
	return await storage.removeItem(key)
}

const getPersistedToken = async () => {
	const token: OAuthTokenResponse = await getItem(tokenKey)
	return token
}

const persistToken = async (token: OAuthTokenResponse) => {
	await setItem(tokenKey, token)
}

const getPersistedCaliperToken = async () => {
	const token: OAuthTokenResponse = await getItem(caliperTokenKey)
	return token
}

export interface PersistenceService extends TokenPersistenceService {
	getItem: (key: string) => any | Promise<any>
	setItem: (key: string, item: any) => void | Promise<void>
	removeItem: (key: string) => void | Promise<void>
	getPersistedCaliperToken: () => OAuthTokenResponse | Promise<OAuthTokenResponse>
}

const service: PersistenceService = {
	getItem,
	setItem,
	removeItem,
	getPersistedToken,
	persistToken,
	getPersistedCaliperToken
}

export default service
