import { defaultLocation, mockHistory } from '../constants/mockData'
import ticketProviderService from './ticketProviderService'
import locationProviderService from './windowService'

describe('ticketProviderService', () => {
	describe('getCode', () => {
		it('returns code if exists', () => {
			locationProviderService.setLocation({ ...defaultLocation, ...{ search: '?ticket=foo' } })
			expect(ticketProviderService.getTicket()).toEqual('foo')
		})
		it('returns nothing if not exists', () => {
			locationProviderService.setLocation(defaultLocation)
			expect(ticketProviderService.getTicket()).toEqual(undefined)
		})
	})
	describe('removeCode', () => {
		it('removes code from search and calls pushState with new url', () => {
			locationProviderService.setLocation({ ...defaultLocation, ...{ search: '?ticket=foo', pathname: '/home' } })
			const history = mockHistory()
			locationProviderService.setHistory(history)
			ticketProviderService.removeTicket()
			expect(history.pushState).toHaveBeenCalledWith('', '', `${defaultLocation.href}home`)
		})
	})
})
