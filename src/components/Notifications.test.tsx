import { shallow } from 'enzyme'
import React from 'react'
import { NOTIFICATION_TYPE } from '../constants'
import { ACTION } from '../redux/actions'
import { Notification } from '../types'
import { mapStateToProps, Notifications, NotificationsProps } from './Notifications'

const mockDispatchAction = jest.fn()

jest.mock('../redux/actionCreator', () => {
	return {
		dispatchAction: (...args: any) => {
			mockDispatchAction(...args)
		}
	}
})

const setup = (notifications: Notification[] = []) => {
	const mockState = {
		notifications: {
			queue: notifications
		},
		modals: {}
	}
	const props = mapStateToProps(mockState)
	return shallow<Notifications, NotificationsProps>(<Notifications {...props} />)
}

const successNotification: Notification = {
	id: '1',
	text: 'you did it! :)',
	type: NOTIFICATION_TYPE.SUCCESS
}
const infoNotification: Notification = {
	id: '2',
	text: 'you could do it :|',
	type: NOTIFICATION_TYPE.INFO
}
const warningNotification: Notification = {
	id: '3',
	text: 'you might not be able to do it :/',
	type: NOTIFICATION_TYPE.WARNING
}
const errorNotification: Notification = {
	id: '4',
	text: 'you didn’t do it :(',
	type: NOTIFICATION_TYPE.ERROR
}

describe('Notifications', () => {
	it('should render closed with no notifications', () => {
		const wrapper = setup()
		expect(wrapper.find('#notificationAlert').prop('open')).toEqual(false)
	})
	it('should render open with notification', () => {
		const wrapper = setup([successNotification])
		expect(wrapper.state().notification).toEqual(successNotification)
		expect(wrapper.find('#notificationAlert').prop('open')).toEqual(true)
	})
	it('should still target the first notification when another is added', () => {
		const wrapper = setup([infoNotification])
		expect(wrapper.state().notification).toEqual(infoNotification)
		wrapper.setProps({
			notifications: [infoNotification, warningNotification]
		})
		expect(wrapper.instance().props.notifications.length).toEqual(2)
		expect(wrapper.state().notification).toEqual(infoNotification)
	})
	it('should render closed after removing a notification', () => {
		jest.useFakeTimers()
		const wrapper = setup([errorNotification])
		expect(wrapper.state().notification).toEqual(errorNotification)
		expect(wrapper.find('#notificationAlert').prop('open')).toEqual(true)
		// remove the notification
		wrapper.setProps({
			notifications: []
		})
		jest.runOnlyPendingTimers()
		expect(wrapper.instance().props.notifications.length).toEqual(0)
		expect(wrapper.state().notification).toBeUndefined()
		expect(wrapper.find('#notificationAlert').prop('open')).toEqual(false)
	})
	describe('removeNotification', () => {
		it('should not dispatch action if no notification', () => {
			const wrapper = setup()
			wrapper.instance().removeNotification()
			expect(mockDispatchAction).toHaveBeenCalledTimes(0)
		})
		it('should dispatch REMOVE_NOTIFICATION action', () => {
			const wrapper = setup([warningNotification])
			expect(wrapper.state().notification).toEqual(warningNotification)
			wrapper.instance().removeNotification()
			expect(mockDispatchAction).toHaveBeenCalledWith(ACTION.REMOVE_NOTIFICATION, {
				notification: warningNotification
			})
		})
		it('should remove notifications when unmounting', () => {
			const wrapper = setup([warningNotification])
			const spy = jest.spyOn(wrapper.instance(), 'removeNotification')
			wrapper.instance().componentWillUnmount()
			expect(spy).toHaveBeenCalledTimes(1)
		})
	})
})
