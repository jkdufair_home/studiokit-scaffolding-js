import { shallow } from 'enzyme'
import React from 'react'
import SearchControls, { SearchControlsProps } from './SearchControls'

function setup(props: Partial<SearchControlsProps>) {
	return shallow(<SearchControls {...props as SearchControlsProps} />)
}

describe('SearchControls', () => {
	describe('Required Message Alert', () => {
		it('is visible with text if requiredMessage exists', () => {
			const wrapper = setup({
				requiredMessage: 'You missed something, dummy'
			})
			const alert = wrapper.find({ id: 'requiredMessageAlert' })
			expect(alert.length).toEqual(1)
			expect(alert.dive().text()).toEqual('You missed something, dummy')
		})
		it('is invisible if requiredMessage does not exists', () => {
			const wrapper = setup({})
			expect(wrapper.find({ id: 'requiredMessageAlert' }).length).toEqual(0)
		})
	})
	describe('Invalid Keywords Message', () => {
		it('is visible if invalidKeywords = true', () => {
			const wrapper = setup({
				invalidKeywords: true
			})
			expect(wrapper.find({ id: 'invalidKeywords' }).length).toEqual(1)
		})
		it('is invisible if invalidKeywords = false', () => {
			const wrapper = setup({})
			expect(wrapper.find({ id: 'invalidKeywords' }).length).toEqual(0)
		})
	})
	describe('Date Field', () => {
		it('is visible if handleDateChange exists', () => {
			const wrapper = setup({
				handleDateChange: jest.fn()
			})
			expect(wrapper.find({ id: 'date' }).length).toEqual(1)
		})
		it('is invisible if handleDateChange does not exists', () => {
			const wrapper = setup({})
			expect(wrapper.find({ id: 'date' }).length).toEqual(0)
		})
	})
	describe('Search All Checkbox', () => {
		it('is visible if canReadAnyGlobally = true and handleQueryAllChange exists', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				handleQueryAllChange: jest.fn()
			})
			expect(wrapper.find({ id: 'queryAll' }).length).toEqual(1)
		})
		it('is invisible if canReadAnyGlobally = false and handleQueryAllChange exists', () => {
			const wrapper = setup({
				handleQueryAllChange: jest.fn()
			})
			expect(wrapper.find({ id: 'queryAll' }).length).toEqual(0)
		})
		it('is invisible if canReadAnyGlobally = true and handleQueryAllChange does not exists', () => {
			const wrapper = setup({
				canReadAnyGlobally: true
			})
			expect(wrapper.find({ id: 'queryAll' }).length).toEqual(0)
		})
		it('is readOnly if canReadSome = false', () => {
			const wrapper = setup({
				handleQueryAllChange: jest.fn(),
				canReadAnyGlobally: true,
				canReadSome: false
			})
			expect(wrapper.find({ id: 'queryAll' }).props().readOnly).toEqual(true)
		})
		it('is not readOnly if canReadSome = true', () => {
			const wrapper = setup({
				handleQueryAllChange: jest.fn(),
				canReadAnyGlobally: true,
				canReadSome: true
			})
			expect(wrapper.find({ id: 'queryAll' }).props().readOnly).toEqual(false)
		})
	})
})
