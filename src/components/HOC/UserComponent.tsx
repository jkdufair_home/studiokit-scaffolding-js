import { ComponentType } from 'react'
import { hasUserData } from '../../redux/helpers'
import AuthenticatedComponent from './AuthenticatedComponent'
import DataDependentComponent from './DataDependentComponent'

const UserComponent = <TOwnProps extends {}>(WrappedComponent: ComponentType<TOwnProps>) =>
	AuthenticatedComponent(DataDependentComponent(WrappedComponent, hasUserData))

export default UserComponent
