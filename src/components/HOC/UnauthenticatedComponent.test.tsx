import { shallow } from 'enzyme'
import React from 'react'
import { BaseReduxState } from '../../types'
import { configureUnauthenticatedComponent, mapStateToProps } from './UnauthenticatedComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (isAuthenticated: boolean, isAuthenticating: boolean) => {
	const UnauthenticatedTestComponent = configureUnauthenticatedComponent(TestComponent)
	const state = {
		auth: {
			isAuthenticated,
			isAuthenticating
		}
	} as BaseReduxState
	const ownProps = {
		foo: 'bar'
	}
	const props = {
		...ownProps,
		...mapStateToProps(state)
	}
	return shallow(<UnauthenticatedTestComponent {...props} />)
}

describe('UnauthenticatedComponent', () => {
	it('should render TestComponent if isAuthenticated and isAuthenticating are both false', () => {
		const wrapper = setup(false, false)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render Redirect if isAuthenticated is true and isAuthenticating is false', () => {
		const wrapper = setup(true, false)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
	it('should render TestComponent if isAuthenticated is false isAuthenticating is true', () => {
		const wrapper = setup(false, true)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render Redirect if isAuthenticated and isAuthenticating are both true', () => {
		const wrapper = setup(true, true)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
})
