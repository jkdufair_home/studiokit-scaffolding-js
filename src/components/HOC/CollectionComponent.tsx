import _, { Dictionary } from 'lodash'
import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { Model, ModelCollection, NET_ACTION } from 'studiokit-net-js'
import MODEL_STATUS from '../../constants/modelStatus'
import { dispatchAction } from '../../redux/actionCreator'
import { BaseReduxState } from '../../types'
import modelUtils from '../../utils/model'
import * as routeUtils from '../../utils/route'
import Loading from '../Loading'
import guidComponent, { GuidComponentWrappedProps } from './GuidComponent'

/** The user defined props passed into `CollectionComponent`. */
export interface CollectionComponentOwnProps {
	/**
	 * The generic path (no Ids) to where the model is stored in redux.
	 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
	 * Can be set in constructor or at render time, e.g. `<C modelName="otherModel" />`
	 */
	modelName: string
	/** The Ids that will be inserted between the components of `modelName`, to link to specific collection items in redux. */
	pathParams: string[]
	/** Custom queryParams for use in all requests. */
	queryParams?: Dictionary<string>
	/** Whether or not to trigger `load()` after mounting. Default is `false`. */
	disableAutoLoad?: boolean
}

/** The props passed into `CollectionComponent` from Redux using `mapStateToProps`. */
export interface CollectionComponentStateProps<TModel extends Model> {
	/** The collection model loaded from redux if available, or loaded into redux using `load()`. */
	model: ModelCollection<TModel>
	/** The collection model converted to an array of all collection items. */
	modelArray: TModel[]
}

/** The props passed into `CollectionComponent` from the user and other HOCs. */
export interface CollectionComponentProps<TModel extends Model>
	extends CollectionComponentOwnProps,
		GuidComponentWrappedProps,
		RouteComponentProps,
		CollectionComponentStateProps<TModel> {}

/** The internal state of `CollectionComponent`. */
interface CollectionComponentState {
	modelStatus: MODEL_STATUS
	previousModelStatus: MODEL_STATUS
	fetchingId?: string
}

export interface CollectionLoadParams {
	/** Id of an item in the collection to load. Otherwise entire collection is loaded. */
	id?: string
	/** Custom pathParams for use in the request. */
	pathParams?: string[]
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
	/** The period (in ms) at which the collection should reload the data. */
	period?: number
	/** The taskId to set for later cancellation. */
	taskId?: string
}

export interface CollectionCreateParams {
	/** The body to send in the request. */
	body: any
	/** The 'Content-Type' header value to use. */
	contentType?: string
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
}

export interface CollectionUpdateParams {
	/** The id of the collection item to update. */
	id: string
	/** The body to send in the request. */
	body: any
	/** The 'Content-Type' header value to use. */
	contentType?: string
	/** The HTTP method to use. Defaults to 'PUT'. */
	method?: string
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
}

export interface CollectionDeleteParams {
	/** The id of the collection item to delete. */
	id: string
	/** The body to send in the request. */
	body?: any
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
}

/** The methods of `CollectionComponent`. */
export interface CollectionComponentMethods {
	/** Dispatch a GET request for the collection, or an item in the collection. */
	load: (params?: CollectionLoadParams) => void
	/**
	 * Stop periodic fetch of the collection.
	 *
	 * @param taskId The taskId that was initially set when periodic fetch was requested.
	 */
	stopPeriodicLoad: (taskId: string) => void
	/**
	 * Dispatch a POST request to create an item in the collection.
	 *
	 * The new item can be tracked using the `guid` at `model[guid]`.
	 */
	create: (params: CollectionCreateParams) => void
	/** Dispatch a PUT request to update an item in the collection. */
	update: (params: CollectionUpdateParams) => void
	/** Dispatch a DELETE request to remove an item from the collection. */
	delete: (params: CollectionDeleteParams) => void
}

/** The props passed down to the `WrappedComponent`. */
export interface CollectionComponentWrappedProps<TModel extends Model>
	extends CollectionComponentProps<TModel>,
		CollectionComponentMethods,
		CollectionComponentState {}

export function configureCollectionComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionComponentWrappedProps<TModel>>,
	LoaderComponent: ComponentType = Loading
) {
	type HocProps = TOwnProps & CollectionComponentProps<TModel>

	return class CollectionComponent extends Component<HocProps, CollectionComponentState>
		implements CollectionComponentMethods {
		constructor(props: HocProps) {
			super(props)
			this.state = {
				// initializing until model is loaded
				modelStatus: MODEL_STATUS.UNINITIALIZED,
				previousModelStatus: MODEL_STATUS.UNINITIALIZED,
				fetchingId: undefined
			}
		}

		/**
		 * Automatically dispatch a GET request when component is mounted.
		 *
		 * Does NOT automatically dispatch if:
		 * * model is already fetching.
		 */
		componentDidMount() {
			const { disableAutoLoad, model } = this.props
			if (!disableAutoLoad && (!model._metadata || !model._metadata.isFetching)) {
				this.load()
			} else if (disableAutoLoad) {
				this.setState({
					modelStatus: MODEL_STATUS.READY
				})
			}
		}

		/**
		 * * If pathParams change, while the component is mounted, dispatch a new GET
		 * * When the model has finished fetching, update the modelStatus
		 */
		componentDidUpdate(prevProps: HocProps) {
			const { model: prevModel, pathParams: prevPathParams } = prevProps
			const { model, pathParams } = this.props
			const { previousModelStatus, modelStatus, fetchingId } = this.state

			// track previous modelStatus to show load vs. save errors
			if (modelStatus !== previousModelStatus) {
				this.setState({
					previousModelStatus: modelStatus
				})
			}

			const modelFetchResult = !!fetchingId
				? modelUtils.getModelFetchResult<TModel>(
						prevModel[fetchingId] as TModel | undefined,
						model[fetchingId] as TModel | undefined
				  )
				: modelUtils.getModelFetchResult(prevModel, model)
			if (modelFetchResult.isFinished) {
				this.setState({
					fetchingId: undefined,
					modelStatus: !modelFetchResult.isSuccess ? MODEL_STATUS.ERROR : MODEL_STATUS.READY
				})
			}

			if (
				pathParams.length > 0 &&
				!_.isEqual(prevPathParams, pathParams) &&
				(modelStatus === MODEL_STATUS.UNINITIALIZED ||
					modelStatus === MODEL_STATUS.READY ||
					modelStatus === MODEL_STATUS.ERROR)
			) {
				this.load({ pathParams })
			}
		}

		/**
		 * Delete the guid in the model when the component is about to be unmounted
		 */
		componentWillUnmount() {
			const { modelName, model, guid, pathParams } = this.props
			if (_.includes(Object.keys(model), guid)) {
				const modelNames = modelName.split('.')
				const tempPathParams = pathParams.slice()
				// should include the guid at the end
				tempPathParams.push(guid)
				if (modelNames.length === tempPathParams.length) {
					let path = ''
					modelNames.forEach((m, i) => {
						if (i === 0) {
							path += `${m}.${tempPathParams[i]}`
						} else {
							path += `.${m}.${tempPathParams[i]}`
						}
					})
					dispatchAction(NET_ACTION.KEY_REMOVAL_REQUESTED, {
						modelName: path
					})
				}
			}
		}

		load = ({ id, pathParams, queryParams, period, taskId }: CollectionLoadParams = {}) => {
			const { modelName, pathParams: propPathParams, queryParams: propQueryParams } = this.props
			const { modelStatus } = this.state
			const p = _.concat([], pathParams || propPathParams, id ? [id] : [])
			if (p.length !== routeUtils.getMinRequiredPathParamsCount(modelName) + (id ? 1 : 0)) {
				throw new Error('pathParams length does not match length of path components')
			}
			if (modelStatus !== MODEL_STATUS.UNINITIALIZED) {
				this.setState({
					fetchingId: id,
					modelStatus: MODEL_STATUS.LOADING
				})
			}
			if (!period) {
				dispatchAction(NET_ACTION.DATA_REQUESTED, {
					modelName,
					method: 'GET',
					pathParams: p,
					queryParams: queryParams || propQueryParams
				})
			} else {
				dispatchAction(NET_ACTION.PERIODIC_DATA_REQUESTED, {
					modelName,
					method: 'GET',
					pathParams: p,
					queryParams: queryParams || propQueryParams,
					period,
					taskId
				})
			}
		}

		stopPeriodicLoad = (taskId: string) => {
			const { modelName } = this.props

			dispatchAction(NET_ACTION.PERIODIC_TERMINATION_REQUESTED, {
				modelName,
				taskId
			})
		}

		create = (params: CollectionCreateParams) => {
			const { body, contentType, queryParams } = params
			if (!body) {
				throw new Error(`'body' is required`)
			}
			const { guid, modelName, queryParams: propQueryParams } = this.props
			const pathParams = this.props.pathParams
			if (pathParams && pathParams.length !== routeUtils.getMinRequiredPathParamsCount(modelName)) {
				throw new Error('pathParams length does not match length of path components')
			}
			this.setState({
				fetchingId: guid,
				modelStatus: MODEL_STATUS.CREATING
			})
			dispatchAction(NET_ACTION.DATA_REQUESTED, {
				modelName,
				guid,
				method: 'POST',
				body,
				pathParams,
				queryParams: queryParams || propQueryParams,
				contentType
			})
		}

		update = (params: CollectionUpdateParams) => {
			const { id, body, contentType, method, queryParams } = params
			if (!id) {
				throw new Error(`'id' is required`)
			}
			if (!body) {
				throw new Error(`'body' is required`)
			}
			const { modelName, pathParams: propPathParams, queryParams: propQueryParams } = this.props
			const pathParams = _.concat([], propPathParams, [id])
			if (pathParams.length !== routeUtils.getMinRequiredPathParamsCount(modelName) + 1) {
				throw new Error('pathParams+id length does not match length of path components')
			}
			const m = method || 'PUT'
			this.setState({
				fetchingId: id,
				modelStatus: MODEL_STATUS.UPDATING
			})
			dispatchAction(NET_ACTION.DATA_REQUESTED, {
				modelName,
				method: m,
				pathParams,
				queryParams: queryParams || propQueryParams,
				body,
				contentType
			})
		}

		delete = (params: CollectionDeleteParams) => {
			const { id, body, queryParams } = params
			if (!id) {
				throw new Error(`'id' is required`)
			}
			const { modelName, pathParams, queryParams: propQueryParams } = this.props
			const p = _.concat([], pathParams, [id])
			if (p.length !== routeUtils.getMinRequiredPathParamsCount(modelName) + 1) {
				throw new Error('pathParams+id length does not match length of path components')
			}
			this.setState({
				fetchingId: id,
				modelStatus: MODEL_STATUS.DELETING
			})
			dispatchAction(NET_ACTION.DATA_REQUESTED, {
				modelName,
				method: 'DELETE',
				pathParams: p,
				queryParams: queryParams || propQueryParams,
				body
			})
		}

		render() {
			const { modelStatus, previousModelStatus, fetchingId } = this.state

			if (modelStatus === MODEL_STATUS.UNINITIALIZED) {
				return <LoaderComponent />
			}

			return (
				<WrappedComponent
					{...this.props}
					modelStatus={modelStatus}
					previousModelStatus={previousModelStatus}
					fetchingId={fetchingId}
					load={this.load}
					stopPeriodicLoad={this.stopPeriodicLoad}
					create={this.create}
					update={this.update}
					delete={this.delete}
				/>
			)
		}
	}
}

export const configureMapStateToProps = <TModel extends Model, TOwnProps extends {}>(modelName: string) => (
	state: BaseReduxState,
	ownProps: TOwnProps & Partial<CollectionComponentProps<TModel>>
) => {
	const newModelName = ownProps.modelName || modelName
	const pathParams = ownProps.pathParams || routeUtils.getPathParams(ownProps, newModelName)
	const reduxModelName = routeUtils.getReduxModelName(pathParams, newModelName)

	let model = _.get(state.models, reduxModelName) as ModelCollection<TModel> | undefined
	model = model && Object.keys(model).length > 0 ? _.merge({}, model) : {}

	// convenient way to access collection as an array
	const modelArray = modelUtils.getModelArray<TModel>(model, ownProps.guid)

	return {
		modelName: newModelName,
		pathParams,
		model,
		modelArray
	}
}

/**
 * HOC that provides "collection" related functionality, using redux `connect()`, react-router-dom `withRouter()`, and `GuidComponent`.
 *
 * @template T The type of model that is in this collection.
 *
 * @param WrappedComponent The component to wrap.
 * @param modelName The generic path (no Ids) to where the collection is stored in redux.
 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
 * Can overriden at render time, e.g. `<C modelName="otherModel" />`.
 * @param LoaderComponent Component to use as the Loader. Defaults to `<Loading />`.
 */
export default function collectionComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionComponentWrappedProps<TModel>>,
	modelName: string,
	LoaderComponent?: ComponentType
) {
	const CollectionComponent = configureCollectionComponent<TModel, TOwnProps>(WrappedComponent, LoaderComponent)
	const mapStateToProps = configureMapStateToProps<TModel, TOwnProps>(modelName)
	// @ts-ignore
	return guidComponent(withRouter(connect(mapStateToProps)(CollectionComponent)))
}
