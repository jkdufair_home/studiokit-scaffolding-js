import React, { Component, ComponentType } from 'react'
import { Modal } from 'react-bootstrap'
import { dispatchAction } from '../../redux/actionCreator'
import { ACTION } from '../../redux/actions'
import guidComponent, { GuidComponentWrappedProps } from '../HOC/GuidComponent'

export interface ConnectedModalWrappedProps extends GuidComponentWrappedProps {
	onEntering: () => void
	onExiting: () => void
	onExited: () => void
}

/**
 * HOC that provides modal lifecycle event methods to coordinate modal state in redux.
 * Uses `GuidComponent` to generate a unique identifier for the modal.
 *
 * @param {*} WrappedComponent The component to wrap.
 */
export function configureConnectedModalComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & ConnectedModalWrappedProps>
) {
	return class ConnectedModalComponent extends Component<TOwnProps & GuidComponentWrappedProps> {
		onExitingTimeout?: number = undefined

		componentWillUnmount() {
			this.onExited()
		}

		onEntering = () => {
			const { guid } = this.props
			dispatchAction(ACTION.MODAL_ENTERING, { guid })
		}

		onExiting = () => {
			// NOTE: "react-bootstrap" / "react-overlays" do not appear
			// to call `onExited` correctly when the `show` prop is toggled.
			//
			// Manually call it after the transition is over.
			//
			// https://github.com/react-bootstrap/react-bootstrap/blob/v0.32.4/src/Modal.js
			// https://github.com/react-bootstrap/react-overlays/blob/v0.8.3/src/Modal.js
			//
			// TRANSITION_DURATION is not a 'public' property on Modal
			this.onExitingTimeout = window.setTimeout(this.onExited, (Modal as any).TRANSITION_DURATION)
		}

		onExited = () => {
			const { guid } = this.props
			dispatchAction(ACTION.MODAL_EXITED, { guid })
			if (!!this.onExitingTimeout) {
				window.clearTimeout(this.onExitingTimeout)
				this.onExitingTimeout = undefined
			}
		}

		render() {
			return (
				<WrappedComponent
					{...this.props}
					onEntering={this.onEntering}
					onExiting={this.onExiting}
					onExited={this.onExited}
				/>
			)
		}
	}
}

export default function connectedModalComponent<TOwnProps extends {}>(
	wrappedComponent: ComponentType<TOwnProps & ConnectedModalWrappedProps>
) {
	const component = configureConnectedModalComponent(wrappedComponent)
	return guidComponent(component)
}
