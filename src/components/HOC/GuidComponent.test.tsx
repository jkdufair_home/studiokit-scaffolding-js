import { shallow } from 'enzyme'
import React from 'react'
import guidComponent, { GuidComponentWrappedProps } from './GuidComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps & GuidComponentWrappedProps) => (
	<div id="testComponent">
		{props.foo}
		{props.guid}
	</div>
)

const setup = () => {
	const TestComponentWithGuid = guidComponent(TestComponent)
	return shallow(<TestComponentWithGuid foo="bar" />)
}

describe('GuidComponent', () => {
	it('should render with a `guid`', () => {
		const wrapper = setup()
		expect(wrapper.prop('guid')).toBeTruthy()
	})
})
