import { shallow } from 'enzyme'
import each from 'jest-each'
import _ from 'lodash'
import React, { Component, ComponentType } from 'react'
import { BaseReduxState } from '../../types'
import {
	configureMapDispatchToProps,
	configureMapStateToProps,
	configureSearchPersistorComponent,
	SearchPersistorWrappedProps
} from './SearchPersistorComponent'

const requiredTestMessage = 'Required test message'

interface TestComponentProps {
	foo: string
}

class TestComponent extends Component<TestComponentProps & SearchPersistorWrappedProps> {
	componentDidMount() {
		this.props.setSearchDefaults(() => this.doSearch(), {
			keywords: 'initial'
		})
	}
	// tslint:disable-next-line
	doSearch = () => {}
	// note: doSearch is passed as a lambda so `spyOn` works correctly
	render() {
		return (
			<div>
				{this.props.foo}
				<button
					id="updateAndPersistSearchButton"
					onClick={() =>
						this.props.updateAndPersistSearch(
							{
								keywords: 'foobar',
								requiredMessage: requiredTestMessage
							},
							this.doSearch
						)
					}
				/>
				<button id="searchButton" onClick={() => this.props.handleSearchClick()} />
				<button id="selectedTabButton" onClick={() => this.props.handleChangeTab(2)} />
				<button
					id="noKeywordChangeButton"
					onClick={() => this.props.handleKeywordsChange({ target: { value: '' } })}
				/>
				<button
					id="shortKeywordChangeButton"
					onClick={() => this.props.handleKeywordsChange({ target: { value: 'f' } })}
				/>
				<button
					id="longKeywordChangeButton"
					onClick={() => this.props.handleKeywordsChange({ target: { value: 'foobar' } })}
				/>
				<button
					id="queryAllChecked"
					onClick={() => this.props.handleQueryAllChange({ target: { checked: true } })}
				/>
				<button
					id="queryAllUnchecked"
					onClick={() => this.props.handleQueryAllChange({ target: { checked: false } })}
				/>
				<button id="pressEnterKeyButton" onClick={() => this.props.handleKeywordsKeyDown({ key: 'Enter' })} />
				<button id="pressSomeOtherKeyButton" onClick={() => this.props.handleKeywordsKeyDown({ key: 'x' })} />

				<button id="resetSearchButton" onClick={() => this.props.resetSearch()} />
			</div>
		)
	}
}

const mockDispatch = {
	dispatch: jest.fn()
}

function setup(
	wrappedComponent: ComponentType<TestComponentProps & SearchPersistorWrappedProps>,
	usePersistedSearch = true,
	additionalState = {}
) {
	const key = 'UsersManage'
	const mockState: Partial<BaseReduxState> = {
		search: {
			UsersManage: usePersistedSearch ? _.merge({}, { keywords: '' }, additionalState) : undefined
		}
	}

	const mockProps = {
		foo: 'bar'
	}
	const mapStateToProps = configureMapStateToProps(key)
	const mapDispatchToProps = configureMapDispatchToProps(key)
	const PersistorComponent = configureSearchPersistorComponent(wrappedComponent)
	const props = {
		...mockProps,
		...mapStateToProps(mockState as BaseReduxState),
		...mapDispatchToProps(mockDispatch.dispatch)
	}
	return shallow<SearchPersistorWrappedProps>(<PersistorComponent {...props} />)
}

describe('SearchPersistorComponent', () => {
	// Apparently jest does not parallelize tests. So this should be safe in the absence of any
	// other context in which to stick it
	let dispatchSpy: any
	beforeEach(() => {
		jest.useFakeTimers()
		dispatchSpy = jest.spyOn(mockDispatch, 'dispatch')
	})

	afterEach(() => {
		expect(dispatchSpy).toHaveBeenCalled()
	})

	each([true, false]).describe('shouldUsePersistedSearch: %s', shouldUsePersistedSearch => {
		it('should update child search props and call callback (i.e. doSearch) when keywords change', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#updateAndPersistSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.keywords).toEqual('foobar')
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should set search props invalidKeywords = true when keywords are too short and trying to search', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#shortKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeTruthy()
		})

		it('should set search props invalidKeywords = false, hasSearch = true, and call doSearch when keywords not too short and trying to search', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeFalsy()
			expect(wrapper.props().search!.hasSearched).toBeTruthy()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should set search props invalidKeywords = false, hasSearch = true, and call doSearch with no keywords', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#noKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeFalsy()
			expect(wrapper.props().search!.hasSearched).toBeTruthy()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should update the selected tab prop when user changes tabs', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#selectedTabButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.selectedTab).toEqual(2)
		})

		it('should update the keywords search prop when keyword is changed', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			jest.runOnlyPendingTimers()
			child.find('#shortKeywordChangeButton').simulate('click')
			expect(wrapper.props().search!.keywords).toEqual('f')
		})

		it('should handle queryAll when it is checked', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#updateAndPersistSearchButton').simulate('click')
			child.find('#queryAllChecked').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.queryAll).toBeTruthy()
			expect(wrapper.props().search!.requiredMessage).toEqual(requiredTestMessage)
		})

		it('should handle queryAll when it is unchecked', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#updateAndPersistSearchButton').simulate('click')
			child.find('#queryAllUnchecked').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.queryAll).toBeFalsy()
			expect(wrapper.props().search!.requiredMessage).toBeNull()
		})

		it('should search when the enter key is pressed', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#pressEnterKeyButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should not search when some other key besides is pressed', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#pressSomeOtherKeyButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(callbackSpy).not.toHaveBeenCalled()
		})

		it('should reset the search correctly', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#resetSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.keywords).toEqual('initial')
		})
	})

	describe('persisted date', () => {
		it('should rehydrate dates correctly from redux', () => {
			const wrapper = setup(TestComponent, true, { date: '2019-01-01T00:00:00.000Z' })
			const child = wrapper.dive()
			child.find('#resetSearchButton').simulate('click')
			expect(wrapper.props().search!.date).toEqual(new Date(Date.UTC(2019, 0, 1, 0, 0, 0, 0)))
		})
	})
})
