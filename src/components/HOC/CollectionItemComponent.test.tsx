import { shallow } from 'enzyme'
import React, { Component, ComponentType } from 'react'
import { Model, ModelCollection } from 'studiokit-net-js'
import { setEndpointMappings } from '../../constants/configuration'
import { BaseReduxState } from '../../types'
import {
	CollectionItemComponentMethods,
	CollectionItemComponentProps,
	CollectionItemComponentWrappedProps,
	configureCollectionItemComponent,
	configureMapStateToProps
} from './CollectionItemComponent'

interface TestModel extends Model {
	id: number
	name: string
}

interface TestComponentProps {
	foo: string
}

const TestLoader = () => <div id="testLoader" />

const TestComponent = (props: TestComponentProps & CollectionItemComponentWrappedProps<TestModel>) => (
	<div id="testComponent">
		{props.model._metadata}
		{props.model.id}
	</div>
)

const setup = (testModels: ModelCollection<TestModel> = {}, extraProps?: any, LoadingComponent?: ComponentType) => {
	const CollectionComponent = configureCollectionItemComponent<TestModel, TestComponentProps>(
		TestComponent,
		LoadingComponent
	)
	const mapStateToProps = configureMapStateToProps<TestModel, TestComponentProps>('testModels')
	const state: Partial<BaseReduxState> = {
		models: {
			testModels
		}
	}
	const ownProps = {
		...{
			foo: 'bar',
			guid: '12345',
			pathParams: ['1'],
			location: {
				pathname: '/'
			},
			match: {
				params: {}
			}
		},
		...extraProps
	}
	const props: TestComponentProps & CollectionItemComponentProps<TestModel> = {
		...ownProps,
		...mapStateToProps(state as BaseReduxState, ownProps)
	}

	// define the interface of the wrapper using the generic types
	return shallow<CollectionItemComponentMethods & Component>(<CollectionComponent {...props} />)
}

beforeAll(() => {
	setEndpointMappings({
		testModels: {
			_config: {
				isCollection: true
			}
		}
	})
})

describe('CollectionItemComponent', () => {
	it('should render <Loading> component on first render', () => {
		const wrapper = setup()
		expect(wrapper.find('Loading').length).toEqual(1)
	})
	it('should render <TestLoader> component if provided', () => {
		const wrapper = setup({}, {}, TestLoader)
		expect(wrapper.find('TestLoader').length).toEqual(1)
	})
	it('should render <TestComponent> immediately if `disableAutoLoad = true`', () => {
		const wrapper = setup({}, { disableAutoLoad: true })
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	describe('functions', () => {
		describe('componentDidMount', () => {
			it('should load() if disableAutoLoad = falsy and model has no metadata', () => {
				const wrapper = setup()
				const instance = wrapper.instance()
				const spy = jest.spyOn(instance, 'load')
				if (instance.componentDidMount) {
					instance.componentDidMount()
				}
				expect(spy).toHaveBeenCalledTimes(1)
			})
			it('should load() if disableAutoLoad = falsy and model is not fetching', () => {
				const wrapper = setup({
					_metadata: {
						isFetching: false,
						hasError: false
					}
				})
				const instance = wrapper.instance()
				const spy = jest.spyOn(instance, 'load')
				if (instance.componentDidMount) {
					instance.componentDidMount()
				}
				expect(spy).toHaveBeenCalledTimes(1)
			})
		})
	})
})
