import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router-dom'
import { Model, ModelCollection } from 'studiokit-net-js'
import { BaseReduxState } from '../../types'
import { canPerformActivityGlobally, defaultOptions } from '../../utils/baseActivity'
import { getModelId } from '../../utils/route'
import CollectionItemComponent, { CollectionItemComponentWrappedProps } from './CollectionItemComponent'
import ModelErrorRedirectComponent from './ModelErrorRedirectComponent'

export interface EntityComponentProps<T extends Model> extends CollectionItemComponentWrappedProps<T> {
	model: T & { isDeleted?: boolean }
}

export interface EntityComponentWrappedHeaderProps<T extends Model> {
	[index: string]: T
}

export interface EntityComponentWrappedProps<T extends Model> {
	modelName: string
	[index: string]: T | string
}

export const configureEntityComponent = <T extends Model>(
	WrappedComponent: ComponentType<EntityComponentWrappedProps<T>>,
	WrappedHeader: ComponentType<EntityComponentWrappedHeaderProps<T>>,
	propName: string,
	entityName: string
) => {
	return class EntityComponent extends Component<EntityComponentProps<T>> {
		render() {
			const { model, modelName } = this.props
			const newProps = {
				[propName]: model
			}
			return (
				<div>
					{!!WrappedHeader && <WrappedHeader {...newProps} />}
					{model.isDeleted ? (
						<Redirect to={`/${entityName === 'groups' ? 'courses' : entityName}`} />
					) : (
						<WrappedComponent {...newProps} modelName={modelName} />
					)}
				</div>
			)
		}
	}
}

export const configureMapStateToProps = <T extends Model>(readActivity: string, modelName: string) => (
	state: BaseReduxState,
	ownProps: EntityComponentProps<T>
) => {
	const canReadGlobally = canPerformActivityGlobally(readActivity, defaultOptions(state))

	// override modelName if admin does not have a role in the given entity
	let newModelName = modelName
	const modelId = getModelId(ownProps, newModelName)
	const hasAccess =
		!!state.models[modelName] &&
		!!(state.models[modelName] as ModelCollection)[modelId] &&
		!!((state.models[modelName] as ModelCollection)[modelId] as Model).id
	if (canReadGlobally && !hasAccess) {
		newModelName = `search.${newModelName}`
	}

	return {
		modelName: newModelName
	}
}

/**
 * A wrapper using CollectionItemComponent that passes down the `model`, to allow nesting of collection components.
 * Uses `CollectionItemComponent`, `ModelErrorRedirectComponent`, `withRouter`, and `connect`
 *
 * @param WrappedComponent The wrapped component
 * @param WrappedHeader The wrapped header
 * @param readActivity Read activity that will be checked for global access
 * @param modelName Model name passed to wrapped component and `CollectionItemComponent`. May be modified when only global read is available
 * @param propName The name for the prop with which the `model` will be passed to the wrapped component
 */
export default function entityComponent<T extends Model>(
	WrappedComponent: ComponentType<EntityComponentWrappedProps<T>>,
	WrappedHeader: ComponentType<EntityComponentWrappedHeaderProps<T>>,
	readActivity: string,
	modelName: string,
	propName: string
) {
	const EntityComponent = configureEntityComponent<T>(WrappedComponent, WrappedHeader, propName, modelName)
	const mapStateToProps = configureMapStateToProps<T>(readActivity, modelName)
	return connect(mapStateToProps)(
		CollectionItemComponent(ModelErrorRedirectComponent(withRouter(EntityComponent)), modelName)
	)
}
