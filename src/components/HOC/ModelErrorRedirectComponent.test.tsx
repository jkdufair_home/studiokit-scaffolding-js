import { shallow } from 'enzyme'
import React from 'react'
import { Model } from 'studiokit-net-js'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { CollectionItemComponentWrappedProps } from './CollectionItemComponent'
import ModelErrorRedirectComponent from './ModelErrorRedirectComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (previousModelStatus: MODEL_STATUS, modelStatus: MODEL_STATUS, hasError: boolean, code?: number) => {
	const lastFetchError = hasError ? { errorData: { code } } : undefined
	const props: TestComponentProps & Partial<CollectionItemComponentWrappedProps<Model>> = {
		foo: 'bar',
		previousModelStatus,
		modelStatus,
		model: {
			_metadata: {
				isFetching: false,
				hasError,
				lastFetchError
			}
		} as Partial<Model>
	}
	const ModelErrorRedirect = ModelErrorRedirectComponent(TestComponent)
	return shallow(<ModelErrorRedirect {...props as TestComponentProps & CollectionItemComponentWrappedProps<Model>} />)
}

describe('ModelErrorRedirect', () => {
	it('should render TestComponent for good route', () => {
		const wrapper = setup(MODEL_STATUS.LOADING, MODEL_STATUS.READY, false)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render Redirect if errorRoute is true', () => {
		const wrapper = setup(MODEL_STATUS.UNINITIALIZED, MODEL_STATUS.ERROR, true, 404)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
	it('should render Redirect if notFoundRoute is true', () => {
		const wrapper = setup(MODEL_STATUS.UNINITIALIZED, MODEL_STATUS.ERROR, true, 500)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
})
