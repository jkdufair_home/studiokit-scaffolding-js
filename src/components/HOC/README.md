# Router Components

## Collections

`CollectionComponent` is setup to work with `studiokit-net-js` and collection models. To use these components:

Add an item to your `apis.js` file, making sure to add `isCollection: true`
```js
groups: {
	_config: {
		isCollection: true
	}
}
```

Also allows nesting of collections
```js
groups: {
	_config: {
		isCollection: true
	},
	members: {
		_config: {
			isCollection: true
		}
	}
}
```

### TL;DR
* Use `CollectionComponent` when
  * You want to load and display an entire collection and possibly create/update/delete items in the collection
* Use `CollectionItemComponent` when
  * You want to load and display a single collection item
  * You want to create a single collection item
  * You want to edit a single collection item

### CollectionComponent

* wraps a component to give it access to a collection
* passes the following `props` to the wrapped component
  * `modelName`: path relating to an item in `apis.js` (levels separated by a '.'). Can be set in constructor or at render time, e.g. `<C modelName="otherModel" />`
  * `guid`: unique identifier of the component, used for creating new collection items
  * `pathParams`: array containing all `id`s for each corresponding segment of `modelName` (separated by a '.')
    * Default: values are pulled from the current route
      * e.g. when modelName = `groups.members`, route = `groups/:groupId/members`, path = `groups/1/members`, pathParams = `[1]`
    * Props: values are passed in as a prop, e.g. `<C props={[4, 5]} />`
  * `model`: the current model from redux, a key-value "collection", with many children stored with their `id`s as keys.
  * `modelArray`: the current model from redux with children mapped to an array. excludes `_metadata` and in progress creating item stored under `guid`
  * `load({ id?, pathParams?, queryParams?, period?, taskId? })`: dispatch a GET request for the collection. pass in `id` to load a single item inside the collection.
  * `create({ body?, contentType?, queryParams? })`: dispatch a POST request to create an item in the collection.
  * `update({ id, body, contentType?, method?, queryParams? })`: dispatch a PUT request to update an item in the collection by `id`.
  * `delete({ id, body?, queryParams? })`: dispatch a DELETE request to remove an item from the collection by `id`.
* notes
  * `model` is loaded from redux if available. `load()` is automatically called during `componentWillMount()`

#### Root Collection
*Component*
```js
const Groups = CollectionComponent(WrappedComponent, 'groups')
```
*Route*
```js
<Route path="/groups" component={Groups} />
```

#### Nested Collection
*Component*
```js
const GroupMembers = CollectionComponent(WrappedComponent, 'groups.members')
```
*Route*
```js
<Route path="/groups/:groupId/members" component={GroupMembers} />
```

### CollectionItemComponent

* wraps a component to give it access to a single collection item
* passes the following `props` to the wrapped component
  * `modelName`: path relating to an item in `apis.js` (levels separated by a '.'). Can be set in constructor or at render time, e.g. `<C modelName="otherModel" />`
  * `guid`: unique identifier of the component, used for creating new collection items
  * `pathParams`: array containing all `id`s for each corresponding segment of `modelName` (separated by a '.')
    * Default: values are pulled from the current route
      * e.g. when modelName = `groups.members`, route = `groups/:groupId/members`, path = `groups/1/members`, pathParams = `[1]`
    * Props: values are passed in as a prop, e.g. `<C props={[4, 5]} />`
  * `model`: the current model from redux.
  * `modelMinusRelations`: the current model from redux without children relations, e.g. for use in PUT requests
  * `load({ pathParams?, queryParams?, period?, taskId? })`: dispatch a GET request for the collection item.
  * `create({ body?, contentType?, queryParams? })`: dispatch a POST request for the collection item.
  * `update({ body, contentType?, method?, queryParams? })`: dispatch a PUT request for the collection item.
  * `delete({ body?, queryParams? })`: dispatch a DELETE request for the collection item.
* notes
  * `model` is loaded from redux if available. `load()` is automatically called during `componentWillMount()` if `pathParams` contains enough ids, a.k.a. we are not creating a new item.

#### Root Collection Item
*Component*
```js
const Group = CollectionItemComponent(WrappedComponent, 'groups')
```
*Route*
```js
<Route path="/groups/:groupId" component={Group} />
```
*Notes*
* Access to a single item in the `groups` collection
* The `model` prop will a single group

#### Nested Collection Item
*Component*
```js
const GroupMember = CollectionItemComponent(WrappedComponent, 'groups.members')
```
*Route*
```js
<Route path="/groups/:groupId/members/:memberId" component={GroupMember} />
```
*Notes*
* Access to a single item in the `groups.members` collection
* The `model` prop will a single member
