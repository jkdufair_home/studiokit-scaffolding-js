import _, { Dictionary } from 'lodash'
import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { Model, ModelCollection, NET_ACTION } from 'studiokit-net-js'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { dispatchAction } from '../../redux/actionCreator'
import { BaseReduxState } from '../../types'
import modelUtils from '../../utils/model'
import * as routeUtils from '../../utils/route'
import Loading from '../Loading'
import { CollectionCreateParams } from './CollectionComponent'
import guidComponent, { GuidComponentWrappedProps } from './GuidComponent'

/** The user defined props passed into `CollectionItemComponent`. */
export interface CollectionItemComponentOwnProps {
	/**
	 * The generic path (no Ids) to where the model is stored in redux.
	 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
	 * Can be set in constructor or at render time, e.g. `<C modelName="otherModel" />`
	 */
	modelName: string
	/** The Ids that will be inserted between the components of `modelName`, to link to specific collection items in redux. */
	pathParams: string[]
	/** Custom queryParams for use in all requests. */
	queryParams?: Dictionary<string>
	/** Whether or not to trigger `load()` after mounting. Default is `false`. */
	disableAutoLoad?: boolean
}

/** The props passed into `CollectionItemComponent` from Redux using `mapStateToProps`. */
export interface CollectionItemComponentStateProps<TModel extends Model> {
	/** The collection item model loaded from redux if available, or loaded into redux using `load()`. */
	model: TModel
	/** The collection item model, without any of its nested relations. */
	modelMinusRelations: Partial<TModel>
}

/** The props passed into `CollectionItemComponent` from the user and other HOCs. */
export interface CollectionItemComponentProps<TModel extends Model>
	extends CollectionItemComponentOwnProps,
		RouteComponentProps,
		GuidComponentWrappedProps,
		CollectionItemComponentStateProps<TModel> {}

/** The internal state of `CollectionItemComponent`. */
interface CollectionItemComponentState {
	modelStatus: MODEL_STATUS
	previousModelStatus: MODEL_STATUS
}

export interface CollectionItemLoadParams {
	/** Custom pathParams for use in the request. */
	pathParams?: string[]
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
	/** The period (in ms) at which the collection should reload the data. */
	period?: number
	/** The taskId to set for later cancellation. */
	taskId?: string
}

export interface CollectionItemUpdateParams {
	/** The body to send in the request. */
	body: any
	/** The 'Content-Type' header value to use. */
	contentType?: string
	/** The HTTP method to use. Defaults to 'PUT'. */
	method?: string
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
}

export interface CollectionItemDeleteParams {
	/** The body to send in the request. */
	body?: any
	/** Custom queryParams for use in the request. */
	queryParams?: Dictionary<string>
}

/** The methods of `CollectionItemComponent`. */
export interface CollectionItemComponentMethods {
	/** Dispatch a GET request for the collection item. */
	load: (params?: CollectionItemLoadParams) => void
	/**
	 * Stop periodic fetch of the collection.
	 *
	 * @param taskId The taskId that was initially set when periodic fetch was requested.
	 */
	stopPeriodicLoad: (taskId: string) => void
	/**
	 * Dispatch a POST request to create the collection item.
	 *
	 * The new item can be tracked using the `guid` as its key in the parent collection.
	 */
	create: (params: CollectionCreateParams) => void
	/** Dispatch a PUT request to update the collection item. */
	update: (params: CollectionItemUpdateParams) => void
	/** Dispatch a DELETE request to remove the collection item. */
	delete: (params?: CollectionItemDeleteParams) => void
}

/** The props passed down to the `WrappedComponent`. */
export interface CollectionItemComponentWrappedProps<T>
	extends CollectionItemComponentProps<T>,
		CollectionItemComponentMethods,
		CollectionItemComponentState {
	model: T
	modelMinusRelations: Partial<T>
}

export function configureCollectionItemComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionItemComponentWrappedProps<TModel>>,
	LoaderComponent: ComponentType = Loading
) {
	type HocProps = TOwnProps & CollectionItemComponentProps<TModel>

	return class CollectionItemComponent extends Component<HocProps, CollectionItemComponentState>
		implements CollectionItemComponentMethods {
		constructor(props: HocProps) {
			super(props)
			this.state = {
				// initializing until model is loaded, or if no model
				modelStatus: MODEL_STATUS.UNINITIALIZED,
				previousModelStatus: MODEL_STATUS.UNINITIALIZED
			}
		}

		/**
		 * Automatically dispatch a GET request when component is mounted.
		 *
		 * Does NOT automatically dispatch if:
		 * * pathParams does not contain enough items.
		 * * model is already fetching.
		 */
		componentDidMount() {
			const { modelName, model, disableAutoLoad, pathParams } = this.props
			if (
				pathParams.length === routeUtils.getMinRequiredPathParamsCount(modelName) + 1 &&
				!disableAutoLoad &&
				(!model._metadata || model._metadata.isFetching === false)
			) {
				this.load()
			} else {
				this.setState({
					modelStatus: MODEL_STATUS.READY
				})
			}
		}

		/**
		 * * If pathParams change, while the component is mounted, dispatch a new GET
		 * * When the model has finished fetching, update the state booleans
		 */
		componentDidUpdate(prevProps: HocProps) {
			const { model: prevModel, pathParams: prevPathParams } = prevProps
			const { modelName, model, pathParams } = this.props
			const { previousModelStatus, modelStatus } = this.state

			// track previous modelStatus to show load vs. save errors
			if (modelStatus !== previousModelStatus) {
				this.setState({
					previousModelStatus: modelStatus
				})
			}

			const modelFetchResult = modelUtils.getModelFetchResult(prevModel, model)
			if (modelFetchResult.isFinished) {
				this.setState({
					modelStatus: !modelFetchResult.isSuccess ? MODEL_STATUS.ERROR : MODEL_STATUS.READY
				})
			}

			if (
				pathParams.length === routeUtils.getMinRequiredPathParamsCount(modelName) + 1 &&
				!_.isEqual(prevPathParams, pathParams) &&
				(modelStatus === MODEL_STATUS.UNINITIALIZED ||
					modelStatus === MODEL_STATUS.READY ||
					modelStatus === MODEL_STATUS.ERROR)
			) {
				this.load({ pathParams })
			}
		}

		load = ({ pathParams, queryParams, period, taskId }: CollectionItemLoadParams = {}) => {
			const { modelName, pathParams: propPathParams, queryParams: propQueryParams } = this.props
			const { modelStatus } = this.state
			const p = pathParams || propPathParams
			if (p && p.length < routeUtils.getMinRequiredPathParamsCount(modelName) + 1) {
				throw new Error('pathParams length does not match length of path components')
			}
			if (modelStatus !== MODEL_STATUS.UNINITIALIZED) {
				this.setState({
					modelStatus: MODEL_STATUS.LOADING
				})
			}
			if (!period) {
				dispatchAction(NET_ACTION.DATA_REQUESTED, {
					modelName,
					method: 'GET',
					pathParams: p,
					queryParams: queryParams || propQueryParams
				})
			} else {
				dispatchAction(NET_ACTION.PERIODIC_DATA_REQUESTED, {
					modelName,
					method: 'GET',
					pathParams: p,
					queryParams: queryParams || propQueryParams,
					period,
					taskId
				})
			}
		}

		stopPeriodicLoad = (taskId: string) => {
			const { modelName } = this.props

			dispatchAction(NET_ACTION.PERIODIC_TERMINATION_REQUESTED, {
				modelName,
				taskId
			})
		}

		create = (params: CollectionCreateParams) => {
			const { body, contentType, queryParams } = params
			if (!body) {
				throw new Error(`'body' is required`)
			}
			const { guid, modelName, pathParams, model, queryParams: propQueryParams } = this.props
			if (!!model.id) {
				throw new Error('model already exists')
			}
			if (pathParams && pathParams.length < routeUtils.getMinRequiredPathParamsCount(modelName)) {
				throw new Error('pathParams length does not match length of path components')
			}
			this.setState({
				modelStatus: MODEL_STATUS.CREATING
			})
			dispatchAction(NET_ACTION.DATA_REQUESTED, {
				modelName,
				guid,
				method: 'POST',
				body,
				pathParams,
				contentType,
				queryParams: queryParams || propQueryParams
			})
		}

		update = (params: CollectionItemUpdateParams) => {
			const { body, contentType, method, queryParams } = params
			if (!body) {
				throw new Error(`'body' is required`)
			}
			const { modelName, pathParams, model, queryParams: propQueryParams } = this.props
			if (!model.id) {
				throw new Error('model does not exist')
			}
			if (pathParams && pathParams.length < routeUtils.getMinRequiredPathParamsCount(modelName) + 1) {
				throw new Error('pathParams length does not match length of path components')
			}
			const m = method || 'PUT'
			this.setState({
				modelStatus: MODEL_STATUS.UPDATING
			})
			dispatchAction(NET_ACTION.DATA_REQUESTED, {
				modelName,
				method: m,
				pathParams,
				body,
				contentType,
				queryParams: queryParams || propQueryParams
			})
		}

		delete = ({ body, queryParams }: CollectionItemDeleteParams = {}) => {
			const { modelName, pathParams, model, queryParams: propQueryParams } = this.props
			if (!model.id) {
				throw new Error('model does not exist')
			}
			if (pathParams && pathParams.length < routeUtils.getMinRequiredPathParamsCount(modelName) + 1) {
				throw new Error('pathParams length does not match length of path components')
			}
			this.setState({
				modelStatus: MODEL_STATUS.DELETING
			})
			dispatchAction(NET_ACTION.DATA_REQUESTED, {
				modelName,
				method: 'DELETE',
				pathParams,
				body,
				queryParams: queryParams || propQueryParams
			})
		}

		render() {
			const { modelStatus, previousModelStatus } = this.state

			if (modelStatus === MODEL_STATUS.UNINITIALIZED) {
				return <LoaderComponent />
			}

			return (
				<WrappedComponent
					{...this.props}
					model={this.props.model}
					modelMinusRelations={this.props.modelMinusRelations}
					modelStatus={modelStatus}
					previousModelStatus={previousModelStatus}
					load={this.load}
					stopPeriodicLoad={this.stopPeriodicLoad}
					create={this.create}
					update={this.update}
					delete={this.delete}
				/>
			)
		}
	}
}

export const configureMapStateToProps = <TModel extends Model, TOwnProps extends {}>(modelName: string) => (
	state: BaseReduxState,
	ownProps: TOwnProps & Partial<CollectionItemComponentProps<TModel>>
) => {
	const newModelName = ownProps.modelName || modelName
	const modelNameLevels = newModelName.split('.')

	const pathParams = ownProps.pathParams || routeUtils.getPathParams(ownProps, newModelName)
	const reduxModelName = routeUtils.getReduxModelName(pathParams, newModelName)

	let model: TModel

	// find `model` using `guid` as its key, to match new item created in `create()`
	if (pathParams.length < modelNameLevels.length) {
		const reduxModel = _.get(state.models, reduxModelName) as ModelCollection<TModel> | undefined
		const collection: ModelCollection<TModel> =
			reduxModel && Object.keys(reduxModel).length > 0 ? _.merge({}, reduxModel) : ({} as ModelCollection<TModel>)

		if (ownProps.guid && collection.hasOwnProperty(ownProps.guid)) {
			model = collection[ownProps.guid] as TModel
		} else {
			const modelByGuid = _.find(
				_.values(collection),
				(m: TModel) => m && m.guid && m.guid === ownProps.guid
			) as TModel
			model = modelByGuid || ({} as TModel)
		}
	} else {
		const reduxModel = _.get(state.models, reduxModelName) as TModel | undefined
		model = reduxModel && Object.keys(reduxModel).length > 0 ? _.merge({}, reduxModel) : ({} as TModel)
	}

	// convenient way to access model without relations, for use in PUT requests
	const modelMinusRelations = modelUtils.getModelMinusRelations(model)

	return {
		modelName: newModelName,
		pathParams,
		model: model as Model,
		modelMinusRelations: modelMinusRelations as Partial<Model>
	}
}

/**
 * HOC that provides "collection" related functionality for a single item in a collection, using redux `connect()`, react-router-dom `withRouter()`, and `GuidComponent`.
 *
 * @template T The type of model.
 *
 * @param WrappedComponent The component to wrap.
 * @param modelName The generic path (no Ids) to where the collection item is stored in redux.
 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
 * Can override at render time, e.g. `<C modelName="otherModel" />`.
 * @param LoaderComponent Component to use as the Loader. Defaults to `<Loading />`.
 */
export default function collectionItemComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionItemComponentWrappedProps<TModel>>,
	modelName: string,
	LoaderComponent?: ComponentType
) {
	const CollectionItemComponent = configureCollectionItemComponent<TModel, TOwnProps>(
		WrappedComponent,
		LoaderComponent
	)
	const mapStateToProps = configureMapStateToProps<TModel, TOwnProps>(modelName)
	// @ts-ignore
	return guidComponent(withRouter(connect(mapStateToProps)(CollectionItemComponent)))
}
