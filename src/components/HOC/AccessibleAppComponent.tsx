import React, { Component, ComponentType, RefObject } from 'react'
import { Omit } from 'react-router'
import { RouteComponentProps, withRouter } from 'react-router-dom'

interface AccessibleComponentState {
	title?: string
}

export function setupAccessibleComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & RouteComponentProps>
) {
	return class AccessibleComponent extends Component<TOwnProps & RouteComponentProps, AccessibleComponentState> {
		readonly root: RefObject<HTMLDivElement>
		readonly titleObserver: MutationObserver
		resetFocusIgnorePatterns = [
			/\/help\/scoring-docs(\/.+)?$/,
			/\/courses\/\d+\/groupAssignments\/\d+\/scores(\/.+)?$/
		]
		constructor(props: TOwnProps & RouteComponentProps) {
			super(props)
			this.root = React.createRef()
			this.titleObserver = new MutationObserver(this.onTitleChange)

			this.state = {
				title: undefined
			}
		}

		componentDidMount() {
			const titleElement = document.querySelector('title')
			if (!!titleElement) {
				this.titleObserver.observe(titleElement, {
					subtree: true,
					characterData: true,
					childList: true
				})
			}
		}

		componentWillUnmount() {
			this.titleObserver.disconnect()
		}

		onTitleChange = (mutations: MutationRecord[]) => {
			// Only change the title when we have more than the default 'Circuit' added to it.
			if (!!mutations[0] && !!mutations[0].target.textContent && mutations[0].target.textContent !== 'Circuit') {
				this.setState({ title: mutations[0].target.textContent })
			}
		}
		shouldResetFocus = (oldPath?: string, newPath?: string) => {
			// Path is the same or either parameter is not a path
			if (!newPath || !oldPath || oldPath === newPath) {
				return false
			}

			for (const route of this.resetFocusIgnorePatterns) {
				if (route.test(oldPath) && route.test(newPath)) {
					// Both paths are sub-paths in the same page
					return false
				}
			}

			// Paths are different and not in the same page
			return true
		}

		componentDidUpdate(prevProps: RouteComponentProps) {
			// Do not reset focus if the new route matches the same router path
			if (this.shouldResetFocus(prevProps.location.pathname, this.props.location.pathname)) {
				if (!!this.root && !!this.root.current) {
					this.root.current.focus()
				}
			}
		}

		render() {
			const { title } = this.state

			return (
				<>
					{/* Announce new screen titles to screen readers similar to loading a new page */}
					<span className="clip accessible-title" aria-live="polite" aria-atomic="true">
						{title}
					</span>
					<div className="outline-0 app-root" ref={this.root} tabIndex={-1}>
						<WrappedComponent {...this.props} />
					</div>
				</>
			)
		}
	}
}

/**
 * Wrap around the main application to add support for resetting focus on page changes
 * and reading title changes to screen reader users
 *
 * @param WrappedComponent - The root of your application UI components
 */
export default function accessibleAppComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & RouteComponentProps>
): React.ComponentClass<
	Omit<TOwnProps & RouteComponentProps, keyof RouteComponentProps<any>>,
	AccessibleComponentState
> {
	const component = setupAccessibleComponent(WrappedComponent)
	return withRouter(component)
}
