import { shallow } from 'enzyme'
import React from 'react'
import { Modal } from 'react-bootstrap'
import * as actionCreator from '../../redux/actionCreator'
import { ACTION } from '../../redux/actions'
import { configureConnectedModalComponent, ConnectedModalWrappedProps } from './ConnectedModalComponent'
import SpyInstance = jest.SpyInstance

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps & ConnectedModalWrappedProps) => (
	<div id="testComponent">{props.foo}</div>
)

const guid = '1'

const setup = () => {
	const Component = configureConnectedModalComponent(TestComponent)
	return shallow<TestComponentProps & ConnectedModalWrappedProps>(<Component guid={guid} foo="bar" />)
}

describe('ConnectedModalComponent', () => {
	let spy: SpyInstance
	beforeAll(() => {
		spy = jest.spyOn(actionCreator, 'dispatchAction')
	})
	afterEach(() => {
		spy.mockClear()
	})
	afterAll(() => {
		spy.mockRestore()
	})
	it('Dispatches modal opening action after calling onEntering', () => {
		const wrapper = setup()
		const wrappedProps = wrapper.props()
		wrappedProps.onEntering()
		expect(actionCreator.dispatchAction).toHaveBeenCalledWith(ACTION.MODAL_ENTERING, { guid })
	})
	it('Dispatches modal closing action after delay when calling onExiting', done => {
		const wrapper = setup()
		const wrappedProps = wrapper.props()
		wrappedProps.onExiting()
		setTimeout(() => {
			expect(actionCreator.dispatchAction).toHaveBeenCalledWith(ACTION.MODAL_EXITED, { guid })
			done()
		}, (Modal as any).TRANSITION_DURATION)
	})
})
