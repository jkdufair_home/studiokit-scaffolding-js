import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Omit, RouteComponentProps } from 'react-router'
import { Redirect, withRouter } from 'react-router-dom'
import { BaseReduxState } from '../../types'

export interface AccessRequiredComponentProps {
	[propName: string]: any
}

export const configureAccessRequiredComponent = <TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>,
	propName: string
) => {
	return class AccessRequiredComponent extends Component<TOwnProps & AccessRequiredComponentProps> {
		render() {
			if (!this.props[propName]) {
				return <Redirect to={'/'} />
			}
			return <WrappedComponent {...this.props} />
		}
	}
}

export const configureMapStateToProps = <TOwnProps extends {}>(
	propName: string,
	hasAccess: (state: BaseReduxState, ownProps: TOwnProps) => boolean
) => (state: BaseReduxState, ownProps: TOwnProps) => {
	return {
		[propName]: hasAccess(state, ownProps)
	} as AccessRequiredComponentProps
}

export default function accessRequiredComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>,
	hasAccess: (state: any, ownProps: any) => boolean,
	propName: string
): React.ComponentClass<Omit<TOwnProps & AccessRequiredComponentProps, keyof RouteComponentProps<any>>> {
	const AccessRequiredComponent = configureAccessRequiredComponent(WrappedComponent, propName)
	const mapStateToProps = configureMapStateToProps(propName, hasAccess)
	// @ts-ignore
	return withRouter(connect(mapStateToProps)(AccessRequiredComponent))
}
