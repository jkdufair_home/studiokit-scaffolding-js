import React, { Component, ComponentType } from 'react'
import { Redirect } from 'react-router-dom'
import { Model } from 'studiokit-net-js'
import MODEL_STATUS from '../../constants/modelStatus'
import { CollectionItemComponentWrappedProps } from './CollectionItemComponent'

/**
 * This component exists as a child to CollectionItemComponent to
 * redirect the user to a 404/500 page if errors occur while fetching data.
 *
 * Nested CollectionItemComponent instances should not
 * use this - they’ll want to handle errors according to the need for
 * alerting the user in the parent.
 *
 * @export
 * @param WrappedComponent Wrapped component that depends on the model. (Usually AsyncComponent)
 * @returns A wrapped component or redirects
 */
export default function modelErrorRedirectComponent<TOwnProps extends {}>(WrappedComponent: ComponentType<TOwnProps>) {
	return class ModelErrorRedirectComponent extends Component<TOwnProps & CollectionItemComponentWrappedProps<Model>> {
		render() {
			const { model, modelStatus, previousModelStatus } = this.props

			const notFoundRoute =
				previousModelStatus === MODEL_STATUS.UNINITIALIZED &&
				modelStatus === MODEL_STATUS.ERROR &&
				model._metadata &&
				model._metadata.hasError &&
				model._metadata.lastFetchError &&
				model._metadata.lastFetchError.errorData.code === 404

			const errorRoute =
				previousModelStatus === MODEL_STATUS.UNINITIALIZED &&
				modelStatus === MODEL_STATUS.ERROR &&
				model._metadata &&
				model._metadata.hasError &&
				model._metadata.lastFetchError &&
				model._metadata.lastFetchError.errorData.code === 500

			if (notFoundRoute) {
				const pathname = window.location.pathname
				return (
					<Redirect
						to={{
							pathname: '/not-found',
							search: `?pathname=${pathname}`
						}}
					/>
				)
			}
			if (errorRoute) {
				return (
					<Redirect
						to={{
							pathname: '/error'
						}}
					/>
				)
			}
			return <WrappedComponent {...this.props} />
		}
	}
}
