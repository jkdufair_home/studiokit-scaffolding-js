import { shallow } from 'enzyme'
import React from 'react'
import { BaseReduxState } from '../../types'
import {
	AccessRequiredComponentProps,
	configureAccessRequiredComponent,
	configureMapStateToProps
} from './AccessRequiredComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps & AccessRequiredComponentProps) => (
	<div id="testComponent">{props.foo}</div>
)

const setup = (propName: string, hasAccess: boolean) => {
	const AccessRequiredComponent = configureAccessRequiredComponent(TestComponent, propName)
	const mapStateToProps = configureMapStateToProps(
		propName,
		jest.fn().mockImplementation((someState: BaseReduxState, someProps: TestComponentProps) => {
			return hasAccess
		})
	)
	const ownProps: TestComponentProps = { foo: 'bar' }
	const props = {
		...ownProps,
		...mapStateToProps({} as BaseReduxState, ownProps)
	} as TestComponentProps & AccessRequiredComponentProps
	return shallow(<AccessRequiredComponent {...props} />)
}

describe('AccessRequiredComponent', () => {
	it('should render TestComponent if hasAccess is true', () => {
		const wrapper = setup('somePropName', true)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render Redirect if hasAccess is false', () => {
		const wrapper = setup('somePropNames', false)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
})
