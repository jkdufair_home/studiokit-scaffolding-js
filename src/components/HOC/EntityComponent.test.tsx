import { shallow } from 'enzyme'
import React from 'react'
import { DeepPartial } from 'redux'
import { Model, ModelCollection } from 'studiokit-net-js'
import activities from '../../constants/baseActivity'
import { mockUser } from '../../constants/mockData'
import { BaseReduxState, UserInfo } from '../../types'
import {
	configureEntityComponent,
	configureMapStateToProps,
	EntityComponentProps,
	EntityComponentWrappedHeaderProps,
	EntityComponentWrappedProps
} from './EntityComponent'

interface EntityComponentTestModel extends Model {
	id?: number
	isDeleted?: boolean
}

const defaultModel = {
	id: 1,
	isDeleted: false
}
const WrappedComponent = () => <div id="wrappedComponent" />
const WrappedHeader = () => <div id="wrappedHeader" />

const setup = (
	readActivity: string,
	modelName: string,
	propName: string,
	user: { userInfo: UserInfo },
	model: EntityComponentTestModel = {},
	params = {}
) => {
	const mockState = {
		models: {
			user,
			[modelName]: {}
		}
	}

	if (!!model.id) {
		const stateModels = mockState.models[modelName] as ModelCollection<EntityComponentTestModel>
		stateModels[model.id] = model
	}

	const mockProps = {
		// collection item
		model,
		// router
		match: { params },
		// extra
		someString: 'someValue'
	}

	const EntityComponent = configureEntityComponent(WrappedComponent, WrappedHeader, propName, 'groups')
	const mapStateToProps = configureMapStateToProps(readActivity, modelName)
	const props: DeepPartial<EntityComponentProps<EntityComponentTestModel>> = Object.assign(
		{},
		mockProps,
		mapStateToProps(mockState as BaseReduxState, mockProps as any)
	)
	return shallow(<EntityComponent {...props as EntityComponentProps<EntityComponentTestModel>} />)
}

describe('EntityComponent', () => {
	it('should pass the model and modelName to the wrapped components', () => {
		const model = Object.assign({}, defaultModel)
		const wrapper = setup(activities.GROUP_READ, 'groups', 'group', mockUser(), model, {
			groupId: 1
		})
		expect(wrapper.find('WrappedHeader').length).toEqual(1)
		const headerProps = wrapper.find('WrappedHeader').props() as EntityComponentWrappedHeaderProps<
			EntityComponentTestModel
		>
		expect(headerProps.group).toEqual(model)
		expect(wrapper.find('WrappedComponent').length).toEqual(1)
		const componentProps = wrapper.find('WrappedComponent').props() as EntityComponentWrappedProps<
			EntityComponentTestModel
		>
		expect(componentProps.group).toEqual(model)
		expect(componentProps.modelName).toEqual('groups')

		// Does not pass extra props to wrapped component
		expect((wrapper.find('WrappedComponent').props() as any).someString).toBe(undefined)
	})
	it('should modify the modelName when user has global permission but no entity', () => {
		const wrapper = setup(
			activities.GROUP_READ,
			'groups',
			'group',
			mockUser([activities.GROUP_READ]),
			{},
			{ groupId: 1 }
		)
		expect(wrapper.find('WrappedComponent').length).toEqual(1)
		const componentProps = wrapper.find('WrappedComponent').props() as EntityComponentWrappedProps<
			EntityComponentTestModel
		>
		expect(componentProps.modelName).toEqual('search.groups')
	})
	it('should render <Redirect /> if model is deleted', () => {
		const wrapper = setup(
			activities.GROUP_READ,
			'groups',
			'group',
			mockUser([activities.GROUP_READ]),
			{ id: 1, isDeleted: true },
			{ groupId: 1 }
		)
		expect(wrapper.find('WrappedComponent').length).toEqual(0)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
})
