import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { BaseReduxState } from '../../types'
import Loading from '../Loading'

interface DataDependentComponentStateProps {
	hasData: boolean
}

export const configureDataDependentComponent = <TOwnProps extends {}>(WrappedComponent: ComponentType<TOwnProps>) => {
	return class DataDependentComponent extends Component<TOwnProps & DataDependentComponentStateProps> {
		render() {
			const { hasData, ...ownProps } = this.props
			return this.props.hasData ? <WrappedComponent {...ownProps as TOwnProps} /> : <Loading />
		}
	}
}

export const configureMapStateToProps = (hasData: (state: BaseReduxState) => boolean) => (
	state: BaseReduxState
): DataDependentComponentStateProps => {
	return {
		hasData: Boolean(!!hasData && hasData(state))
	}
}

export default function dataDependentComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>,
	hasData: (state: any) => boolean
) {
	const DataDependentComponent = configureDataDependentComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps(hasData)
	// @ts-ignore
	return connect(mapStateToProps)(DataDependentComponent)
}
