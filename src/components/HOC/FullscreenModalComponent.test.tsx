import { shallow } from 'enzyme'
import React from 'react'
import {
	configureFullscreenModalComponent,
	FullscreenModalProps,
	FullscreenModalWrappedProps
} from './FullscreenModalComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps & FullscreenModalWrappedProps) => (
	<div id="testComponent">{props.foo}</div>
)

const setup = (props: TestComponentProps & FullscreenModalProps) => {
	const WrappedComponent = configureFullscreenModalComponent(TestComponent)
	return shallow(<WrappedComponent {...props} />)
}

describe('FullscreenModalComponent', () => {
	const props = {
		guid: '1',
		foo: 'bar',
		onEntering: jest.fn(),
		onExiting: jest.fn(),
		onExited: jest.fn()
	}
	beforeEach(() => {
		props.onEntering.mockReset()
		props.onExiting.mockReset()
		props.onExited.mockReset()
	})
	describe('componentDidMount', () => {
		it('should be open by default if `props.isOpen` is undefined', () => {
			const wrapper = setup(props)
			expect(wrapper.state('isOpen')).toEqual(true)
		})
		it('should call `props.onEntering` if `props.isOpen` is undefined', () => {
			setup(props)
			expect(props.onEntering.mock.calls.length).toEqual(1)
		})
		it('should call `props.onEntering` if `props.isOpen` is true', () => {
			setup({ ...props, ...{ isOpen: true } })
			expect(props.onEntering.mock.calls.length).toEqual(1)
		})
		it('should not call `props.onEntering` if `props.isOpen` is false', () => {
			setup({ ...props, ...{ isOpen: false } })
			expect(props.onEntering.mock.calls.length).toEqual(0)
		})
	})
	describe('componentDidUpdate', () => {
		it('should call `props.onEntering` if `props.isOpen` is defined, and changes to true', () => {
			const wrapper = setup({ ...props, ...{ isOpen: false } })
			expect(props.onEntering.mock.calls.length).toEqual(0)
			wrapper.setProps({
				isOpen: true
			})
			expect(props.onEntering.mock.calls.length).toEqual(1)
		})
		it('should call `props.onExiting` if `props.isOpen` is defined, and changes to false', () => {
			const wrapper = setup({ ...props, ...{ isOpen: true } })
			expect(props.onEntering.mock.calls.length).toEqual(1)
			wrapper.setProps({
				isOpen: false
			})
			expect(props.onExiting.mock.calls.length).toEqual(1)
		})
		it('should call `props.onEntering` if `props.isOpen` is undefined and `state.isOpen` changes to true', () => {
			const wrapper = setup(props)
			expect(props.onEntering.mock.calls.length).toEqual(1)
			wrapper.setState({
				isOpen: false
			})
			wrapper.setState({
				isOpen: true
			})
			expect(props.onEntering.mock.calls.length).toEqual(2)
		})
		it('should call `props.onExiting` if `props.isOpen` is undefined and `state.isOpen` changes to false', () => {
			const wrapper = setup(props)
			expect(props.onEntering.mock.calls.length).toEqual(1)
			wrapper.setProps({
				isOpen: false
			})
			expect(props.onExiting.mock.calls.length).toEqual(1)
		})
	})
})
