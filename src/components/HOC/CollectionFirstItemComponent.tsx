import React, { Component, ComponentType } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { Model } from 'studiokit-net-js'
import modelUtils from '../../utils/model'
import { CollectionComponentMethods, CollectionComponentWrappedProps } from './CollectionComponent'
import { CollectionItemComponentProps } from './CollectionItemComponent'

/** The props passed into `WrappedComponent`. */
export interface CollectionFirstItemComponentWrappedProps<TModel>
	extends CollectionItemComponentProps<TModel>,
		RouteComponentProps,
		CollectionComponentMethods {
	model: TModel // NOTE: ensure `WrappedComponent` gets the correct model type. fix for broken @types/react-redux issue.
	modelMinusRelations: Partial<TModel>
}

/**
 * HOC meant to pass the first collection item to the wrapped component as its model.
 * Should be wrapped in `CollectionComponent`.
 */
export default function collectionFirstItemComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionFirstItemComponentWrappedProps<TModel>>
) {
	return class CollectionFirstItemComponent extends Component<TOwnProps & CollectionComponentWrappedProps<TModel>> {
		getFirstItem = () => {
			const { model, modelArray, guid } = this.props
			const singleItem =
				modelArray.length > 0
					? modelArray[0]
					: !!model[guid]
					? ((model[guid] as unknown) as TModel)
					: ({} as TModel)
			return singleItem
		}

		render() {
			const { pathParams } = this.props
			const firstItem = this.getFirstItem()
			const modelMinusRelations = modelUtils.getModelMinusRelations(firstItem)
			const p = [...pathParams]
			if (!!firstItem.id) {
				p.push(firstItem.id.toString())
			}
			return (
				<WrappedComponent
					{...this.props}
					pathParams={p}
					model={firstItem as TModel}
					modelMinusRelations={modelMinusRelations as Partial<TModel>}
				/>
			)
		}
	}
}
