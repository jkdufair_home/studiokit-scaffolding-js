import React, { Component, ComponentType } from 'react'
import uuid from 'uuid'

export interface GuidComponentWrappedProps {
	guid: string
}

export interface GuidComponentState {
	guid: string
}

export default function guidComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & GuidComponentWrappedProps>
) {
	return class GuidComponent extends Component<TOwnProps, GuidComponentState> {
		constructor(props: TOwnProps) {
			super(props)
			this.state = {
				guid: uuid.v4()
			}
		}

		render() {
			return <WrappedComponent {...this.props} guid={this.state.guid} />
		}
	}
}
