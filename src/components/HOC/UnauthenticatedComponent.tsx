import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { BaseReduxState } from '../../types'

interface StateProps {
	isAuthenticating: boolean
	isAuthenticated: boolean
}

export const configureUnauthenticatedComponent = <TOwnProps extends {}>(WrappedComponent: ComponentType<TOwnProps>) => {
	return class UnauthenticatedComponent extends Component<TOwnProps & StateProps> {
		render() {
			const { isAuthenticating, isAuthenticated, ...ownProps } = this.props
			if (!isAuthenticating && isAuthenticated) {
				return <Redirect to={'/'} />
			}
			return <WrappedComponent {...ownProps as TOwnProps} />
		}
	}
}

export const mapStateToProps = (state: BaseReduxState): StateProps => {
	return {
		isAuthenticating: !state.auth.isAuthenticated && state.auth.isAuthenticating,
		isAuthenticated: state.auth.isAuthenticated
	}
}

export default function unauthenticatedComponent<TOwnProps extends {}>(WrappedComponent: ComponentType<TOwnProps>) {
	const UnauthenticatedComponent = configureUnauthenticatedComponent(WrappedComponent)
	// @ts-ignore
	return connect(mapStateToProps)(UnauthenticatedComponent)
}
