import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom'
import { defaultOptions } from '../../utils/baseActivity'

/**
 * This HOC ensures that the wrapped component is rendered iff the predicate provided is satisfied.
 *
 * Typically this component is used by passing one of the functions in utils/activities as the
 * `accessPredicate` and a constant from "constants/activities" as the `requiredActivity`.
 *
 * If a lambda is passed as the predicate, it is passed (but may or may not opt to utilize) the `requiredEntity`
 * and `modelsProperty` parameters. They can be ignored if not needed by the lambda.
 *
 * @param {Component} WrappedComponent The component which requires activity/activities in order to render
 * @param {function} accessPredicate A predicate accepting a required activity and an optional entity and/or
 * 		userInfo object
 * @param {string} requiredActivity The required activity which is passed to the predicate for evaluation
 * @param {string} modelsProperty The property used to locate the entity or entities needed for entity-level
 * activity grants
 *
 */

interface ActivityRequiredProps {
	hasAccess: boolean
}

export const activityRequiredComponent = (WrappedComponent: ComponentType) => {
	return class ActivityRequiredComponent extends Component<ActivityRequiredProps & RouteComponentProps, {}> {
		public render() {
			if (!this.props.hasAccess) {
				return <Redirect to={'/'} />
			}
			return <WrappedComponent {...this.props} />
		}
	}

	/**
	 * Return a `hasAccess` boolean property that evaluates the accessPredicate passed
	 * to this HOC, passing the required activity and an options object containing
	 * the object(s) needed for determining activities having been granted to the user.
	 *
	 * @param {Object} state The redux store
	 * @param {Object} ownProps The props passed into the ActivityRequiredComponent
	 */
}
export const configureMapStateToProps = (
	hasAccess: (state: any, ownProps: any) => boolean,
	accessPredicate: any,
	requiredActivity: string,
	modelsProperty: string
) => (state: any, ownProps: any) => {
	return {
		hasAccess: accessPredicate(requiredActivity, defaultOptions(state, ownProps, modelsProperty))
	}
}
export default function accessRequiredComponent(
	WrappedComponent: ComponentType,
	hasAccess: (state: any, ownProps: any) => boolean,
	accessPredicate: any,
	requiredActivity: string,
	modelsProperty: string
): React.ComponentClass<Pick<any, string | number | symbol>, any> {
	const ActivityRequiredComponent = activityRequiredComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps(hasAccess, accessPredicate, requiredActivity, modelsProperty)
	return withRouter(connect(mapStateToProps)(ActivityRequiredComponent))
}
