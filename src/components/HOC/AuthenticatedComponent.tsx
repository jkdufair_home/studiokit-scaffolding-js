import _ from 'lodash'
import { parse, stringify } from 'query-string'
import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { StaticContext } from 'react-router'
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom'
import { AuthState } from 'studiokit-auth-js'
import { BaseReduxState } from '../../types'

export const configureAuthenticatedComponent = <TOwnProps extends {}>(WrappedComponent: ComponentType<TOwnProps>) => {
	return class AuthenticatedComponent extends Component<TOwnProps & AuthState & RouteComponentProps> {
		isNotAuthenticated() {
			const { isAuthenticating, isAuthenticated } = this.props
			return !isAuthenticating && !isAuthenticated
		}

		getRedirectQueryString() {
			const location = this.props.location
			const pathname = location.pathname
			let query = parse(this.props.location.search) || {}
			// do not overwrite the "returnUrl" when server redirects back to "/" with the OAuth Code or CAS Ticket
			// it will already be persisted
			if (pathname === '/' && (query.code || query.ticket)) {
				return `?${stringify(query)}`
			}
			query = _.assign(query, { returnUrl: pathname })
			return `?${stringify(query)}`
		}

		render() {
			if (this.isNotAuthenticated()) {
				return <Redirect to={`/login${this.getRedirectQueryString()}`} />
			}
			return <WrappedComponent {...this.props} />
		}
	}
}

export const configureMapStateToProps = () => (state: BaseReduxState) => {
	return {
		isAuthenticating: !state.auth.isAuthenticated && state.auth.isAuthenticating,
		isAuthenticated: state.auth.isAuthenticated
	}
}

export default function authenticatedComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>
): React.ComponentClass<Pick<RouteComponentProps<any, StaticContext, any>, never>, any> {
	const AuthenticatedComponent = configureAuthenticatedComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps()
	// @ts-ignore
	return withRouter(connect(mapStateToProps)(AuthenticatedComponent))
}
