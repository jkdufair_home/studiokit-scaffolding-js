import React, { Component, ComponentType } from 'react'
import ReactModal from 'react-modal'
import connectedModalComponent, { ConnectedModalWrappedProps } from './ConnectedModalComponent'

export interface FullscreenModalProps extends ConnectedModalWrappedProps {
	isOpen?: boolean
	closeModal?: () => void
	contentLabel?: string
}

export interface FullscreenModalWrappedProps extends FullscreenModalProps {
	closeModal: () => void
}

export interface FullscreenModalState {
	isOpen: boolean
}

export const configureFullscreenModalComponent = <TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & FullscreenModalWrappedProps>
) => {
	return class FullscreenModalComponent extends Component<TOwnProps & FullscreenModalProps, FullscreenModalState> {
		constructor(props: TOwnProps & FullscreenModalProps) {
			super(props)

			this.state = {
				isOpen: props.isOpen === undefined ? true : props.isOpen
			}

			// https://github.com/reactjs/react-modal#app-element
			// Accessibility fixed when reading the content in the modal
			if (!!document.getElementById('root')) {
				ReactModal.setAppElement('#root')
			}
		}

		componentDidMount() {
			const { isOpen } = this.state
			if (isOpen) {
				this.props.onEntering()
			}
		}

		componentDidUpdate(prevProps: TOwnProps & FullscreenModalProps, prevState: FullscreenModalState) {
			const { onEntering, onExiting } = this.props

			let isOpen: boolean | undefined

			if (this.props.isOpen !== undefined && this.props.isOpen !== this.state.isOpen) {
				// using props to handle isOpen, update state
				isOpen = this.props.isOpen
			} else if (this.props.isOpen === undefined && prevState.isOpen !== this.state.isOpen) {
				// using state only to handle isOpen
				isOpen = this.state.isOpen
			}

			if (isOpen === undefined) {
				return
			}

			if (isOpen) {
				onEntering()
			} else {
				onExiting()
			}
			this.setState({
				isOpen
			})
		}

		closeModal = () => {
			if (this.props.closeModal) {
				this.props.closeModal()
				return
			}
			this.setState({
				isOpen: false
			})
		}

		render() {
			const { contentLabel } = this.props
			const { isOpen } = this.state
			return (
				<ReactModal
					isOpen={isOpen}
					contentLabel={contentLabel}
					className="fullscreen-modal"
					overlayClassName="fullscreen-modal-overlay"
					shouldCloseOnOverlayClick={false}>
					<WrappedComponent {...this.props} closeModal={this.closeModal} />
				</ReactModal>
			)
		}
	}
}

export default function fullscreenModalComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & FullscreenModalWrappedProps>
) {
	const component = configureFullscreenModalComponent(WrappedComponent)
	return connectedModalComponent(component)
}
