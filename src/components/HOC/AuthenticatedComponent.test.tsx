import { shallow } from 'enzyme'
import React from 'react'
import { RouteComponentProps } from 'react-router'
import { DeepPartial } from 'redux'
import { AuthState } from 'studiokit-auth-js'
import { BaseReduxState } from '../../types'
import { configureAuthenticatedComponent, configureMapStateToProps } from './AuthenticatedComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (isAuthenticated: boolean, isAuthenticating: boolean) => {
	const AuthenticatedComponent = configureAuthenticatedComponent(TestComponent)
	const state = {
		auth: {
			isAuthenticated,
			isAuthenticating
		}
	} as BaseReduxState
	const mockProps: DeepPartial<RouteComponentProps> = {
		location: {
			pathname: '/'
		}
	}
	const mapStateToProps = configureMapStateToProps()
	const props = {
		foo: 'bar',
		...(mockProps as RouteComponentProps),
		...(mapStateToProps(state) as AuthState)
	}
	return shallow(<AuthenticatedComponent {...props} />)
}

describe('AuthenticatedComponent', () => {
	it('should render Redirect if isAuthenticated and isAuthenticating are both false', () => {
		const wrapper = setup(false, false)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
	it('should render TestComponent if isAuthenticated is true', () => {
		const wrapper = setup(true, false)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render TestComponent if isAuthenticating is true', () => {
		const wrapper = setup(false, true)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render TestComponent if isAuthenticated and isAuthenticating are both true', () => {
		const wrapper = setup(true, true)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
})
