import _ from 'lodash'
import React, { Component, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { ACTION } from '../../redux/actions'
import { BaseReduxState, Search } from '../../types'

export interface SearchPersistorProps {
	persistSearch: (search?: Search) => void
	persistedSearch: Search
}

interface SearchPersistorState {
	search: Search | undefined
	defaultSearch: Search | undefined
}

export interface SearchPersistorMethods {
	setSearchDefaults: (doSearch: () => void, search: Search) => void
	updateAndPersistSearch: (search?: Search, callback?: () => void) => void
	handleSearchClick: () => void
	handleChangeTab: (selectedTab: number) => void
	handleKeywordsChange: (event: any) => void
	handleKeywordsKeyDown: (event: any) => void
	handleQueryAllChange: (event: any) => void
	resetSearch: () => void
}

export interface SearchPersistorWrappedProps extends SearchPersistorProps, SearchPersistorMethods {
	search?: Search
}

export function configureSearchPersistorComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & SearchPersistorWrappedProps>
) {
	return class SearchPersistorComponent extends Component<TOwnProps & SearchPersistorProps, SearchPersistorState>
		implements SearchPersistorMethods {
		constructor(props: TOwnProps & SearchPersistorProps) {
			super(props)
			this.state = {
				search: undefined,
				defaultSearch: undefined
			}
		}

		// tslint:disable-next-line
		doSearch: () => void = () => {}

		componentDidMount() {
			const { persistedSearch } = this.props
			if (!!persistedSearch) {
				this.setState({ search: persistedSearch })
			}
		}

		//#region init and state updates

		setSearchDefaults = (doSearch: () => void, search: Search) => {
			this.doSearch = doSearch

			if (this.state.search === undefined) {
				this.setState({ search, defaultSearch: search }, doSearch)
			} else {
				this.setState(
					{
						defaultSearch: search
					},
					doSearch
				)
			}
		}

		updateAndPersistSearch = (search?: Search, callback?: () => void) => {
			this.setState(
				{
					search: _.merge({}, this.state.search, search)
				},
				() => {
					this.props.persistSearch(this.state.search)
					if (callback) {
						callback()
					}
				}
			)
		}

		//#endregion init and state updates

		//#region handlers

		handleSearchClick = () => {
			const { search } = this.state

			// keywords should be more than 2 characters
			if (!!search && !!search.keywords && search.keywords.length < 3) {
				this.updateAndPersistSearch({
					invalidKeywords: true
				})
				return
			}

			this.updateAndPersistSearch(
				{
					hasSearched: true,
					invalidKeywords: false
				},
				this.doSearch
			)
		}

		handleChangeTab = (selectedTab: number) => {
			this.updateAndPersistSearch({ selectedTab })
		}

		handleKeywordsChange = (event: any) => {
			this.updateAndPersistSearch({
				keywords: event.target.value
			})
		}

		handleKeywordsKeyDown = (event: any) => {
			if (event.key === 'Enter') {
				this.handleSearchClick()
			}
		}

		handleQueryAllChange = (event: any) => {
			const queryAll = event.target.checked
			this.updateAndPersistSearch({
				queryAll,
				requiredMessage: !queryAll ? null : this.state.search ? this.state.search.requiredMessage : null
			})
		}

		resetSearch = () => {
			this.updateAndPersistSearch(this.state.defaultSearch, this.doSearch)
		}

		//#endregion handlers

		render() {
			return (
				<WrappedComponent
					updateAndPersistSearch={this.updateAndPersistSearch}
					setSearchDefaults={this.setSearchDefaults}
					search={this.state.search}
					handleSearchClick={this.handleSearchClick}
					handleChangeTab={this.handleChangeTab}
					handleKeywordsChange={this.handleKeywordsChange}
					handleKeywordsKeyDown={this.handleKeywordsKeyDown}
					handleQueryAllChange={this.handleQueryAllChange}
					resetSearch={this.resetSearch}
					{...this.props}
				/>
			)
		}
	}
}

export const configureMapStateToProps = (key: string) => (state: BaseReduxState): SearchPersistorProps => {
	const search = state.search[key]
	if (!!search && !!search.date) {
		search.date = new Date(Date.parse(search.date))
	}
	return {
		persistedSearch: search
	} as SearchPersistorProps
}

export const configureMapDispatchToProps = (key: string) => (dispatch: Dispatch): SearchPersistorProps => {
	let searchDebounce: any
	const dispatchPersistSearch = (search: Search) => {
		dispatch({
			type: ACTION.PERSIST_SEARCH,
			data: {
				[`${key}`]: search
			}
		})
	}
	const persistSearchDebounce = (search: Search) => {
		if (!!searchDebounce) {
			clearTimeout(searchDebounce)
		}
		searchDebounce = setTimeout(() => dispatchPersistSearch(search), 1000)
	}

	return {
		persistSearch: persistSearchDebounce
	} as SearchPersistorProps
}

/**
 * HOC to handle persistence and restoration of "Manage" components search parameters.
 * Note: if you store a search date in a child component, make sure it’s called `date`.
 * Rehydration of dates doesn’t work as expected out of the box with redux-persist.
 * Something more generic might be in order in the future.
 *
 * Note: this also uses the `configureX` pattern of declaration for testability.
 *
 * API for child components:
 *  - Call `setSearchDefaults(this.doSearch, this.getDefaultSearch())` in child’s `componentDidMount`
 *  - Implement `getDefaultSearch`
 *  - Call this HOC’s handlers as needed
 *  - Implement any additional child-specific handlers and call this HOC’s `updateAndPersistSearch` in them
 *  - Make sure your `doSearch` and `render` handle undefined search props
 *  - Avoid intoxicating substances and wash your hands regularly
 *
 * @param WrappedComponent The component to wrap
 * @param key The key under "search" in redux where the component’s search params will be stored
 * @returns The wrapped component, passing down `search` and `persistSearch` as props
 */
export default function searchPersistorComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & SearchPersistorWrappedProps>,
	key: string
) {
	const SearchPersistorComponent = configureSearchPersistorComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps(key)
	const mapDispatchToProps = configureMapDispatchToProps(key)
	return connect(
		mapStateToProps,
		mapDispatchToProps
		// @ts-ignore
	)(SearchPersistorComponent)
}
