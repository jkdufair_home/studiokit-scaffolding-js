import { shallow } from 'enzyme'
import React from 'react'
import { BaseReduxState } from '../../types'
import { configureDataDependentComponent, configureMapStateToProps } from './DataDependentComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (hasData: boolean) => {
	const DataDependentTestComponent = configureDataDependentComponent(TestComponent)
	const mapStateToProps = configureMapStateToProps(
		jest.fn().mockImplementation((state: any) => {
			return hasData
		})
	)
	const ownProps: TestComponentProps = {
		foo: 'bar'
	}
	const props = {
		...ownProps,
		...mapStateToProps({} as BaseReduxState)
	}
	return shallow(<DataDependentTestComponent {...props} />)
}

describe('DataDependentComponent', () => {
	it('should render TestComponent if hasData returns true', () => {
		const wrapper = setup(true)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render Loading if hasData returns false', () => {
		const wrapper = setup(false)
		expect(wrapper.find('Loading').length).toEqual(1)
	})
})
