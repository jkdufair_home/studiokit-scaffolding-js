import CircularProgress from '@material-ui/core/CircularProgress'
import React, { FunctionComponent } from 'react'

export interface RefreshIndicatorProps {
	label?: string
	labelClassName?: string
}

const CustomRefreshIndicator: FunctionComponent<RefreshIndicatorProps> = ({ label, labelClassName }) => {
	return (
		<div>
			<CircularProgress
				size={40}
				style={{
					display: 'inherit',
					position: 'relative',
					margin: '2rem auto 1rem'
				}}
			/>
			{label && (
				<p style={{ textAlign: 'center' }} className={`i f4${!!labelClassName ? ` ${labelClassName}` : ''}`}>
					{label}
				</p>
			)}
		</div>
	)
}

export default CustomRefreshIndicator
