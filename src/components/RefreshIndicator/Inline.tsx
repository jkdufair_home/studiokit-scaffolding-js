import RefreshIndicator, { CircularProgressProps } from '@material-ui/core/CircularProgress'
import React, { FunctionComponent } from 'react'

const RefreshIndicatorInline: FunctionComponent<CircularProgressProps> = props => {
	return (
		<RefreshIndicator
			{...props}
			size={35}
			style={{
				position: 'relative'
			}}
			className={`dib${props.className ? ` ${props.className}` : ''}`}
		/>
	)
}

export default RefreshIndicatorInline
