import React, { FunctionComponent } from 'react'
import RefreshIndicator, { RefreshIndicatorProps } from './index'

const RefreshIndicatorBordered: FunctionComponent<RefreshIndicatorProps> = props => {
	return (
		<div className="ba b--dashed bw1 b--custom-blue">
			<RefreshIndicator {...props} />
		</div>
	)
}

export default RefreshIndicatorBordered
