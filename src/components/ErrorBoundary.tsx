import * as Sentry from '@sentry/browser'
import React, { ErrorInfo, PureComponent } from 'react'
import { Col, Grid, Row } from 'react-bootstrap'

interface ErrorBoundaryState {
	error?: Error
	errorInfo?: ErrorInfo
}

export default class ErrorBoundary extends PureComponent<{}, ErrorBoundaryState> {
	constructor(props: {}) {
		super(props)
		this.state = { error: undefined, errorInfo: undefined }
	}

	componentDidCatch(error: Error, errorInfo: ErrorInfo) {
		// Catch errors in any components below and re-render with error message
		this.setState({
			error,
			errorInfo
		})
		// You can also log error messages to an error reporting service here
		Sentry.captureException(new Error(error.toString()))
	}

	render() {
		if (!!this.state.error && !!this.state.errorInfo) {
			return (
				<>
					<Grid className="mt3">
						<Row>
							<Col md={8}>
								<h2>Oops! Something went wrong while processing your request.</h2>
								<p>
									Please refresh the page and try again. If you have any questions or concerns please
									contact us at <a href="mailto:tlt@purdue.edu">tlt@purdue.edu</a>
								</p>
								<details style={{ whiteSpace: 'pre-wrap' }}>
									{this.state.error.toString()}
									<br />
									{this.state.errorInfo.componentStack}
								</details>
							</Col>
						</Row>
					</Grid>
				</>
			)
		}
		// Normally, just render children
		return this.props.children
	}
}
