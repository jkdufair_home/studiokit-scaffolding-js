import { shallow } from 'enzyme'
import React from 'react'
import { RouteComponentProps } from 'react-router'
import { setAppConfig } from '../constants'
import { AppConfiguration, BaseReduxState, Client } from '../types'
import { mapStateToProps, NewVersionAlert, NewVersionAlertProps } from './NewVersionAlert'

const setup = (client: Client) => {
	const mockState: Partial<BaseReduxState> = {
		models: {
			client
		}
	}
	const mockProps = {
		location: {
			pathname: '/'
		}
	} as RouteComponentProps
	const props = Object.assign({}, mockProps, mapStateToProps(mockState as BaseReduxState))
	return shallow<NewVersionAlert, NewVersionAlertProps & RouteComponentProps>(<NewVersionAlert {...props} />)
}

describe('NewVersionAlert', () => {
	beforeAll(() => {
		const appConfig = {
			VERSION: '1.5.0'
		} as AppConfiguration
		setAppConfig(appConfig)
	})
	it('should show update alert if version is greater', () => {
		const wrapper = setup({
			clientId: 'web',
			currentVersion: '1.6.0'
		})
		expect(wrapper.find({ id: 'newVersionAlert' }).prop('open')).toEqual(true)
	})
	it('should not show update alert if version is equal', () => {
		const wrapper = setup({
			clientId: 'web',
			currentVersion: '1.5.0'
		})
		expect(wrapper.find({ id: 'newVersionAlert' }).prop('open')).toEqual(false)
	})
	it('should not show update alert if version is less', () => {
		const wrapper = setup({
			clientId: 'web',
			currentVersion: '1.4.0'
		})
		expect(wrapper.find({ id: 'newVersionAlert' }).prop('open')).toEqual(false)
	})
	describe('refresh', () => {
		let reload: () => void
		beforeAll(() => {
			reload = document.location.reload
			document.location.reload = jest.fn()
		})
		afterAll(() => {
			document.location.reload = reload
		})
		it('is called on pathname change if hasVersionUpdate = true', () => {
			const wrapper = setup({
				clientId: 'web',
				currentVersion: '1.6.0'
			})
			const instance = wrapper.instance()
			const spy = jest.spyOn(instance, 'refresh')
			wrapper.setProps({
				location: {
					pathname: '/help'
				}
			} as RouteComponentProps)
			expect(spy).toHaveBeenCalledTimes(1)
		})
		it('is not called on pathname change if hasVersionUpdate = false', () => {
			const wrapper = setup({
				clientId: 'web',
				currentVersion: '1.5.0'
			})
			const instance = wrapper.instance()
			const spy = jest.spyOn(instance, 'refresh')
			wrapper.setProps({
				location: {
					pathname: '/help'
				}
			} as RouteComponentProps)
			expect(spy).toHaveBeenCalledTimes(0)
		})
	})
})
