import Button from '@material-ui/core/Button'
import IconReset from '@material-ui/icons/Autorenew'
import IconSearch from '@material-ui/icons/Search'
import React, { FunctionComponent } from 'react'
import { Alert, Checkbox, ControlLabel, FormControl, FormGroup, HelpBlock } from 'react-bootstrap'
import { Search } from '../types'
import DateField from './Forms/DateField'
import { SearchPersistorMethods } from './HOC/SearchPersistorComponent'

export interface SearchControlsProps extends Search, SearchPersistorMethods {
	keywordPlaceholder: string
	queryAllText: string
	handleDateChange: (dateString: string, date?: Date) => void
	canReadAnyGlobally: boolean
	canReadSome: boolean
}

const SearchControls: FunctionComponent<SearchControlsProps> = ({
	keywords,
	invalidKeywords,
	dateString,
	queryAll,
	requiredMessage,
	handleKeywordsChange,
	handleKeywordsKeyDown,
	handleSearchClick,
	resetSearch,
	handleQueryAllChange,
	keywordPlaceholder,
	handleDateChange,
	queryAllText,
	canReadAnyGlobally,
	canReadSome
}) => {
	return (
		<>
			{!!requiredMessage && (
				<Alert bsStyle="danger" className="mb0" id="requiredMessageAlert">
					<h4 className="f6 ma0">{requiredMessage}</h4>
				</Alert>
			)}
			<FormGroup
				controlId="keywords"
				className="fl-ns mw5-ns mr3-ns mb3 mb0-ns full-width-xxs"
				validationState={invalidKeywords ? 'warning' : null}>
				<ControlLabel className="f7">Keywords</ControlLabel>
				<FormControl
					type="text"
					placeholder={keywordPlaceholder}
					name="keywords"
					autoFocus
					onChange={handleKeywordsChange}
					onKeyDown={handleKeywordsKeyDown}
					value={keywords}
				/>
				{invalidKeywords && (
					<HelpBlock className="i f6" id="invalidKeywords">
						Please enter at least 3 characters.
					</HelpBlock>
				)}
			</FormGroup>
			{!!handleDateChange && (
				<>
					<span className="i mw3-ns mr3-ns mt3-ns pv3-ns fl-ns tc full-width-xxs">and / or</span>
					<DateField
						id="date"
						onChange={handleDateChange}
						label="Date"
						aria-label="Active date"
						className="fl-ns mw5-ns mr3-ns mb3 mb0-ns full-width-xxs"
						value={dateString}
					/>
				</>
			)}
			<div className="fl-ns mt3-ns pt2-ns full-width-xxs">
				{canReadAnyGlobally && !!handleQueryAllChange && (
					<Checkbox
						name="queryAll"
						id="queryAll"
						checked={queryAll}
						readOnly={canReadAnyGlobally && !canReadSome}
						onChange={handleQueryAllChange}
						className="fl-ns f6 mr2-ns mb3 full-width-xxs">
						<span className="b black">{queryAllText}</span>
					</Checkbox>
				)}
				<Button
					color="primary"
					className="btn btn-primary fr-ns ml2-ns mb3 full-width-xxs"
					onClick={handleSearchClick}>
					<IconSearch className="fill-white v-mid" />
					Search
				</Button>
				<Button className="btn btn-text color-pink fr-ns mb3 full-width-xxs" onClick={resetSearch}>
					<IconReset className="fill-pink v-mid" />
					Reset
				</Button>
			</div>
		</>
	)
}
export default SearchControls
