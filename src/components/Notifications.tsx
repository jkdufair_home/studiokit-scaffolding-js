import { Button, Snackbar, SnackbarContent } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NOTIFICATION_TYPE } from '../constants'
import { dispatchAction } from '../redux/actionCreator'
import { ACTION } from '../redux/actions'
import { ModalsState } from '../redux/reducers/modalsReducer'
import { NotificationState } from '../redux/reducers/notificationsReducer'
import { Notification } from '../types'

export interface NotificationsProps {
	notifications: Notification[]
	isModalShowing: boolean
}

interface NotificationsState {
	notification?: Notification
	className: string
	isOpen: boolean
}

export class Notifications extends Component<NotificationsProps, NotificationsState> {
	afterCloseTimeout: any = undefined

	constructor(props: NotificationsProps) {
		super(props)
		this.state = {
			notification: undefined,
			className: '',
			isOpen: false // use to handle animation separate from existence of notification
		}
	}

	componentDidMount() {
		this.updateNotification()
	}

	componentDidUpdate(prevProps: NotificationsProps) {
		this.updateNotification()
	}

	componentWillUnmount() {
		const { notifications } = this.props
		const { isOpen } = this.state
		if (isOpen && notifications.length > 0) {
			this.removeNotification()
		}
		this.clearAfterCloseTimeout()
	}

	clearAfterCloseTimeout = () => {
		if (!!this.afterCloseTimeout) {
			clearTimeout(this.afterCloseTimeout)
			this.afterCloseTimeout = undefined
		}
	}

	updateNotification = () => {
		const { isModalShowing, notifications } = this.props
		const { isOpen } = this.state
		if (!isModalShowing && !isOpen && notifications.length > 0) {
			const notification = notifications[0]
			this.setState({
				notification,
				className: this.classNameForType(notification.type),
				isOpen: true
			})
		} else if (isOpen && notifications.length === 0) {
			this.setState(
				{
					isOpen: false
				},
				() => {
					// wait until after Snackbar hide animation to clear the UI
					this.afterCloseTimeout = setTimeout(() => {
						this.clearAfterCloseTimeout()
						this.setState({
							notification: undefined,
							className: ''
						})
					}, 300)
				}
			)
		}
	}

	removeNotification = () => {
		const { notification } = this.state
		if (!notification) {
			return
		}
		dispatchAction(ACTION.REMOVE_NOTIFICATION, {
			notification
		})
	}

	classNameForType = (type: NOTIFICATION_TYPE) => {
		switch (type) {
			case NOTIFICATION_TYPE.INFO:
				return 'bg-info alert-info'
			case NOTIFICATION_TYPE.SUCCESS:
				return 'bg-success alert-success'
			case NOTIFICATION_TYPE.WARNING:
				return 'bg-warning alert-warning'
			case NOTIFICATION_TYPE.ERROR:
				return 'bg-danger alert-danger'
		}
	}

	render() {
		const { notification, className, isOpen } = this.state
		return (
			<div id="notifications" aria-live="polite">
				<Snackbar
					id="notificationAlert"
					anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
					open={isOpen}
					autoHideDuration={8000}
					onClose={this.removeNotification}
					className="mh2 alert">
					<SnackbarContent
						className={className}
						aria-describedby="notificationAlertMessage"
						message={
							<span id="notificationAlertMessage">
								<h4 className="normal pt2">{!!notification && notification.text}</h4>
							</span>
						}
						action={[
							<Button
								id="notificationAlertCloseButton"
								aria-label="Close"
								color="inherit"
								key="close"
								onClick={this.removeNotification}>
								<IconClose className="v-mid" />
								Close
							</Button>
						]}
					/>
				</Snackbar>
			</div>
		)
	}
}

export const mapStateToProps = (state: { notifications: NotificationState; modals: ModalsState }) => {
	return {
		notifications: state.notifications.queue,
		isModalShowing: Object.keys(state.modals).length > 0
	}
}

export default connect(mapStateToProps)(Notifications)
