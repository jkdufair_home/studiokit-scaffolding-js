import { Modal } from 'react-bootstrap'
import ConnectedModalComponent from './HOC/ConnectedModalComponent'

/**
 * Wrapped version of "react-bootstrap"'s `Modal` component. Uses `ConnectedModalComponent` to coordinate modal state in redux.
 */
// @ts-ignore
export default ConnectedModalComponent(Modal)
