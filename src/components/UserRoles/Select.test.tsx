import { shallow } from 'enzyme'
import React from 'react'
import { OverlayTriggerProps } from 'react-bootstrap'
import ROLE from '../../constants/role'
import RoleSelect, { RoleSelectProps } from './Select'

describe('RoleSelect', () => {
	let props: RoleSelectProps
	beforeEach(() => {
		props = {
			className: 'some-class',
			value: ROLE.GROUP_OWNER,
			options: [ROLE.GROUP_OWNER, ROLE.GROUP_GRADER, ROLE.GROUP_LEARNER],
			onChange: jest.fn()
		}
	})
	it('uses the default control Id label if not passed', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.prop('controlId')).toEqual('RoleSelect')
	})
	it('uses the passed in control Id', () => {
		props.controlId = 'testId'
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.prop('controlId')).toEqual('testId')
	})
	it('renders options', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('option').length).toEqual(3)
	})
	it('sets value', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('FormControl').prop('value')).toEqual(ROLE.GROUP_OWNER)
	})
	it('adds className', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('FormControl').prop('className')).toEqual('some-class')
	})
	it('calls onChange', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		wrapper.find('FormControl').simulate('change', { target: { value: ROLE.GROUP_GRADER } })
		expect(props.onChange.mock.calls.length).toEqual(1)
	})
	it('uses a label with multiple options', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('ControlLabel').length).toEqual(1)
	})
	it('renders form group and label as a span with one option', () => {
		props.options = [ROLE.GROUP_OWNER]
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('span').length).toEqual(2)
	})
	it('renders an OverlayTrigger and a help icon if popover content is provided', () => {
		props.labelVisible = true
		props.popoverContentComponent = <p>Content</p>
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('OverlayTrigger').length).toEqual(1)
		// Child of <p> element is the contained text
		expect((wrapper.find('OverlayTrigger').props() as OverlayTriggerProps).overlay.props.children).toEqual(
			'Content'
		)
		expect(wrapper.find('ControlLabel').props().className).not.toEqual(expect.stringContaining('visually-hidden'))
		expect(
			wrapper
				.find('ControlLabel')
				.render()
				.find('svg').length
		).toEqual(1)
	})
	it('does not render an OverlayTrigger or help icon if no content is provided', () => {
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('OverlayTrigger').length).toEqual(0)
		expect(wrapper.find('ControlLabel').props().className).toEqual(expect.stringContaining('visually-hidden'))
		expect(
			wrapper
				.find('ControlLabel')
				.render()
				.find('svg').length
		).toEqual(0)
	})
	it('renders as static div if only one option', () => {
		props.options = [ROLE.GROUP_OWNER]
		const wrapper = shallow(<RoleSelect {...props} />)
		expect(wrapper.find('option').length).toEqual(0)
		expect(wrapper.find('div').length).toEqual(1)
	})
})
