import { Button, TableCell, TableRow } from '@material-ui/core'
import IconDelete from '@material-ui/icons/Delete'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BaseReduxState, User, UserWithDefaultRole } from '../../types'
import { textForRole } from '../../utils/role'
import RoleSelect from './Select'

export interface UserRolesTableEditProps {
	readOnly: boolean
	canModifySelf: boolean
	/**
	 * Array of the roles available in text form, i.e. 'Group Owner'
	 * Required if not readOnly
	 */
	roles?: string[]
	swapUserRoles?: (user: UserWithDefaultRole, newRole: string, role: string) => void
	removeUserFromRole?: (user: UserWithDefaultRole) => void
}

export interface UserRolesTableRowProps extends UserRolesTableEditProps {
	user: UserWithDefaultRole
	id: string
}

interface UserRolesTableRowConnectedProps {
	currentUserId: string
}

export class UserRolesTableRow extends Component<UserRolesTableRowProps & UserRolesTableRowConnectedProps> {
	changeRole = (e: any) => {
		const { user, swapUserRoles } = this.props
		const newRole = e.target.value as string
		if (!swapUserRoles) {
			throw new Error(`swapUserRoles is undefined`)
		}
		swapUserRoles(user, newRole, user.defaultRole)
	}

	removeUserFromRole = () => {
		const { user, removeUserFromRole } = this.props
		if (!removeUserFromRole) {
			throw new Error(`removeUserFromRole is undefined`)
		}
		removeUserFromRole(user)
	}

	render() {
		const { user, currentUserId, readOnly, canModifySelf, roles } = this.props
		return (
			<TableRow>
				<TableCell className="pv0 ph3 first-name">{!!user.firstName ? user.firstName : ''}</TableCell>
				<TableCell className="pv0 ph3 last-name">{!!user.lastName ? user.lastName : ''}</TableCell>
				<TableCell className="pv0 ph3 email">{user.email}</TableCell>
				{!readOnly ? (
					<TableCell className="pv0 ph3">
						{user.id !== currentUserId || canModifySelf ? (
							<RoleSelect
								controlId={`RoleSelectRow${user.id}`}
								className="mt3"
								options={!!roles ? roles : []}
								value={user.defaultRole}
								onChange={this.changeRole}
							/>
						) : (
							textForRole(user.defaultRole)
						)}
					</TableCell>
				) : (
					<TableCell className="pv0 ph3">textForRole(user.defaultRole)</TableCell>
				)}
				{!readOnly && (
					<TableCell className="pv0 ph3 tc">
						{(user.id !== currentUserId || canModifySelf) && (
							<Button
								className="color-red removeUserButton"
								aria-label="Remove user"
								onClick={this.removeUserFromRole}>
								<IconDelete className="fill-red v-mid" />
								Remove
							</Button>
						)}
					</TableCell>
				)}
			</TableRow>
		)
	}
}

export const mapStateToProps = (state: BaseReduxState) => {
	if (!state.models.user || !state.models.user.userInfo) {
		throw new Error('Current user id is not stored in redux')
	}
	return {
		currentUserId: state.models.user.userInfo.id
	}
}

export default connect(mapStateToProps)(UserRolesTableRow)
