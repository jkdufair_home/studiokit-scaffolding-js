import IconHelp from '@material-ui/icons/Help'
import React, { ReactNode } from 'react'
import { ControlLabel, FormControl, FormGroup, OverlayTrigger } from 'react-bootstrap'
import { textForRole } from '../../utils/role'

const label = (hasMultipleOptions: boolean, labelVisible: boolean, hasTooltip: boolean) => {
	const content = (
		<>
			Access Level
			{hasTooltip && <IconHelp className="ml1 v-mid fill-blue" tabIndex={0} />}
		</>
	)

	const classes = `f6 ${!labelVisible ? 'visually-hidden' : ''}`

	return hasMultipleOptions ? (
		<ControlLabel className={classes}>{content}</ControlLabel>
	) : (
		<span className={classes + ' b'}>{content}</span>
	)
}

const overlay = (hasMultipleOptions: boolean, labelVisible: boolean = false, accessLevelPopover?: ReactNode) =>
	!!accessLevelPopover ? (
		<OverlayTrigger placement="right" trigger={['click', 'hover', 'focus']} overlay={accessLevelPopover}>
			{label(hasMultipleOptions, labelVisible, true)}
		</OverlayTrigger>
	) : (
		label(hasMultipleOptions, labelVisible, false)
	)

const selector = (options: string[], value: string, onChange: any, className?: string) =>
	options.length > 1 ? (
		<FormControl
			className={!!className ? className : ''}
			aria-label="Access Level"
			componentClass="select"
			placeholder="select"
			onChange={onChange}
			value={value}>
			{options.map((role, i) => (
				<option value={role} key={i}>
					{textForRole(role)}
				</option>
			))}
		</FormControl>
	) : (
		<div>{textForRole(options[0])}</div>
	)

export interface RoleSelectProps {
	className?: string
	options: string[]
	value: string
	onChange: any
	/* controlId = 'RoleSelect' */
	controlId?: string
	/* labelVisible = false */
	labelVisible?: boolean
	popoverContentComponent?: ReactNode
}

/**
 * Create a selector which will be properly labeled based on the number of options and provided content.
 *
 * @param {string} className - CSS class passed to the selector FormControl component
 * @param {string[]} options - Array of options the user can choose from
 * @param {string} value - Currently selected value
 * @param {function} onChange - Function that is called when the value changes
 * @param {string} controlId - Id for the selector
 * @param {boolean} labelVisible - Whether the label text should be visible
 * @param {ReactNode} popoverContentComponent - Content of the popover tooltip
 *
 * @returns {ReactElement} Selector and label component
 */
const RoleSelect = (props: RoleSelectProps) => {
	const { className, options, value, onChange, controlId, labelVisible, popoverContentComponent } = props
	const content = (
		<>
			{overlay(options.length > 1, !!labelVisible, popoverContentComponent)}
			{selector(options, value, onChange, className)}
		</>
	)

	return options.length > 1 ? (
		<FormGroup controlId={!!controlId ? controlId : 'RoleSelect'}>{content}</FormGroup>
	) : (
		<span>{content}</span>
	)
}

export default RoleSelect
