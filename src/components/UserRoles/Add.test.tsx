import { shallow } from 'enzyme'
import React from 'react'
import { setAppConfig } from '../../constants'
import { defaultAppConfiguration } from '../../constants/mockData'
import ROLE from '../../constants/role'
import { isPurdueShard } from '../../utils/shard'
import UserRolesAdd, { UserRolesAddProps } from './Add'

jest.mock('../../utils/shard', () => {
	return {
		isPurdueShard: jest.fn().mockImplementation(() => true)
	}
})

function setup(props?: Partial<UserRolesAddProps>) {
	const defaultProps = {
		entityName: 'course',
		addUsersToRole: jest.fn(),
		shouldReset: false,
		isAddingUsersToRole: false,
		defaultRole: ROLE.GROUP_LEARNER,
		roleDescriptions: {
			[ROLE.GROUP_OWNER]: 'some description',
			[ROLE.GROUP_GRADER]: 'some description',
			[ROLE.GROUP_LEARNER]: 'some description'
		}
	}
	const finalProps = Object.assign({}, defaultProps, props)
	return shallow<UserRolesAdd>(<UserRolesAdd {...finalProps as UserRolesAddProps} />)
}

describe('UserRolesAdd', () => {
	beforeAll(() => {
		setAppConfig(defaultAppConfiguration)
	})
	it('renders with correct state, description', () => {
		const wrapper = setup()
		expect(wrapper.find('Row').length).toEqual(1)
		expect(wrapper.state()).toEqual({
			identifiers: '',
			role: ROLE.GROUP_LEARNER,
			showErrorAlert: false,
			errorMessage: undefined
		})
		const p = wrapper.find('p').first()
		expect(p.text()).toEqual(
			"To add people to this course, enter their Purdue career account alias or PUID, select their access level for this course, and click 'Add.'"
		)
	})
	it('resets state using props.shouldReset', () => {
		const wrapper = setup()
		wrapper.setState({
			identifiers: 'wgrauvog'
		})
		expect(wrapper.state()).toEqual({
			identifiers: 'wgrauvog',
			role: ROLE.GROUP_LEARNER,
			showErrorAlert: false,
			errorMessage: undefined
		})
		wrapper.setProps({
			shouldReset: true
		})
		expect(wrapper.state()).toEqual({
			identifiers: '',
			role: ROLE.GROUP_LEARNER,
			showErrorAlert: false,
			errorMessage: undefined
		})
	})
	it('renders description without props.entityName', () => {
		const wrapper = setup()
		wrapper.setProps({ entityName: undefined })
		const p = wrapper.find('p').first()
		expect(p.text()).toEqual(
			"To add people, enter their Purdue career account alias or PUID, select their access level, and click 'Add.'"
		)
	})
	it('shows FERPA warning', () => {
		const wrapper = setup()
		// simulate changing the current RoleSelect option
		wrapper.setState({
			role: ROLE.GROUP_OWNER
		})
		expect(wrapper.find({ id: 'ferpaWarning' }).length).toEqual(1)
	})
	it('shows Add Button if not adding', () => {
		const wrapper = setup()
		expect(wrapper.find({ id: 'userRolesAddButton' }).length).toEqual(1)
	})
	it('hides Add Button if adding and shows RefreshIndicatorInline', () => {
		const wrapper = setup({
			isAddingUsersToRole: true
		})
		expect(wrapper.find({ id: 'userRolesAddButton' }).length).toEqual(0)
		expect(wrapper.find('RefreshIndicatorInline').length).toEqual(1)
	})
	describe('functions', () => {
		let wrapper: any
		let instance: UserRolesAdd
		beforeEach(() => {
			wrapper = setup()
			instance = wrapper.instance()
		})
		describe('handleSubmit', () => {
			it('shows error message if invalid', () => {
				wrapper.setState({
					identifiers: 'wgrauvog\r\njase\r\n0'
				})
				instance.handleSubmit()
				expect(wrapper.state('showErrorAlert')).toEqual(true)
				expect(wrapper.state('errorMessage')).toBeTruthy()
			})
			it('calls props.addUsersToRole if valid', () => {
				const addUsersToRole = jest.fn()
				wrapper = setup({
					addUsersToRole
				})
				instance = wrapper.instance()
				wrapper.setState({
					identifiers: 'wgrauvog\r\njase'
				})
				instance.handleSubmit()
				expect(addUsersToRole.mock.calls.length).toEqual(1)
			})
		})
		describe('closeErrorAlert', () => {
			it('closes the alert and clears the errorMessage', () => {
				wrapper.setState({
					showErrorAlert: true,
					errorMessage: 'some message'
				})
				expect(wrapper.state()).toEqual({
					identifiers: '',
					role: ROLE.GROUP_LEARNER,
					showErrorAlert: true,
					errorMessage: 'some message'
				})
				instance.closeErrorAlert()
				expect(wrapper.state()).toEqual({
					identifiers: '',
					role: ROLE.GROUP_LEARNER,
					showErrorAlert: false,
					errorMessage: undefined
				})
			})
		})
		describe('updateIdentifiers', () => {
			it('succeeds', () => {
				expect(wrapper.state('identifiers')).toEqual('')
				instance.updateIdentifiers({ target: { value: 'wgrauvog\r\njase' } })
				expect(wrapper.state('identifiers')).toEqual('wgrauvog\r\njase')
			})
		})
		describe('updateRole', () => {
			it('succeeds', () => {
				expect(wrapper.state('role')).toEqual(ROLE.GROUP_LEARNER)
				instance.updateRole({ target: { value: ROLE.GROUP_OWNER } })
				expect(wrapper.state('role')).toEqual(ROLE.GROUP_OWNER)
			})
		})
		describe('getValidationErrorMessage', () => {
			describe('Purdue', () => {
				it('should return empty warning', () => {
					const response = 'Please enter at least one Purdue career account alias or PUID.'
					expect(instance.getValidationErrorMessage()).toEqual(response)
					expect(instance.getValidationErrorMessage([])).toEqual(response)
				})
				it('should return undefined with valid PUIDs and aliases', () => {
					expect(instance.getValidationErrorMessage(['wgrauvog', 'jase', '0000000000'])).toBeUndefined()
				})
				it('should return message with invalid identifiers', () => {
					expect(instance.getValidationErrorMessage(['wgrauvog', 'jase', 'x'])).toEqual(
						`Please enter valid Purdue career account aliases or PUIDs. The following are invalid:\r\nx`
					)
				})
			})
			describe('Not Purdue', () => {
				beforeAll(() => {
					;(isPurdueShard as any).mockImplementation(() => false)
				})
				it('should return empty warning', () => {
					const response = 'Please enter at least one Email Address.'
					expect(instance.getValidationErrorMessage()).toEqual(response)
					expect(instance.getValidationErrorMessage([])).toEqual(response)
				})
				it('should return undefined with valid emails', () => {
					expect(
						instance.getValidationErrorMessage(['wgrauvog@purdue.edu', 'jase@gmail.com'])
					).toBeUndefined()
				})
				it('should return message with invalid email addresses', () => {
					expect(instance.getValidationErrorMessage(['wgrauvog', 'jase', 'x'])).toEqual(
						`Please enter valid Email Addresses. The following are invalid:\r\nwgrauvog\r\njase\r\nx`
					)
				})
			})
		})
	})
})
