import { shallow } from 'enzyme'
import React from 'react'
import { defaultGroupLearner, defaultGroupUsers } from '../../constants/mockData'
import ROLE from '../../constants/role'
import UserRolesTable, { TABLE_COLUMN, TABLE_ORDER, UserRolesTableProps } from './Table'

const roles = [ROLE.GROUP_OWNER, ROLE.GROUP_GRADER, ROLE.GROUP_LEARNER]

const setup = (props?: Partial<UserRolesTableProps>) => {
	const mockProps = Object.assign(
		{
			users: defaultGroupUsers,
			roles
		},
		props
	)
	return shallow(<UserRolesTable {...mockProps as UserRolesTableProps} />)
}

describe('UserRolesTable', () => {
	it('renders, sorted by lastName asc', () => {
		const wrapper = setup()
		expect(wrapper.find('Connect(UserRolesTableRow)').length).toEqual(3)
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.LAST_NAME,
			order: TABLE_ORDER.ASC
		})
	})
	it('hides Remove col if readOnly', () => {
		const wrapper = setup({
			readOnly: true
		})
		expect(wrapper.find('WithStyles(TableCell)').length).toEqual(4)
	})
	it('cycles sort and updates label from header button clicks', () => {
		const wrapper = setup()
		// default, lastName asc
		let button = wrapper.find('WithStyles(Button)').first()
		expect(button.prop('aria-label')).toEqual('Sort by First Name Ascending')
		// click => firstName asc
		button.simulate('click')
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.FIRST_NAME,
			order: TABLE_ORDER.ASC
		})
		button = wrapper.find('WithStyles(Button)').first()
		expect(button.prop('aria-label')).toEqual('Sort by First Name Descending')
		// click 2 => firstName desc
		button.simulate('click')
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.FIRST_NAME,
			order: TABLE_ORDER.DESC
		})
		button = wrapper.find('WithStyles(Button)').first()
		expect(button.prop('aria-label')).toEqual('Sort by Default')
		// click 3 => default
		button.simulate('click')
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.LAST_NAME,
			order: TABLE_ORDER.ASC
		})
		button = wrapper.find('WithStyles(Button)').first()
		expect(button.prop('aria-label')).toEqual('Sort by First Name Ascending')
	})
	describe('returns to default sort if prop.users change', () => {
		const wrapper = setup()
		// default
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.LAST_NAME,
			order: TABLE_ORDER.ASC
		})
		// change => firstName desc
		const button = wrapper.find('WithStyles(Button)').first()
		button.simulate('click')
		button.simulate('click')
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.FIRST_NAME,
			order: TABLE_ORDER.DESC
		})
		// change prop.users
		wrapper.setProps({ users: [defaultGroupLearner] })
		// default again
		expect(wrapper.state()).toEqual({
			orderBy: TABLE_COLUMN.LAST_NAME,
			order: TABLE_ORDER.ASC
		})
	})
	describe('getLabel', () => {
		it('returns undefined if col const not found', () => {
			const wrapper = setup()
			const instance = wrapper.instance() as UserRolesTable
			expect(instance.getLabel('foo')).toBeUndefined()
		})
	})
})
