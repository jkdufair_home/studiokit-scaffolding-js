import { shallow } from 'enzyme'
import _ from 'lodash'
import React from 'react'
import { defaultGroupOwner, defaultNamelessGroupLearner, mockUser } from '../../constants/mockData'
import ROLE from '../../constants/role'
import { BaseReduxState, UserInfo } from '../../types'
import { setDefaultRole } from '../../utils/userRoles'
import { mapStateToProps, UserRolesTableRow, UserRolesTableRowProps } from './TableRow'

const roles = [ROLE.GROUP_OWNER, ROLE.GROUP_GRADER, ROLE.GROUP_LEARNER]

const setup = (user: Partial<UserInfo>, props: Partial<UserRolesTableRowProps>) => {
	const mockState = {
		models: {
			user: {
				userInfo: user
			}
		}
	} as Partial<BaseReduxState>
	const mockProps = Object.assign(
		{
			roles,
			user
		},
		props
	) as UserRolesTableRowProps
	const finalProps = Object.assign({}, mockProps, mapStateToProps(mockState as BaseReduxState))
	return shallow(<UserRolesTableRow {...finalProps} />)
}

describe('UserRolesTableRow', () => {
	it('displays name correctly with firstName and lastName', () => {
		const wrapper = setup(mockUser().userInfo, { user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER) })
		expect(
			wrapper
				.find('.first-name')
				.render()
				.text()
		).toBe('Jane')
		expect(
			wrapper
				.find('.last-name')
				.render()
				.text()
		).toBe('Doe')
		expect(
			wrapper
				.find('.email')
				.render()
				.text()
		).toBe('jane@gmail.com')
	})
	it('displays name correctly without firstName and lastName', () => {
		const wrapper = setup(mockUser().userInfo, {
			user: setDefaultRole(defaultNamelessGroupLearner, ROLE.GROUP_OWNER)
		})
		expect(
			wrapper
				.find('.first-name')
				.render()
				.text()
		).toBe('')
		expect(
			wrapper
				.find('.last-name')
				.render()
				.text()
		).toBe('')
		expect(
			wrapper
				.find('.email')
				.render()
				.text()
		).toBe('nicki@minaj.superbass')
	})
	it('renders with readOnly by default', () => {
		const wrapper = setup(mockUser().userInfo, {
			user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER)
		})
		expect(wrapper.find('WithStyles(TableCell)').length).toEqual(5)
		expect(wrapper.find('RoleSelect').length).toEqual(1)
		expect(wrapper.find({ className: 'color-red removeUserButton' }).length).toEqual(1)
	})
	it('hides RoleSelect and remove button if readOnly', () => {
		const wrapper = setup(mockUser().userInfo, {
			user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER),
			readOnly: true
		})
		expect(wrapper.find('WithStyles(TableCell)').length).toEqual(4)
		expect(wrapper.find('RoleSelect').length).toEqual(0)
		expect(wrapper.find({ className: 'removeUserButton' }).length).toEqual(0)
	})
	it('hides RoleSelect and remove button if user is current user and canModifySelf = false', () => {
		const wrapper = setup(defaultGroupOwner, {
			user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER)
		})
		expect(wrapper.find('WithStyles(TableCell)').length).toEqual(5)
		expect(wrapper.find('RoleSelect').length).toEqual(0)
		expect(wrapper.find({ className: 'color-red removeUserButton' }).length).toEqual(0)
	})
	it('shows RoleSelect and remove button if user is current user and canModifySelf = true', () => {
		const wrapper = setup(defaultGroupOwner, {
			user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER),
			canModifySelf: true
		})
		expect(wrapper.find('WithStyles(TableCell)').length).toEqual(5)
		expect(wrapper.find('RoleSelect').length).toEqual(1)
		expect(wrapper.find({ className: 'color-red removeUserButton' }).length).toEqual(1)
	})
	describe('changeRole', () => {
		it('throws if props.swapUserRoles is undefined', () => {
			const wrapper = setup(mockUser().userInfo, {
				user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER)
			})
			const instance = wrapper.instance() as UserRolesTableRow
			expect(() => {
				instance.changeRole({ target: { value: ROLE.GROUP_GRADER } })
			}).toThrowError('swapUserRoles is undefined')
		})
		it('calls props.swapUserRoles', () => {
			const swapUserRoles = jest.fn()
			const wrapper = setup(mockUser().userInfo, {
				user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER),
				swapUserRoles
			})
			const instance = wrapper.instance() as UserRolesTableRow
			instance.changeRole({ target: { value: ROLE.GROUP_GRADER } })
			expect(swapUserRoles.mock.calls.length).toEqual(1)
		})
	})
	describe('removeUserFromRole', () => {
		it('throws if props.removeUserFromRole is undefined', () => {
			const wrapper = setup(mockUser().userInfo, {
				user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER)
			})
			const instance = wrapper.instance() as UserRolesTableRow
			expect(() => {
				instance.removeUserFromRole()
			}).toThrowError('removeUserFromRole is undefined')
		})
		it('calls props.removeUserFromRole', () => {
			const removeUserFromRole = jest.fn()
			const wrapper = setup(mockUser().userInfo, {
				user: setDefaultRole(defaultGroupOwner, ROLE.GROUP_OWNER),
				removeUserFromRole
			})
			const instance = wrapper.instance() as UserRolesTableRow
			instance.removeUserFromRole()
			expect(removeUserFromRole.mock.calls.length).toEqual(1)
		})
	})
})
