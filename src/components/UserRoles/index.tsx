import _ from 'lodash'
import React, { Component } from 'react'
import { Alert, Col, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { FetchError, hooks, Model, NET_ACTION } from 'studiokit-net-js'
import uuid from 'uuid'
import AlertDialog from '../../components/AlertDialog'
import Loading from '../../components/Loading'
import UserRolesAdd from '../../components/UserRoles/Add'
import UserRolesTable from '../../components/UserRoles/Table'
import MODEL_STATUS from '../../constants/modelStatus'
import ROLE from '../../constants/role'
import { dispatchAction } from '../../redux/actionCreator'
import { BaseReduxState, RoleDescription, User, UserRole, UserWithDefaultRole } from '../../types'
import {
	canPerformActivityGlobally,
	canPerformActivityGloballyOrOnEntity,
	defaultOptions
} from '../../utils/baseActivity'
import { getDomainIdentifierTypePluralString } from '../../utils/domainIdentifier'
import modelUtils from '../../utils/model'
import { textForRole } from '../../utils/role'
import { sortByDefaultRoleAndName } from '../../utils/sort'
import { displayName } from '../../utils/user'
import { CollectionComponentWrappedProps } from '../HOC/CollectionComponent'

export interface UserRolesComponentProps {
	// Parent
	modifyUserRoleActivityName: string
	filterUsers: (userRoles: UserRole[]) => UserRole[]
	applyDefaultRoleToUsers: (userRoles: UserRole[]) => UserWithDefaultRole[]
	canModifySelf: boolean
	defaultRole: string
	roleDescriptions: RoleDescription
	removeUserDescription?: (user: UserRole) => void
	renderTableDescription?: (canModify: boolean) => void
	entity?: Model
	entityName?: string
	getRootModelName?: (props: { modelName: string }) => string
}

export interface UserRolesComponentStateProps {
	// Local
	swapUserRoles?: any // PT.object
	canModify: boolean
}

interface UserRolesComponentState {
	sortedUsers: UserRole[]
	// add
	addUsersHookId: any
	isAddingUsers: boolean
	identifiersToAdd?: string[]
	roleForAdd?: string
	shouldResetAddForm: boolean
	// remove
	shouldShowRemoveDialog: boolean
	userToRemove: UserRole | undefined
	rollForRemove?: string
	// swap
	userToSwap: UserRole | undefined
	// messages
	successMessage?: string
	existingUsersMessage?: string
	failMessage?: string
}

export interface AddBusinessModel {
	addedUsers: UserRole[]
	existingUsers: UserRole[]
	invalidIdentifiers: string[]
	allowedDomains: string
	invalidDomainIdentifiers: string[]
}

/**
 * Component used to manage either UserRoles (global roles) or EntityUserRoles.
 */
export class UserRoles extends Component<
	CollectionComponentWrappedProps<UserRole> & UserRolesComponentProps & UserRolesComponentStateProps,
	UserRolesComponentState
> {
	state: UserRolesComponentState = {
		sortedUsers: [],
		// add
		addUsersHookId: uuid.v4(),
		isAddingUsers: false,
		identifiersToAdd: undefined,
		roleForAdd: undefined,
		shouldResetAddForm: false,
		// remove
		shouldShowRemoveDialog: false,
		userToRemove: undefined,
		// swap
		userToSwap: undefined,
		// messages
		successMessage: undefined,
		existingUsersMessage: undefined,
		failMessage: undefined
	}

	componentDidMount() {
		this.setStateFromProps(this.props)
		const { addUsersHookId } = this.state
		hooks.unregisterNoStoreActionHook(addUsersHookId)
	}

	componentDidUpdate(
		prevProps: CollectionComponentWrappedProps<UserRole> & UserRolesComponentProps & UserRolesComponentStateProps
	) {
		const { modelArray: prevModelArray, swapUserRoles: prevSwapUserRoles, modelStatus: prevModelStatus } = prevProps
		const { modelArray, swapUserRoles, modelStatus, entityName } = this.props

		if (!_.isEqual(prevModelArray, modelArray)) {
			this.setStateFromProps(this.props)
		}

		// loading error
		if (
			(prevModelStatus === MODEL_STATUS.UNINITIALIZED || prevModelStatus === MODEL_STATUS.LOADING) &&
			modelStatus === MODEL_STATUS.ERROR
		) {
			this.setState({
				failMessage: `Oops! There was an error loading${
					!!entityName ? ` the people for your ${entityName}` : ''
				}.\r\nPlease try again.`
			})
		}

		// removing
		if (prevModelStatus === MODEL_STATUS.DELETING && modelStatus !== MODEL_STATUS.DELETING) {
			this.didRemove(modelStatus === MODEL_STATUS.READY)
		}

		// swapping
		const swapUserRoleResult = modelUtils.getModelFetchResult(prevSwapUserRoles, swapUserRoles)
		if (swapUserRoleResult.isFinished) {
			this.didSwap(swapUserRoleResult.isSuccess, swapUserRoleResult.error)
		}
	}

	setStateFromProps = (
		props: CollectionComponentWrappedProps<UserRole> & UserRolesComponentProps & UserRolesComponentStateProps
	) => {
		const { modelArray: userRoles, filterUsers, applyDefaultRoleToUsers } = props
		this.setState({
			sortedUsers: applyDefaultRoleToUsers(filterUsers(userRoles)).sort(sortByDefaultRoleAndName)
		})
	}

	//#region Add Users

	addUsers = (ids: string[], role: ROLE) => {
		const { entity, getRootModelName } = this.props
		this.setState({
			identifiersToAdd: ids,
			roleForAdd: role,
			successMessage: undefined,
			failMessage: undefined,
			shouldResetAddForm: false,
			isAddingUsers: true
		})
		const { addUsersHookId } = this.state
		hooks.registerNoStoreActionHook(addUsersHookId, data => {
			hooks.unregisterNoStoreActionHook(addUsersHookId)
			this.didAdd(data)
		})
		const rootModelName = !!getRootModelName ? getRootModelName(this.props) : undefined
		dispatchAction(NET_ACTION.DATA_REQUESTED, {
			modelName: `${!!rootModelName ? `${rootModelName}.` : ''}addUserRoles`,
			pathParams: !!entity ? [entity.id] : undefined,
			body: {
				entityId: !!entity ? entity.id : undefined,
				identifiers: ids,
				roleName: role
			},
			noStore: true,
			guid: addUsersHookId
		})
	}

	didAdd = (data?: AddBusinessModel) => {
		const { entityName } = this.props
		const { roleForAdd, identifiersToAdd } = this.state
		if (!roleForAdd || !identifiersToAdd || identifiersToAdd.length === 0) {
			throw new Error('didAdd was called in the incorrect state')
		}

		if (!data) {
			// tslint:disable-next-line: no-shadowed-variable
			const failMessage = `The following identifiers were not added${
				!!entityName ? ` to your ${entityName}` : ''
			}:\r\n${identifiersToAdd.join('\r\n')}`

			this.setState({
				shouldResetAddForm: true,
				isAddingUsers: false,
				identifiersToAdd: undefined,
				roleForAdd: undefined,
				failMessage
			})
			return
		}

		const { addedUsers, existingUsers, invalidIdentifiers, allowedDomains, invalidDomainIdentifiers } = data
		const roleString = textForRole(roleForAdd)

		const addedNames =
			!!addedUsers && addedUsers.length > 0
				? addedUsers
						.map((au: User) => {
							return `${displayName(au)}${!!au.uid ? ` (${au.uid})` : ''}`
						})
						.join('\r\n')
				: undefined

		const successMessage = !!addedNames
			? `The following people were successfully added${
					!!entityName ? ` to your ${entityName}` : ''
			  } as ${roleString}s:\r\n${addedNames}`
			: undefined

		const unchangedNames =
			!!existingUsers && existingUsers.length > 0
				? existingUsers.map((eu: User) => `${displayName(eu)}${!!eu.uid ? ` (${eu.uid})` : ''}`).join('\r\n')
				: undefined

		const existingUsersMessage = !!unchangedNames
			? `The following people were already${
					!!entityName ? ` in your ${entityName} as` : ''
			  } ${roleString}s:\r\n${unchangedNames}`
			: undefined

		let failMessage =
			!!invalidIdentifiers && invalidIdentifiers.length > 0
				? `The following ${getDomainIdentifierTypePluralString()} are invalid:\r\n${invalidIdentifiers.join(
						'\r\n'
				  )}`
				: undefined

		if (!!invalidDomainIdentifiers && invalidDomainIdentifiers.length > 0) {
			failMessage = `${
				!!failMessage ? `${failMessage}\r\n` : ''
			}The following ${getDomainIdentifierTypePluralString()} do not match the allowed domains of ${allowedDomains.replace(
				',',
				', '
			)}:\r\n${invalidDomainIdentifiers.join('\r\n')}`
		}

		this.setState({
			shouldResetAddForm: true,
			isAddingUsers: false,
			identifiersToAdd: undefined,
			roleForAdd: undefined,
			successMessage,
			existingUsersMessage,
			failMessage
		})

		// only reload if we had some successful adds
		if (!!addedNames) {
			this.props.load()
		}
	}

	//#endregion Add Users

	//#region Remove User

	alertRemoveUser = (userToRemove: UserRole) => {
		this.setState({
			userToRemove,
			shouldShowRemoveDialog: true
		})
	}

	removeUserTitle = (userToRemove: UserRole) => {
		return `Remove ${textForRole(userToRemove.roles[0])}`
	}

	removeUserDescription = (userToRemove: UserRole) => {
		const { removeUserDescription } = this.props
		const defaultWarning = (
			<p className="ma0">
				Are you sure you want to <strong>remove {displayName(userToRemove)}</strong>?
			</p>
		)
		if (!removeUserDescription) {
			return defaultWarning
		}
		return removeUserDescription(userToRemove)
	}

	removeUser = (shouldRemove: boolean) => {
		const { entity } = this.props
		const { userToRemove } = this.state
		if (userToRemove === undefined) {
			throw new Error('removeUser was called without setting userToRemove in state')
		}
		this.setState({
			shouldShowRemoveDialog: false,
			successMessage: undefined,
			failMessage: undefined,
			// clear if cancelled
			userToRemove: shouldRemove ? userToRemove : undefined
		})
		if (!shouldRemove) {
			return
		}
		this.props.delete({
			id: userToRemove.id,
			body: !!entity ? { roleName: userToRemove.roles[0], entityId: entity.id } : undefined
		})
	}

	didRemove = (isSuccess: boolean) => {
		const { entityName } = this.props
		const { userToRemove } = this.state
		if (userToRemove === undefined) {
			throw new Error('didRemove was called without setting state correctly')
		}

		const name = displayName(userToRemove)
		if (!isSuccess) {
			const failMessage = `Oops! There was an error removing ${name}${
				!!entityName ? ` from your ${entityName}` : ''
			}. Please try again.`
			this.setState({
				userToRemove: undefined,
				failMessage
			})
			return
		}

		const successMessage = `${name} was successfully removed${!!entityName ? ` from your ${entityName}` : ''}.`
		this.setState({
			userToRemove: undefined,
			successMessage
		})

		this.props.load()
	}

	//#endregion Remove User

	//#region Swap User Role

	swapUserRoles = (userToSwap: UserRole, newRoleName: string, currentRoleName: string) => {
		const { entity, getRootModelName } = this.props
		this.setState({
			userToSwap,
			successMessage: undefined,
			failMessage: undefined
		})
		const rootModelName = !!getRootModelName ? getRootModelName(this.props) : undefined
		dispatchAction(NET_ACTION.DATA_REQUESTED, {
			modelName: `${rootModelName}.swapUserRoles`,
			pathParams: !!entity ? [entity.id] : undefined,
			body: {
				entityId: !!entity ? entity.id : undefined,
				userId: userToSwap.id,
				newRoleName,
				currentRoleName
			}
		})
	}

	didSwap = (isSuccess: boolean, error?: boolean | FetchError) => {
		const { entityName } = this.props
		const { userToSwap } = this.state
		if (userToSwap === undefined) {
			throw new Error('didSwap was called without setting userToSwap in state')
		}
		const name = displayName(userToSwap)

		if (!isSuccess) {
			const failMessage = `Oops! There was an error updating ${name}’s access level${
				!!entityName ? ` in your ${entityName}` : ''
			}.\r\nPlease try again.`
			this.setState({
				userToSwap: undefined,
				failMessage
			})
			return
		}

		const successMessage = `${name}’s access level was changed successfully.`
		this.setState({
			userToSwap: undefined,
			successMessage
		})

		this.props.load()
	}

	//#endregion Swap User Role

	render() {
		const {
			modelStatus,
			canModify,
			canModifySelf,
			entityName,
			renderTableDescription,
			defaultRole,
			roleDescriptions
		} = this.props
		const {
			isAddingUsers,
			sortedUsers,
			shouldResetAddForm,
			shouldShowRemoveDialog,
			userToRemove,
			successMessage,
			existingUsersMessage,
			failMessage
		} = this.state

		const roles = _.keys(roleDescriptions)

		return (
			<>
				{!!successMessage && (
					<Alert
						id="successMessageAlert"
						bsStyle="success"
						onDismiss={() =>
							this.setState({
								successMessage: undefined
							})
						}>
						<h4 className="ma0 pre-wrap">{successMessage}</h4>
					</Alert>
				)}
				{!!existingUsersMessage && (
					<Alert
						id="existingUsersMessageAlert"
						bsStyle="info"
						onDismiss={() =>
							this.setState({
								existingUsersMessage: undefined
							})
						}>
						<h4 className="ma0 pre-wrap">{existingUsersMessage}</h4>
					</Alert>
				)}
				{!!failMessage && (
					<Alert
						id="failMessageAlert"
						bsStyle="warning"
						onDismiss={() =>
							this.setState({
								failMessage: undefined
							})
						}>
						<h4 className="ma0 pre-wrap">{failMessage}</h4>
					</Alert>
				)}
				{canModify && (
					<UserRolesAdd
						id="entityUserRolesAdd"
						entityName={entityName}
						addUsersToRole={this.addUsers}
						shouldReset={shouldResetAddForm}
						isAddingUsersToRole={isAddingUsers}
						defaultRole={defaultRole}
						roleDescriptions={roleDescriptions}
					/>
				)}
				{!!renderTableDescription && renderTableDescription(canModify)}
				<Row className="mt3">
					<Col xs={12}>
						{modelStatus === MODEL_STATUS.LOADING || sortedUsers.length < 1 ? (
							<Loading />
						) : (
							<UserRolesTable
								id="entityUserRolesTable"
								users={sortedUsers}
								readOnly={!canModify}
								canModifySelf={canModifySelf}
								roles={roles.map(r => textForRole(r))}
								removeUserFromRole={this.alertRemoveUser}
								swapUserRoles={this.swapUserRoles}
							/>
						)}
					</Col>
				</Row>
				{!!userToRemove && (
					<AlertDialog
						id="removeUserAlert"
						isOpen={shouldShowRemoveDialog}
						title={this.removeUserTitle(userToRemove)}
						description={this.removeUserDescription(userToRemove)}
						onDestroy={() => this.removeUser(true)}
						destroyText="Yes, remove the user"
						onCancel={() => this.removeUser(false)}
						cancelText="No, I changed my mind"
					/>
				)}
			</>
		)
	}
}

export const mapStateToProps = (
	state: BaseReduxState,
	ownProps: CollectionComponentWrappedProps<UserRole> & UserRolesComponentProps
) => {
	let swapUserRoles
	if (!!ownProps.getRootModelName && !!ownProps.entity) {
		swapUserRoles = _.get(
			state.models,
			`${ownProps.getRootModelName(ownProps)}.${ownProps.entity.id}.swapUserRoles`
		)
	}
	const canModifyGlobally = canPerformActivityGlobally(ownProps.modifyUserRoleActivityName, defaultOptions(state))
	return {
		swapUserRoles,
		canModifySelf: ownProps.hasOwnProperty('canModifySelf') ? ownProps.canModifySelf : canModifyGlobally,
		canModify: !!ownProps.entity
			? canPerformActivityGloballyOrOnEntity(
					ownProps.modifyUserRoleActivityName,
					defaultOptions(state, ownProps, 'entity')
			  )
			: canModifyGlobally
	}
}

export default connect(mapStateToProps)(UserRoles)
