import { shallow } from 'enzyme'
import React from 'react'
import { Model } from 'studiokit-net-js'
import * as net from 'studiokit-net-js'
import ACTIVITIES from '../../constants/baseActivity'
import {
	defaultAdmins,
	defaultGroupLearner,
	defaultGroupOwner,
	defaultGroupUsers,
	defaultNamedGroupLearner,
	defaultNamelessGroupLearner,
	mockGroups,
	mockUser
} from '../../constants/mockData'
import MODEL_STATUS from '../../constants/modelStatus'
import ROLE from '../../constants/role'
import { BaseReduxState, RoleDescription, UserInfo, UserRole } from '../../types'
import { getRootModelName } from '../../utils/entityUserRole'
import { filterManualGroupUsers } from '../../utils/groupRoles'
import { filterUsersByRole } from '../../utils/user'
import { applyDefaultRoleToUsers } from '../../utils/userRoles'
import { CollectionComponentWrappedProps } from '../HOC/CollectionComponent'
import { mapStateToProps, UserRoles, UserRolesComponentProps, UserRolesComponentStateProps } from './index'

net.hooks.registerNoStoreActionHook = jest.fn((key, hook) => {
	hook(null)
})

jest.mock('../../utils/shard', () => {
	return {
		isPurdueShard: jest.fn().mockImplementation(() => true)
	}
})

function setupAdmins(userInfo: UserInfo, entities: Model[], defaultRole: string, admins = defaultAdmins) {
	return setup(
		userInfo,
		'userRoles',
		admins,
		ACTIVITIES.USER_ROLE_MODIFY_ANY,
		defaultRole,
		{
			[defaultRole]: 'some description'
		},
		undefined,
		undefined,
		undefined,
		filterUsersByRole(defaultRole)
	)
}

function setupGroup(
	userInfo: UserInfo,
	groups: Model[],
	defaultRole: string,
	groupUsers = defaultGroupUsers as UserRole[]
) {
	return setup(
		userInfo,
		'groups.entityUserRoles',
		groupUsers,
		ACTIVITIES.GROUP_USER_ROLE_MODIFY,
		defaultRole,
		{ [ROLE.GROUP_GRADER]: 'something', [ROLE.GROUP_LEARNER]: 'something', [ROLE.GROUP_OWNER]: 'something' },
		groups,
		'course',
		getRootModelName,
		filterManualGroupUsers
	)
}

function setup(
	userInfo: UserInfo,
	modelName: string,
	userRoles: UserRole[],
	modifyUserRoleActivityName: string,
	defaultRole: string,
	roleDescriptions: RoleDescription,
	entities?: Model[],
	entityName?: string,
	getRootModelNameMethod?: (props: { modelName: string }) => string,
	filterUsers?: (users: any[]) => any[]
) {
	const mockState = {
		models: {
			user: {
				userInfo
			}
		}
	} as Partial<BaseReduxState>
	const wrappedProps = {
		load: jest.fn(),
		delete: jest.fn(),
		create: jest.fn(),
		update: jest.fn(),
		modelArray: userRoles,
		modelName,
		modelStatus: MODEL_STATUS.READY,
		pathParams: !!entityName ? [1] : undefined
	} as Partial<CollectionComponentWrappedProps<UserRole>>

	const mockProps = {
		entity: !!entityName && !!entities ? (entities[1] as Model) : undefined,
		entityName,
		modifyUserRoleActivityName,
		defaultRole,
		roleDescriptions,
		filterUsers,
		applyDefaultRoleToUsers: applyDefaultRoleToUsers(defaultRole),
		getRootModelName: getRootModelNameMethod,
		canModifySelf: true
	} as UserRolesComponentProps

	const ownProps = Object.assign({}, mockProps, wrappedProps)

	const props = Object.assign(
		{},
		ownProps,
		mapStateToProps(
			mockState as BaseReduxState,
			ownProps as UserRolesComponentProps & CollectionComponentWrappedProps<UserRole>
		)
	) as UserRolesComponentProps & UserRolesComponentStateProps & CollectionComponentWrappedProps<UserRole>

	return shallow(<UserRoles {...props} />)
}

function runTests(
	setupFunc: (userInfo: UserInfo, entities: Model[], defaultRole: string, users?: UserRole[]) => any,
	mockEntities: any,
	modifyUserRoleActivityName: string,
	entityName: string | undefined,
	defaultRole: string
) {
	describe('UserRolesAdd', () => {
		it(`is visible for user with global ${modifyUserRoleActivityName}`, () => {
			const wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				!!mockEntities ? mockEntities() : undefined,
				defaultRole
			)
			expect(wrapper.find({ id: 'entityUserRolesAdd' }).length).toEqual(1)
		})
		if (!!mockEntities) {
			it(`is visible for user with ${modifyUserRoleActivityName} on ${entityName}`, () => {
				const wrapper = setupFunc(mockUser().userInfo, mockEntities([modifyUserRoleActivityName]), defaultRole)
				expect(wrapper.find({ id: 'entityUserRolesAdd' }).length).toEqual(1)
			})
		}
		it(`is invisible for user without ${modifyUserRoleActivityName}`, () => {
			const wrapper = setupFunc(mockUser().userInfo, !!mockEntities ? mockEntities() : undefined, defaultRole)
			expect(wrapper.find({ id: 'entityUserRolesAdd' }).length).toEqual(0)
		})
	})
	describe('UserRolesTable', () => {
		it('is not displayed without users', () => {
			const wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				!!mockEntities ? mockEntities() : undefined,
				defaultRole,
				[]
			)
			expect(wrapper.find({ id: 'entityUserRolesTable' }).length).toEqual(0)
		})
		it(`is not read only for user with global ${modifyUserRoleActivityName}`, () => {
			const wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				!!mockEntities ? mockEntities() : undefined,
				defaultRole
			)
			expect(wrapper.find({ id: 'entityUserRolesTable' }).prop('readOnly')).toEqual(false)
		})
		if (!!mockEntities) {
			it(`is not read only for user with ${modifyUserRoleActivityName} on ${entityName}`, () => {
				const wrapper = setupFunc(mockUser().userInfo, mockEntities([modifyUserRoleActivityName]), defaultRole)
				expect(wrapper.find({ id: 'entityUserRolesTable' }).prop('readOnly')).toEqual(false)
			})
		}
		it(`is read only for user without ${modifyUserRoleActivityName}`, () => {
			const wrapper = setupFunc(mockUser().userInfo, !!mockEntities ? mockEntities() : undefined, defaultRole)
			expect(wrapper.find({ id: 'entityUserRolesTable' }).prop('readOnly')).toEqual(true)
		})
	})
	describe('functions', () => {
		let wrapper: any
		let instance: UserRoles
		beforeEach(() => {
			wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				!!mockEntities ? mockEntities() : undefined,
				defaultRole
			)
			instance = wrapper.instance()
		})
		describe('componentDidUpdate', () => {
			it('should update state if props.modelArray changes', () => {
				const spy = jest.spyOn(instance, 'setStateFromProps')
				wrapper.setProps({
					modelArray: [defaultGroupLearner, defaultGroupOwner]
				})
				expect(spy).toHaveBeenCalledTimes(1)
				if (defaultRole === ROLE.ADMIN) {
					// Note: In the admin users case, we filter out anyone who is not an admin from the list
					expect(wrapper.state('sortedUsers')).toEqual([])
				} else {
					const groupOwnerWithDefaultRole = Object.assign(
						{ defaultRole: ROLE.GROUP_OWNER, defaultRoleText: 'Instructor' },
						defaultGroupOwner
					)
					const groupLearnerWithDefaultRole = Object.assign(
						{ defaultRole: ROLE.GROUP_LEARNER, defaultRoleText: 'Student' },
						defaultGroupLearner
					)
					expect(wrapper.state('sortedUsers')).toEqual([
						groupOwnerWithDefaultRole,
						groupLearnerWithDefaultRole
					])
				}
			})

			it('should set failMessage on load error', () => {
				wrapper.setProps({
					modelStatus: MODEL_STATUS.LOADING
				})
				wrapper.setProps({
					modelStatus: MODEL_STATUS.ERROR
				})
				expect(wrapper.state('failMessage')).toEqual(
					`Oops! There was an error loading${
						!!entityName ? ` the people for your ${entityName}` : ''
					}.\r\nPlease try again.`
				)
			})
			it('should call didRemove after delete success', () => {
				const spy = jest.spyOn(instance, 'didRemove')
				instance.alertRemoveUser(defaultGroupLearner)
				instance.removeUser(true)
				wrapper.setProps({
					modelStatus: MODEL_STATUS.DELETING
				})
				wrapper.setProps({
					modelStatus: MODEL_STATUS.READY
				})
				expect(spy).toHaveBeenCalledWith(true)
				expect(wrapper.state('userToRemove')).toBeUndefined()
			})
			it('should call didRemove after delete fail', () => {
				const spy = jest.spyOn(instance, 'didRemove')
				instance.alertRemoveUser(defaultGroupLearner)
				instance.removeUser(true)
				wrapper.setProps({
					modelStatus: MODEL_STATUS.DELETING
				})
				wrapper.setProps({
					modelStatus: MODEL_STATUS.ERROR
				})
				expect(spy).toHaveBeenCalledWith(false)
				expect(wrapper.state('userToRemove')).toBeUndefined()
			})
			it('should call didSwap after swap success', () => {
				const spy = jest.spyOn(instance, 'didSwap')
				instance.swapUserRoles(defaultGroupLearner, ROLE.GROUP_OWNER, ROLE.GROUP_LEARNER)
				wrapper.setProps({
					swapUserRoles: undefined
				})
				wrapper.setProps({
					swapUserRoles: {
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					}
				})
				expect(spy).toHaveBeenCalledWith(true, undefined)
				expect(wrapper.state('userToSwap')).toBeUndefined()
			})
			it('should call didSwap after swap fail', () => {
				const spy = jest.spyOn(instance, 'didSwap')
				instance.swapUserRoles(defaultGroupLearner, ROLE.GROUP_OWNER, ROLE.GROUP_LEARNER)
				wrapper.setProps({
					swapUserRoles: undefined
				})
				wrapper.setProps({
					swapUserRoles: {
						_metadata: {
							isFetching: false,
							hasError: true,
							lastFetchError: 'boo'
						}
					}
				})
				expect(spy).toHaveBeenCalledWith(false, 'boo')
				expect(wrapper.state('userToSwap')).toBeUndefined()
			})
		})
		describe('addUsers', () => {
			it('should call didAdd from hook', () => {
				const spy = jest.spyOn(instance, 'didAdd')
				instance.addUsers(['wgrauvog'], ROLE.GROUP_OWNER)
				expect(spy).toHaveBeenCalled()
			})
		})
		describe('didAdd', () => {
			it('sets failMessage if no data, and shows alert', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['wgrauvog']
				})
				instance.didAdd()
				expect(wrapper.state('failMessage')).toEqual(
					`The following identifiers were not added${
						!!entityName ? ` to your ${entityName}` : ''
					}:\r\nwgrauvog`
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
			})
			it('sets success message, shows alert', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['wgrauvog', 'jase', 'x']
				})
				instance.didAdd({
					addedUsers: [defaultNamedGroupLearner],
					existingUsers: [],
					invalidIdentifiers: [],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('successMessage')).toEqual(
					`The following people were successfully added${
						!!entityName ? ` to your ${entityName}` : ''
					} as Instructors:\r\nNicki Minaj (nm-named)`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
			})
			it('sets success message, shows alert without uid', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['wgrauvog', 'jase', 'x']
				})
				instance.didAdd({
					addedUsers: [defaultNamelessGroupLearner, defaultNamedGroupLearner],
					existingUsers: [],
					invalidIdentifiers: [],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('successMessage')).toEqual(
					`The following people were successfully added${
						!!entityName ? ` to your ${entityName}` : ''
					} as Instructors:\r\nnicki@minaj.superbass (nm-nameless)\r\nNicki Minaj (nm-named)`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
			})
			it('sets existing users message, shows alert', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['wgrauvog', 'jase', 'x']
				})
				instance.didAdd({
					addedUsers: [],
					existingUsers: [defaultNamedGroupLearner],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('existingUsersMessage')).toEqual(
					`The following people were already${
						!!entityName ? ` in your ${entityName} as` : ''
					} Instructors:\r\nNicki Minaj (nm-named)`
				)
				expect(wrapper.find({ id: 'existingUsersMessageAlert' }).length).toEqual(1)
			})
			it('sets existing users message, shows alert without uid', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['wgrauvog', 'jase', 'x']
				})
				instance.didAdd({
					addedUsers: [],
					existingUsers: [{ id: '2', email: 'jase@purdue.edu', roles: [] }],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('existingUsersMessage')).toEqual(
					`The following people were already${
						!!entityName ? ` in your ${entityName} as` : ''
					} Instructors:\r\njase@purdue.edu`
				)
				expect(wrapper.find({ id: 'existingUsersMessageAlert' }).length).toEqual(1)
			})
			it('sets invalid identifiers message, shows alert', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['x']
				})
				instance.didAdd({
					addedUsers: [],
					existingUsers: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('failMessage')).toEqual(
					'The following Purdue career account aliases or PUIDs are invalid:\r\nx'
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
			})
			it('sets invalid identifiers domain message, shows alert', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['x']
				})
				instance.didAdd({
					addedUsers: [],
					existingUsers: [],
					invalidIdentifiers: [],
					allowedDomains: 'gmail.com,purdue.edu',
					invalidDomainIdentifiers: ['x']
				})
				expect(wrapper.state('failMessage')).toEqual(
					'The following Purdue career account aliases or PUIDs do not match the allowed domains of gmail.com, purdue.edu:\r\nx'
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
			})
			it('combines invalid identifiers and invalid identifiers domain message, shows alert', () => {
				wrapper.setState({
					roleForAdd: ROLE.GROUP_OWNER,
					identifiersToAdd: ['x', 'y']
				})
				instance.didAdd({
					addedUsers: [],
					existingUsers: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'gmail.com,purdue.edu',
					invalidDomainIdentifiers: ['y']
				})
				expect(wrapper.state('failMessage')).toEqual(
					'The following Purdue career account aliases or PUIDs are invalid:\r\nx\r\nThe following Purdue career account aliases or PUIDs do not match the allowed domains of gmail.com, purdue.edu:\r\ny'
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
			})
		})
		describe('alertRemoveUser', () => {
			it('shows alert when removing learner', () => {
				instance.alertRemoveUser(defaultGroupLearner)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(true)
				expect(wrapper.state('userToRemove')).toEqual(defaultGroupLearner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				expect(removeUserAlert.prop('title')).toEqual('Remove Student')
				expect(shallow(removeUserAlert.prop('description')).text()).toBe(
					'Are you sure you want to remove Joe Schmoe?'
				)
			})
			it('closes alert after onCancel', () => {
				instance.alertRemoveUser(defaultGroupLearner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				removeUserAlert.prop('onCancel')()
				// userToRemove is cleared, so it is not rendered
				expect(wrapper.find({ id: 'removeUserAlert' }).length).toEqual(0)
			})
			it('closes alert after onDestroy', () => {
				instance.alertRemoveUser(defaultGroupLearner)
				let removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				removeUserAlert.prop('onDestroy')()
				// userToRemove still exists, check isOpen
				removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(false)
			})
			it('shows alert when removing owner', () => {
				instance.alertRemoveUser(defaultGroupOwner)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(true)
				expect(wrapper.state('userToRemove')).toEqual(defaultGroupOwner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				expect(removeUserAlert.prop('title')).toEqual('Remove Instructor')
			})
			it('shows alert with custom description', () => {
				wrapper.setProps({
					removeUserDescription: () => {
						return 'foo'
					}
				})
				instance.alertRemoveUser(defaultGroupOwner)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(true)
				expect(wrapper.state('userToRemove')).toEqual(defaultGroupOwner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				expect(removeUserAlert.prop('title')).toEqual('Remove Instructor')
				expect(removeUserAlert.prop('description')).toEqual('foo')
			})
		})
		describe('removeUser', () => {
			it('should clear userToRemove on cancel', () => {
				instance.alertRemoveUser(defaultGroupLearner)
				instance.removeUser(false)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(false)
				expect(wrapper.state('userToRemove')).toBeUndefined()
			})
			it('should call props.delete', () => {
				const deleteFn = jest.fn()
				wrapper.setProps({
					delete: deleteFn
				})
				instance.alertRemoveUser(defaultGroupLearner)
				instance.removeUser(true)
				expect(wrapper.state('userToRemove')).toEqual(defaultGroupLearner)
				expect(deleteFn.mock.calls.length).toEqual(1)
			})
		})
		describe('didRemove', () => {
			it('should clear userToRemove and show error message on failure', () => {
				instance.alertRemoveUser(defaultGroupLearner)
				instance.removeUser(true)
				instance.didRemove(false)
				expect(wrapper.state('failMessage')).toEqual(
					`Oops! There was an error removing Joe Schmoe${
						!!entityName ? ` from your ${entityName}` : ''
					}. Please try again.`
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userToRemove')).toBeUndefined()
			})
			it('should clear userToRemove, call props.load, and show success message', () => {
				const load = jest.fn()
				wrapper.setProps({
					load
				})
				instance.alertRemoveUser(defaultGroupLearner)
				instance.removeUser(true)
				instance.didRemove(true)
				expect(wrapper.state('successMessage')).toEqual(
					`Joe Schmoe was successfully removed${!!entityName ? ` from your ${entityName}` : ''}.`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userToRemove')).toBeUndefined()
				expect(load.mock.calls.length).toEqual(1)
			})
		})
		describe('swapUserRoles', () => {
			it('should set userToSwap', () => {
				instance.swapUserRoles(defaultGroupLearner, ROLE.GROUP_OWNER, ROLE.GROUP_LEARNER)
				expect(wrapper.state('userToSwap')).toEqual(defaultGroupLearner)
			})
		})
		describe('didSwap', () => {
			it('should clear userToSwap and show error message on failure', () => {
				instance.swapUserRoles(defaultGroupLearner, ROLE.GROUP_OWNER, ROLE.GROUP_LEARNER)
				instance.didSwap(false)
				expect(wrapper.state('failMessage')).toEqual(
					`Oops! There was an error updating Joe Schmoe’s access level${
						!!entityName ? ` in your ${entityName}` : ''
					}.\r\nPlease try again.`
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userToSwap')).toBeUndefined()
			})
			it('should clear userToSwap, call props.load, and show success message', () => {
				instance.swapUserRoles(defaultGroupLearner, ROLE.GROUP_OWNER, ROLE.GROUP_LEARNER)
				instance.didSwap(true)
				expect(wrapper.state('successMessage')).toEqual('Joe Schmoe’s access level was changed successfully.')
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userToSwap')).toBeUndefined()
			})
		})
	})
	describe('alerts', () => {
		let wrapper: any
		beforeEach(() => {
			wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				!!mockEntities ? mockEntities() : undefined,
				defaultRole
			)
		})
		describe('successMessageAlert', () => {
			it('should close after onDismiss', () => {
				wrapper.setState({
					successMessage: 'foo'
				})
				const successMessageAlert = wrapper.find({ id: 'successMessageAlert' })
				expect(successMessageAlert.length).toEqual(1)
				successMessageAlert.prop('onDismiss')()
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(0)
			})
		})
		describe('existingUsersMessageAlert', () => {
			it('should close after onDismiss', () => {
				wrapper.setState({
					existingUsersMessage: 'foo'
				})
				const existingUsersMessageAlert = wrapper.find({ id: 'existingUsersMessageAlert' })
				expect(existingUsersMessageAlert.length).toEqual(1)
				existingUsersMessageAlert.prop('onDismiss')()
				expect(wrapper.find({ id: 'existingUsersMessageAlert' }).length).toEqual(0)
			})
		})
		describe('failMessageAlert', () => {
			it('should close after onDismiss', () => {
				wrapper.setState({
					failMessage: 'foo'
				})
				const failMessageAlert = wrapper.find({ id: 'failMessageAlert' })
				expect(failMessageAlert.length).toEqual(1)
				failMessageAlert.prop('onDismiss')()
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(0)
			})
		})
	})
}

describe('UserRoles', () => {
	describe('Admins', () => {
		runTests(setupAdmins, undefined, ACTIVITIES.USER_ROLE_MODIFY_ANY, undefined, ROLE.ADMIN)
	})
	describe('Group Users', () => {
		runTests(setupGroup, mockGroups, ACTIVITIES.GROUP_USER_ROLE_MODIFY, 'course', ROLE.GROUP_OWNER)
	})
})
