import Button from '@material-ui/core/Button'
import AddCircle from '@material-ui/icons/AddCircle'
import _ from 'lodash'
import React, { Component, Fragment } from 'react'
import { Alert, Col, ControlLabel, FormControl, FormGroup, Popover, Row } from 'react-bootstrap'
import AlertDialog from '../../components/AlertDialog'
import RefreshIndicatorInline from '../../components/RefreshIndicator/Inline'
import RoleSelect from '../../components/UserRoles/Select'
import ROLE from '../../constants/role'
import { RoleDescription } from '../../types'
import { getDomainIdentifierTypePluralString, getDomainIdentifierTypeString } from '../../utils/domainIdentifier'
import { isPurdueShard } from '../../utils/shard'
import { isEmail, isPurdueAliasOrPuid } from '../../utils/string'

const FERPA_ROLES = [ROLE.SUPER_ADMIN, ROLE.ADMIN, ROLE.GROUP_OWNER, ROLE.GROUP_GRADER]

export interface UserRolesAddProps {
	id: string
	entityName?: string // optional, if not targeting entityUserRoles
	addUsersToRole: any // todo function,
	shouldReset: boolean
	isAddingUsersToRole: boolean
	defaultRole: string
	roleDescriptions: RoleDescription
}

interface UserRolesAddState {
	identifiers: string
	role: string
	showErrorAlert: boolean
	errorMessage?: string
}

export default class UserRolesAdd extends Component<UserRolesAddProps, UserRolesAddState> {
	constructor(props: UserRolesAddProps) {
		super(props)
		this.state = {
			identifiers: '',
			role: this.props.defaultRole,
			showErrorAlert: false,
			errorMessage: undefined
		}
	}

	updateRole = (e: any) => {
		this.setState({
			role: e.target.value as string
		})
	}

	componentDidUpdate(prevProps: UserRolesAddProps) {
		const { shouldReset: prevShouldReset } = prevProps
		const { shouldReset } = this.props
		if (prevShouldReset === false && shouldReset === true) {
			this.setState({
				identifiers: '',
				role: this.props.defaultRole
			})
		}
	}

	handleSubmit = () => {
		const { identifiers, role } = this.state
		const identifiersArray = identifiers
			.split(/\r\n|\r|\n/g)
			.map(id => id.trim())
			.filter(id => id !== '') // filter identifiers that are empty
		const errorMessage = this.getValidationErrorMessage(identifiersArray)
		if (!!errorMessage) {
			this.setState({
				showErrorAlert: true,
				errorMessage
			})
		} else {
			this.props.addUsersToRole(identifiersArray, role)
		}
	}

	closeErrorAlert = () => {
		this.setState({
			showErrorAlert: false,
			errorMessage: undefined
		})
	}

	updateIdentifiers = (e: any) => {
		this.setState({
			identifiers: e.target.value as string
		})
	}

	getValidationErrorMessage = (identifiers?: string[]) => {
		if (!identifiers || identifiers.length === 0) {
			return `Please enter at least one ${getDomainIdentifierTypeString()}.`
		}

		let invalidIdentifiers
		if (isPurdueShard() && !identifiers.every(id => isPurdueAliasOrPuid(id))) {
			invalidIdentifiers = identifiers.filter(id => !isPurdueAliasOrPuid(id))
		}
		if (!isPurdueShard() && !identifiers.every(id => isEmail(id))) {
			invalidIdentifiers = identifiers.filter(id => !isEmail(id))
		}
		if (!!invalidIdentifiers) {
			return `Please enter valid ${getDomainIdentifierTypePluralString()}. The following are invalid:\r\n${invalidIdentifiers.join(
				'\r\n'
			)}`
		}
		return undefined
	}

	render() {
		const { identifiers, role, showErrorAlert, errorMessage } = this.state
		const { entityName, isAddingUsersToRole, roleDescriptions } = this.props
		const roles = _.keys(roleDescriptions)
		const accessLevelPopover = (
			<Popover id="accessLevel">
				<h3>Access Level</h3>
				{roles.map((r, index) => (
					<Fragment key={index}>{roleDescriptions[r]}</Fragment>
				))}
			</Popover>
		)

		return (
			<Row>
				<Col xs={12}>
					<h3>Add People</h3>
					<p>
						To add people
						{!!entityName ? ` to this ${entityName}` : ''}, enter their {getDomainIdentifierTypeString()},
						select their access level
						{!!entityName ? ` for this ${entityName}` : ''}, and click 'Add.'
					</p>
				</Col>
				<Col xs={12} sm={6}>
					<FormGroup controlId="UserEntry">
						<ControlLabel className="f6">
							{getDomainIdentifierTypePluralString()} (separated by a new line)
						</ControlLabel>
						<FormControl
							type="text"
							componentClass="textarea"
							name="account"
							aria-label="The account names to add"
							onChange={this.updateIdentifiers}
							value={identifiers}
							placeholder={getDomainIdentifierTypeString()}
						/>
					</FormGroup>
				</Col>
				<Col xs={12} sm={4}>
					<RoleSelect
						options={roles}
						value={role}
						onChange={this.updateRole}
						labelVisible
						popoverContentComponent={accessLevelPopover}
					/>
				</Col>
				<Col xs={12} sm={2} className="mt3-ns pt2-ns">
					{isAddingUsersToRole && <RefreshIndicatorInline className="mr3" />}
					{!isAddingUsersToRole && (
						<Button
							id="userRolesAddButton"
							className="btn btn-primary full-width-xxs"
							disabled={!identifiers}
							color="primary"
							onClick={this.handleSubmit}>
							<AddCircle className="fill-white v-mid" />
							Add
						</Button>
					)}
					<AlertDialog
						isOpen={showErrorAlert}
						title={`Error Adding People to the ${entityName}`}
						description={<div className="pre-wrap">{errorMessage}</div>}
						proceedText="Got it. Thanks!"
						onProceed={this.closeErrorAlert}
						onCancel={this.closeErrorAlert}
					/>
				</Col>
				{_.includes(FERPA_ROLES, role) && (
					<Col xs={12}>
						<Alert id="ferpaWarning" bsStyle="warning">
							This access level includes assignment scores in Circuit. Please consider whether{' '}
							<a
								href="https://www.purdue.edu/registrar/FERPA/index.html"
								target="_blank"
								rel="noopener noreferrer">
								FERPA
							</a>{' '}
							certification is needed.
						</Alert>
					</Col>
				)}
			</Row>
		)
	}
}
