import { Button, Table, TableBody, TableCell, TableHead, TableRow, TableSortLabel } from '@material-ui/core'
import ArrowDown from '@material-ui/icons/ArrowDropDown'
import _ from 'lodash'
import React, { Component } from 'react'
import { User } from '../../types'
import { sort } from '../../utils/sort'
import UserRolesTableRow, { UserRolesTableEditProps } from './TableRow'

export const TABLE_COLUMN = {
	FIRST_NAME: 'firstName',
	LAST_NAME: 'lastName',
	EMAIL: 'email',
	ROLE: 'roleText'
}

type ORDER = 'asc' | 'desc'
export const TABLE_ORDER = {
	ASC: 'asc' as ORDER,
	DESC: 'desc' as ORDER
}

export interface UserRolesTableProps extends UserRolesTableEditProps {
	id?: string
	users: User[]
}

interface UserRolesTableState {
	orderBy: string
	order: ORDER
}

export default class UserRolesTable extends Component<UserRolesTableProps, UserRolesTableState> {
	constructor(props: UserRolesTableProps) {
		super(props)
		this.state = this.getDefaultState()
	}

	getDefaultState = () => {
		return {
			orderBy: TABLE_COLUMN.LAST_NAME,
			order: TABLE_ORDER.ASC
		}
	}

	doSort = () => {
		const { users } = this.props
		const { orderBy, order } = this.state
		return sort(users, orderBy, order === TABLE_ORDER.ASC)
	}

	componentDidUpdate(prevProps: UserRolesTableProps) {
		// reset sort if parent updated
		if (!_.isEqual(prevProps.users, this.props.users)) {
			this.setState(this.getDefaultState())
		}
	}

	handleSort = (newOrderBy: string) => {
		// asc => desc => off (back to parent sorting)
		const newState = this.getDefaultState()
		if (this.state.orderBy !== newOrderBy) {
			newState.order = TABLE_ORDER.ASC
			newState.orderBy = newOrderBy
		} else if (this.state.order === TABLE_ORDER.ASC) {
			newState.order = TABLE_ORDER.DESC
			newState.orderBy = newOrderBy
		}
		this.setState(newState)
	}

	getLabel = (col: string) => {
		let label
		switch (col) {
			case TABLE_COLUMN.FIRST_NAME:
				label = 'First Name'
				break
			case TABLE_COLUMN.LAST_NAME:
				label = 'Last Name'
				break
			case TABLE_COLUMN.EMAIL:
				label = 'Email'
				break
			case TABLE_COLUMN.ROLE:
				label = 'Access Level'
				break
			default:
				break
		}
		return label
	}

	getDetailedLabel = (col: string) => {
		const { orderBy, order } = this.state
		const prefix = 'Sort by'
		const label = this.getLabel(col)
		if (orderBy === col && order === TABLE_ORDER.DESC) {
			return `${prefix} Default`
		}
		const direction = orderBy !== col ? 'Ascending' : 'Descending'
		return `${prefix} ${label} ${direction}`
	}

	getHeaderButton = (col: string) => {
		const { orderBy, order } = this.state
		return (
			<Button
				aria-label={this.getDetailedLabel(col)}
				onClick={() => {
					this.handleSort(col)
				}}>
				<TableSortLabel
					tabIndex={-1}
					className="pa0"
					active={orderBy === col}
					direction={order}
					IconComponent={ArrowDown}>
					{this.getLabel(col)}
				</TableSortLabel>
			</Button>
		)
	}

	render() {
		const { swapUserRoles, removeUserFromRole, readOnly, roles, canModifySelf } = this.props
		const sortedUsers = this.doSort()
		return (
			<div className="table-container">
				<Table>
					<TableHead>
						<TableRow>
							<TableCell className="header-sort">
								{this.getHeaderButton(TABLE_COLUMN.FIRST_NAME)}
							</TableCell>
							<TableCell className="header-sort">
								{this.getHeaderButton(TABLE_COLUMN.LAST_NAME)}
							</TableCell>
							<TableCell className="header-sort">{this.getHeaderButton(TABLE_COLUMN.EMAIL)}</TableCell>
							<TableCell className="header-sort">{this.getHeaderButton(TABLE_COLUMN.ROLE)}</TableCell>
							{!readOnly && <TableCell className="header-sort ph2 tc">Remove</TableCell>}
						</TableRow>
					</TableHead>
					<TableBody>
						{sortedUsers &&
							sortedUsers.map((user, index) => (
								<UserRolesTableRow
									id={`user-table-row-${index}`}
									key={index}
									user={user}
									readOnly={readOnly}
									canModifySelf={canModifySelf}
									roles={roles}
									swapUserRoles={swapUserRoles}
									removeUserFromRole={removeUserFromRole}
								/>
							))}
					</TableBody>
				</Table>
			</div>
		)
	}
}
