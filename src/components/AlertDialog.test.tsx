import { shallow } from 'enzyme'
import React from 'react'
import AlertDialog from './AlertDialog'

const mockOnCancel = jest.fn()
const mockOnProceed = jest.fn()
const mockOnDestroy = jest.fn()

const setup = () => {
	const mockProps = {
		isOpen: true,
		title: 'test',
		description: 'test',
		onProceed: mockOnProceed,
		proceedText: 'test',
		onCancel: mockOnCancel,
		cancelText: 'test',
		onDestroy: mockOnDestroy,
		destroyText: 'test',
		manualBackground: true
	}
	return shallow(<AlertDialog {...mockProps} />)
}

describe('AlertDialog', () => {
	it('renders', () => {
		const wrapper = setup()
	})
})
