import React, { FunctionComponent } from 'react'
import RefreshIndicator from './RefreshIndicator'

interface LoadingProps {
	message?: string
}

const Loading: FunctionComponent<LoadingProps> = ({ message }) => (
	<div className="loading loading-screen">
		<div className="section text-center main-content">
			<RefreshIndicator />
			{!!message && <h4>{message}</h4>}
		</div>
	</div>
)

export default Loading
