import { Button, Snackbar, SnackbarContent } from '@material-ui/core'
import Refresh from '@material-ui/icons/Refresh'
import WarningIcon from '@material-ui/icons/Warning'
import compareVersions from 'compare-versions'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { getAppConfig } from '../constants/configuration'
import { BaseReduxState } from '../types'
import { Client } from '../types/Client'

export interface NewVersionAlertProps {
	client?: Client
}

export class NewVersionAlert extends Component<NewVersionAlertProps & RouteComponentProps> {
	componentDidUpdate(prevProps: NewVersionAlertProps & RouteComponentProps) {
		if (this.props.location.pathname !== prevProps.location.pathname && this.hasVersionUpdate()) {
			this.refresh()
		}
	}

	hasVersionUpdate = () => {
		const { client } = this.props
		const version = getAppConfig().VERSION
		return (
			!!client && !!client.currentVersion && !!version && compareVersions(version, client.currentVersion) === -1
		)
	}

	refresh = () => {
		document.location.reload()
	}

	render() {
		return (
			<Snackbar
				id="newVersionAlert"
				anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
				open={this.hasVersionUpdate()}>
				<SnackbarContent
					className="bg-color-white color-almost-black"
					aria-describedby="newVersionAlertMessage"
					message={
						<span id="newVersionAlertMessage">
							<h2>
								<WarningIcon className="f3 fl mr2 v-mid fill-orange" /> A new version of Circuit is
								available
							</h2>
							<p className="ma0">
								Please click "Refresh" to refresh your browser. If you see this message again, please
								try clearing your browser cache and try again.
							</p>
						</span>
					}
					action={[
						<Button key="refresh" aria-label="Refresh" color="inherit" onClick={this.refresh}>
							<Refresh className="fill-black" /> Refresh
						</Button>
					]}
				/>
			</Snackbar>
		)
	}
}

export const mapStateToProps = (state: BaseReduxState): NewVersionAlertProps => {
	return {
		client: state.models.client
	}
}

export default withRouter(connect(mapStateToProps)(NewVersionAlert))
