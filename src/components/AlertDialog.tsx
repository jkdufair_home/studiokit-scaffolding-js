import Button from '@material-ui/core/Button'
import React, { FunctionComponent, MouseEventHandler } from 'react'
import { Modal } from 'react-bootstrap'
import ConnectedModal from './ConnectedModal'

export interface AlertDialogProps {
	id?: string
	isOpen: boolean
	title: string
	description: any
	onProceed?: MouseEventHandler<HTMLElement>
	proceedText?: string
	onCancel: MouseEventHandler<HTMLElement>
	cancelText?: string
	onDestroy?: MouseEventHandler<HTMLElement>
	destroyText?: string
	manualBackground?: boolean
}

const AlertDialog: FunctionComponent<AlertDialogProps> = (props: AlertDialogProps) => {
	const {
		isOpen,
		title,
		description,
		onProceed,
		proceedText,
		onCancel,
		cancelText,
		onDestroy,
		destroyText,
		manualBackground
	} = props
	return (
		<ConnectedModal show={isOpen} onHide={onCancel} className={`z-9999${manualBackground ? ' bg-black-50' : ''}`}>
			<Modal.Header closeButton>
				<Modal.Title>{title}</Modal.Title>
			</Modal.Header>
			{!!description && <Modal.Body>{description}</Modal.Body>}
			<Modal.Footer>
				{!!onProceed && (
					<Button
						id="alert-dialog-proceed-btn"
						variant="contained"
						aria-label={proceedText || 'Confirm'}
						color="primary"
						className="pull-right"
						onClick={onProceed}>
						{proceedText || 'Confirm'}
					</Button>
				)}
				<Button
					id="alert-dialog-cancel-btn"
					aria-label={cancelText || 'Cancel'}
					className={`${!!onDestroy ? 'pull-right' : 'pull-left'} ${!!onProceed ? 'mr1' : ''}`}
					onClick={onCancel}>
					{cancelText || 'Cancel'}
				</Button>
				{!!onDestroy && (
					<Button
						id="alert-dialog-destroy-btn"
						aria-label={destroyText || 'Destroy'}
						color="secondary"
						className="pull-left color-red"
						onClick={onDestroy}>
						{destroyText || 'Destroy'}
					</Button>
				)}
			</Modal.Footer>
		</ConnectedModal>
	)
}

export default AlertDialog
