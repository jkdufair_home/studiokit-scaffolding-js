import TextField, { StandardTextFieldProps } from '@material-ui/core/TextField'
import moment from 'moment'
import React, { Component } from 'react'
import {
	getFormattedNumberedDateForInput,
	getZonedMomentFromLocalDateTime,
	isDateInputSupported
} from '../../utils/date'

const datePattern = '^[0-9]{4}-[0-9]{2}-[0-9]{2}$'
const dateFormat = 'YYYY-MM-DD'
const dateRegex = new RegExp(datePattern)

export interface DateFieldProps
	extends Pick<StandardTextFieldProps, Exclude<keyof StandardTextFieldProps, 'onChange'>> {
	value?: string
	minDate?: string
	maxDate?: string
	onChange: (dateString: string, date?: Date) => void
}

export default class DateField extends Component<DateFieldProps> {
	onChange: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> = event => {
		const dateString = event.target.value
		const isInputValid = dateRegex.test(dateString)
		const date =
			isInputValid && this.isDateValid(dateString)
				? getZonedMomentFromLocalDateTime(dateString).toDate()
				: undefined
		if (!!this.props.onChange) {
			this.props.onChange(dateString, date)
		}
	}

	isDateValid = (dateString: string) => !!dateString && dateString.length > 0 && moment(dateString).isValid()

	render() {
		const { value, disabled, minDate, maxDate } = this.props
		const formatWarning =
			!!value && value.length > 0 && !dateRegex.test(value)
				? isDateInputSupported
					? `Must match the format "MM/DD/YYYY"`
					: `Must match the format "${dateFormat}"`
				: !!value && value.length > 0 && !this.isDateValid(value)
				? 'Must be a valid date'
				: undefined

		// prevent custom props from being passed to TextField
		const newProps = { ...this.props }
		const { className } = newProps
		delete newProps.minDate
		delete newProps.maxDate
		delete newProps.className

		return (
			<div className={className}>
				<TextField
					{...newProps}
					type="date"
					className="material-text-field-date"
					inputProps={{
						pattern: datePattern,
						placeholder: dateFormat,
						min: !!minDate && !disabled ? getFormattedNumberedDateForInput(minDate) : null,
						max: !!maxDate && !disabled ? getFormattedNumberedDateForInput(maxDate) : null
					}}
					onChange={this.onChange}
				/>
				{formatWarning && <label className="color-red i f6 db">{formatWarning}</label>}
			</div>
		)
	}
}
