import { shallow } from 'enzyme'
import React from 'react'
import TimeField, { TimeFieldProps } from './TimeField'

describe('TimeField', () => {
	it('should render', () => {
		const props: TimeFieldProps = {
			id: 'date',
			onChange: (dateString: string, date?: Date) => {
				// noop
			},
			label: 'Date',
			'aria-label': 'Active date',
			className: 'outer-class',
			value: ''
		}
		const wrapper = shallow(<TimeField {...props} />)
		expect(wrapper.prop('className')).toEqual('outer-class')
	})
})
