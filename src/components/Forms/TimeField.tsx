import TextField, { StandardTextFieldProps } from '@material-ui/core/TextField'
import IconHelp from '@material-ui/icons/Help'
import moment from 'moment'
import React, { Component } from 'react'
import { OverlayTrigger, Popover } from 'react-bootstrap'
import { isTimeInputSupported } from '../../utils/date'

const timePattern = '^(2[0-3]|[0-1]?[0-9]):([0-5][0-9])$'
const timeFormat = 'HH:mm'
const timeRegex = new RegExp(timePattern)

export interface TimeFieldProps
	extends Pick<StandardTextFieldProps, Exclude<keyof StandardTextFieldProps, 'onChange'>> {
	value?: string
	onChange: (timeString: string, time?: Date) => void
}

export default class TimeField extends Component<TimeFieldProps> {
	onChange: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> = event => {
		const timeString = event.target.value
		const isInputValid = timeRegex.test(timeString)
		const timeMoment = moment(timeString, ['HH:mm'])
		const time = isInputValid && this.isTimeValid(timeString) ? timeMoment.toDate() : undefined
		if (!!this.props.onChange) {
			this.props.onChange(timeString, time)
		}
	}

	isTimeValid = (timeString: string) =>
		!!timeString && timeString.length > 0 && moment(timeString, ['HH:mm']).isValid()

	render() {
		const { value } = this.props

		const formatWarning =
			!!value && value.length > 0 && !timeRegex.test(value)
				? isTimeInputSupported
					? `Must match the format "hh:mm a"`
					: `Must match the format "${timeFormat}" using 24 hour time`
				: !!value && value.length > 0 && !this.isTimeValid(value)
				? 'Must be a valid time'
				: undefined

		// prevent some props from being passed to TextField
		const newProps = { ...this.props }
		const { className } = newProps
		delete newProps.className

		if (!!newProps.label && !isTimeInputSupported) {
			const popover = (
				<Popover id="activatedAssignments">
					<h3>Missing Browser Features</h3>
					<p>
						Your current browser does not have a default time picker. You must use the 24 hour time format,
						"{timeFormat}
						".
					</p>
				</Popover>
			)
			newProps.label = (
				<OverlayTrigger placement="top" trigger={['click', 'hover', 'focus']} overlay={popover}>
					<span>
						{newProps.label} <small>(24 hour time)</small>
						<IconHelp className="ml1 v-top fill-blue f7" fontSize="small" tabIndex={0} />
					</span>
				</OverlayTrigger>
			)
		}

		return (
			<div className={className}>
				<TextField
					{...newProps}
					type="time"
					className="material-text-field-time"
					inputProps={{
						pattern: timePattern,
						placeholder: timeFormat
					}}
					onChange={this.onChange}
				/>
				{formatWarning && <label className="color-red i f6 db">{formatWarning}</label>}
			</div>
		)
	}
}
