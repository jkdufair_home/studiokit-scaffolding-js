import { shallow } from 'enzyme'
import React from 'react'
import DateField, { DateFieldProps } from './DateField'

describe('DateField', () => {
	it('should render', () => {
		const props: DateFieldProps = {
			id: 'date',
			onChange: (dateString: string, date?: Date) => {
				// noop
			},
			label: 'Date',
			'aria-label': 'Active date',
			className: 'outer-class',
			value: ''
		}
		const wrapper = shallow(<DateField {...props} />)
		expect(wrapper.prop('className')).toEqual('outer-class')
	})
})
