import React, { Component } from 'react'
import { MenuItem } from 'react-bootstrap'
import { connect } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { ModelCollection, NET_ACTION } from 'studiokit-net-js'
import { dispatchAction } from '../../redux/actionCreator'
import { ExternalTerm, Group } from '../../types'
import { isNowBeforeDate } from '../../utils/date'
import { filterGroupsByEndDate } from '../../utils/groupDates'
import { groupsAsAnythingButLearner, groupsAsLearner } from '../../utils/groupRoles'
import { sortByNameNatural } from '../../utils/sort'
import { ManagedNavDropdown } from './ManagedNavDropdown'

interface Props {
	title: string
	id: string
	groups: ModelCollection<Group>
	anyRoleButLearner?: boolean
	externalTerms: ModelCollection<ExternalTerm>
}

class GroupsDropdown extends Component<Props> {
	loadGroups = () => {
		dispatchAction(NET_ACTION.DATA_REQUESTED, { modelName: 'groups' })
	}

	didToggle = (isOpen: boolean) => {
		if (isOpen) {
			this.loadGroups()
		}
	}

	renderGroupMenu = (groups: Group[]) => {
		if (!groups) {
			return null
		}
		return groups.sort(sortByNameNatural).map((group, index) => {
			const groupURL = `/courses/${group.id}`
			return (
				<LinkContainer key={index} to={`${groupURL}`}>
					<MenuItem>{group.name}</MenuItem>
				</LinkContainer>
			)
		})
	}

	render() {
		const { title, id, groups, externalTerms, anyRoleButLearner } = this.props
		const groupsFilteredByRole = anyRoleButLearner ? groupsAsAnythingButLearner(groups) : groupsAsLearner(groups)
		const currentGroups = filterGroupsByEndDate(groupsFilteredByRole, externalTerms, isNowBeforeDate)
		if (currentGroups.length === 0) {
			return null
		}
		return (
			<ManagedNavDropdown title={title} id={id} didToggle={this.didToggle}>
				{this.renderGroupMenu(currentGroups)}
			</ManagedNavDropdown>
		)
	}
}

const mapStateToProps = (state: any) => {
	return {
		groups: !!state.models && !!state.models.groups ? state.models.groups : {},
		externalTerms: state.models.externalTerms
	}
}

export default connect(mapStateToProps)(GroupsDropdown)
