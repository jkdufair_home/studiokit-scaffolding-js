import Lock from '@material-ui/icons/Lock'
import React, { Component, ReactNode } from 'react'
import { MenuItem } from 'react-bootstrap'

import { AUTH_ACTION } from 'studiokit-auth-js'
import { dispatchAction } from '../../redux/actionCreator'
import { ManagedNavDropdown } from './ManagedNavDropdown'

export interface UserDropdownProps {
	username?: string
	children?: ReactNode
}

export default class UserDropdown extends Component<UserDropdownProps> {
	doLogOut = () => {
		dispatchAction(AUTH_ACTION.LOG_OUT_REQUESTED, {})
	}

	render() {
		const { username, children } = this.props

		return (
			<ManagedNavDropdown id="user-menu" className="user-menu" title={`Hi, ${username}`}>
				{children}
				<MenuItem onSelect={this.doLogOut}>
					<Lock className="menu-icon fill-white" />
					Log Out
				</MenuItem>
			</ManagedNavDropdown>
		)
	}
}
