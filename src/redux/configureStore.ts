import * as Sentry from '@sentry/browser'
import { routerMiddleware } from 'connected-react-router'
import { History } from 'history'
import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { createLogger } from 'redux-logger'
import { Persistor, persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'
import createSentryMiddleware from 'redux-sentry-middleware'

import { getAppConfig } from '../constants/configuration'
import { BaseReduxState } from '../types'
import { setStore as setStoreForActionCreator } from './actionCreator'
import configureReducers from './configureReducers'
import rootSaga from './sagas/rootSaga'

const configureStore = <TReduxState extends BaseReduxState>(history: History<any>) => {
	const appConfig = getAppConfig()
	const sentryMiddleware = createSentryMiddleware(Sentry)
	const sagaMiddleware = createSagaMiddleware()
	const reduxRouterMiddleware = routerMiddleware(history)
	const commonMiddlewares = [sentryMiddleware, sagaMiddleware, reduxRouterMiddleware]

	let enchancers
	if (appConfig.NODE_ENV !== 'production') {
		const logger = createLogger({
			collapsed: (getState, action, logEntry) => !logEntry || !logEntry.error
		})
		enchancers = composeWithDevTools(applyMiddleware(...commonMiddlewares, logger))
	} else {
		enchancers = applyMiddleware(...commonMiddlewares)
	}

	const store = createStore(configureReducers<TReduxState>(history), enchancers)
	setStoreForActionCreator(store)
	const persistor: Persistor = persistStore(store, undefined, () => {
		// Finished rehydration
		// start the sagas after rehydration so
		// rehydrate is guaranteed to be the first action
		sagaMiddleware.run(rootSaga)
	})

	return { store, persistor }
}

export default configureStore
