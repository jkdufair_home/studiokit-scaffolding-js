import _ from 'lodash'
import { Dictionary } from 'lodash'
import { Action } from 'redux'
import { ACTION } from '../actions'

export interface SearchAction extends Action {
	data: Dictionary<any>
}

export interface SearchState extends Dictionary<any> {}

/**
 * Store the state of a manage component's search parameters so the params can be
 * re-applied later when revisiting the same component
 *
 * @export
 * @param {*} [state={}]
 * @param {*} action
 * @returns
 */
export default function searchReducer(state: SearchState = {}, action: SearchAction) {
	switch (action.type) {
		case ACTION.PERSIST_SEARCH:
			return _.merge({}, state, action.data)

		default:
			return state
	}
}
