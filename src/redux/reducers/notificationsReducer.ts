import _ from 'lodash'
import { Action } from 'redux'
import { Notification } from '../../types'
import { ACTION } from '../actions'

export interface NotificationAction extends Action {
	notification: Notification
}

export interface NotificationState {
	queue: Notification[]
}

const initialState: NotificationState = {
	queue: []
}

export default function notificationsReducer(state = initialState, action: NotificationAction) {
	let updatedState: NotificationState
	switch (action.type) {
		case ACTION.ADD_NOTIFICATION:
			updatedState = _.merge({}, state)
			updatedState.queue.push(action.notification)
			return updatedState
		case ACTION.REMOVE_NOTIFICATION:
			updatedState = _.merge({}, state)
			updatedState.queue = updatedState.queue.filter(n => n.id !== action.notification.id)
			return updatedState
		default:
			return state
	}
}
