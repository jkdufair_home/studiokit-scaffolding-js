import modalsReducer from './modalsReducer'
import notificationsReducer from './notificationsReducer'
import searchReducer from './searchReducer'

export { modalsReducer, notificationsReducer, searchReducer }
