import { Dictionary } from 'lodash'
import { Action } from 'redux'
import { ACTION } from '../actions'

export interface ModalAction extends Action {
	guid: string
}

export interface ModalsState extends Dictionary<boolean> {}

/// state is a dictionary where keys are modal guids, for easy lookup
const initialState: ModalsState = {}

export default function modalsReducer(state = initialState, action: ModalAction) {
	let guid: string
	switch (action.type) {
		case ACTION.MODAL_ENTERING:
			guid = action.guid
			if (!!state[guid]) {
				return state
			}
			const newState = { ...state, ...{ [`${guid}`]: true } }
			return newState
		case ACTION.MODAL_EXITED:
			guid = action.guid
			if (!state[guid]) {
				return state
			}
			// destructure `state`, separating the value of key `guid` to `removed`, and the rest of the keys to `stateCopy`
			const { [guid]: removed, ...stateCopy } = state
			return stateCopy
		default:
			return state
	}
}
