import { connectRouter } from 'connected-react-router'
import { History } from 'history'
import { Dictionary } from 'lodash'
import { combineReducers, Reducer } from 'redux'
import { PersistPartial, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { reducers as authReducers } from 'studiokit-auth-js'
import { FetchAction, reducers as netReducers } from 'studiokit-net-js'
import { BaseModelsState, BaseReduxState } from '../types'
import { modalsReducer, notificationsReducer, searchReducer } from './reducers'

//#region Setup

const defaultPersistBlacklist = [
	'logs',
	'statusMessages',
	'env',
	'modals',
	'models',
	// blacklist nested persists
	'auth',
	'notifications'
]

let persistBlacklist: string[] = []

export const updatePersistBlacklist = (blacklist: string[]) => {
	persistBlacklist = [...defaultPersistBlacklist, ...blacklist]
}

let otherReducers: Dictionary<Reducer> = {}

export const setOtherReducers = (value: Dictionary<Reducer>) => {
	otherReducers = value
}

//#endregion Setup

const rootPersistConfig = {
	key: 'root',
	storage,
	blacklist: persistBlacklist
}

// create nested persist configs for nested key blacklisting

const authPersistConfig = {
	key: 'auth',
	storage,
	blacklist: ['isInitialized']
}

const notificationsPersistConfig = {
	key: 'notifications',
	storage,
	blacklist: ['queue']
}

export default <TReduxState extends BaseReduxState>(history: History): Reducer<TReduxState & PersistPartial, any> =>
	persistReducer(
		rootPersistConfig,
		combineReducers({
			auth: persistReducer(authPersistConfig, authReducers.authReducer),
			notifications: persistReducer(notificationsPersistConfig, notificationsReducer),
			modals: modalsReducer,
			models: netReducers.fetchReducer as Reducer<BaseModelsState, FetchAction>,
			router: connectRouter(history),
			search: searchReducer,
			...otherReducers
		})
	) as Reducer<TReduxState & PersistPartial, any>
