import { BaseReduxState } from '../types'

export const hasUserData = (state: BaseReduxState) =>
	!!state.models && !!state.models.user && !!state.models.user.userInfo && !!state.models.user.userInfo.id
