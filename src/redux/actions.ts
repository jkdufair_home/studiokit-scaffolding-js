import uuid from 'uuid'
import { dispatchAction } from './actionCreator'

export enum ACTION {
	// auth
	IDENTITY_PROVIDER_LOGIN_REQUESTED = 'auth/IDENTITY_PROVIDER_LOGIN_REQUESTED',

	// general notifications
	ADD_NOTIFICATION = 'notifications/ADD_NOTIFICATION',
	REMOVE_NOTIFICATION = 'notifications/REMOVE_NOTIFICATION',

	// persist manage pages state between route changes
	PERSIST_SEARCH = 'manage/PERSIST_SEARCH',

	// modals
	MODAL_ENTERING = 'modals/ENTERING',
	MODAL_EXITED = 'modals/EXITED'
}

export const addNotification = ({ text, type }: { text: string; type: string }) => {
	dispatchAction(ACTION.ADD_NOTIFICATION, {
		notification: {
			id: uuid.v4(),
			text,
			type
		}
	})
}
