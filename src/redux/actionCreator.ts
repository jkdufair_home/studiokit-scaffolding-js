import { Action, Store } from 'redux'

let store: Store

const setStore = (storeParam: Store): void => {
	store = storeParam
}

const dispatchAction = (type: string, payload: any): void => {
	store.dispatch(Object.assign({}, { type }, payload))
}

const createAction = (type: string, payload: any): Action => {
	return Object.assign({}, { type }, payload)
}

export { setStore, dispatchAction, createAction }
