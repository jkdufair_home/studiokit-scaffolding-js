import { take } from 'redux-saga/effects'
import { AUTH_ACTION } from 'studiokit-auth-js'
import { NET_ACTION } from 'studiokit-net-js'
import { dispatchAction } from '../actionCreator'

//#region Setup

let onPostLogin: () => void

export const setOnPostLogin = (value: () => void) => {
	onPostLogin = value
}

//#endregion Setup

export default function* postLoginDataSaga() {
	const externalProvidersAction = {
		modelName: 'externalProviders',
		taskId: 'external-provider-refresh'
	}
	while (true) {
		yield take(AUTH_ACTION.GET_TOKEN_SUCCEEDED)
		// load groups on initial login. refreshing happens elsewhere
		dispatchAction(NET_ACTION.DATA_REQUESTED, { modelName: 'groups' })
		// grab external providers once a day (for those tab-leaver-openers)
		dispatchAction(
			NET_ACTION.PERIODIC_DATA_REQUESTED,
			Object.assign({}, externalProvidersAction, {
				period: 1000 * 60 * 60 * 24
			})
		)
		if (!!onPostLogin) {
			onPostLogin()
		}
		yield take(AUTH_ACTION.LOG_OUT_REQUESTED)
		dispatchAction(NET_ACTION.PERIODIC_TERMINATION_REQUESTED, externalProvidersAction)
	}
}
