import * as Sentry from '@sentry/browser'
import _, { Dictionary } from 'lodash'
import { SagaIterator } from 'redux-saga'
import { all, AllEffect, ForkEffect, takeEvery } from 'redux-saga/effects'
import {
	AUTH_ACTION,
	endpointMappings as authEndpointMappings,
	getOAuthToken,
	sagas as authSagas
} from 'studiokit-auth-js'
import { ErrorFunction, LoggerFunction, sagas as netSagas } from 'studiokit-net-js'
import { getAppConfig, getEndpointMappings } from '../../constants/configuration'
import codeProviderService from '../../services/codeProviderService'
import persistenceService from '../../services/persistenceService'
import ticketProviderService from '../../services/ticketProviderService'
import caliperSaga from './caliperSaga'
import configurationSaga from './configurationSaga'
import errorSaga from './errorSaga'
import identityProviderSaga from './identityProviderSaga'
import initialDataLoadSaga from './initialDataLoadSaga'
import postLoginDataSaga from './postLoginDataSaga'
import postLoginRedirectSaga from './postLoginRedirectSaga'
import sentrySaga from './sentrySaga'

//#region Setup

let otherDependentSagas: Dictionary<SagaIterator> = {}

export const setOtherDependentSagas = (value: Dictionary<SagaIterator>) => {
	otherDependentSagas = value
}

//#endregion Setup

function* dependentSagas() {
	yield all({
		noStoreSaga: netSagas.noStoreSaga(),
		configurationSaga: configurationSaga(),
		caliperSaga: caliperSaga(),
		sentrySaga: sentrySaga(),
		initialDataLoadSaga: initialDataLoadSaga(),
		identityProviderSaga: identityProviderSaga(),
		postLoginRedirectSaga: postLoginRedirectSaga(),
		postLoginDataSaga: postLoginDataSaga(),
		...otherDependentSagas
	})
}

const errorHandler: ErrorFunction = (message?: string) => {
	Sentry.captureException(new Error(message))
}

export default function* rootSaga(): IterableIterator<ForkEffect | AllEffect<any>> {
	const appConfig = getAppConfig()
	const endpointMappings = getEndpointMappings()

	if (appConfig.IS_DOWNTIME) {
		return
	}

	const logger: LoggerFunction | undefined =
		appConfig.NODE_ENV === 'production'
			? () => {
					/* no op */
			  } // disable logging
			: undefined // use default logger

	// Don’t start the remaining sagas until net and auth are initialized
	// takeEvery is used because we wouldn’t want to block on `take`
	yield takeEvery(AUTH_ACTION.AUTH_INITIALIZED, dependentSagas)
	yield all({
		errorSaga: errorSaga(),
		netSaga: netSagas.fetchSaga(
			_.merge(authEndpointMappings, endpointMappings),
			appConfig.API_BASE_URL,
			getOAuthToken,
			errorHandler,
			logger
		),
		authSaga: authSagas.authSaga(
			{
				client_id: appConfig.CLIENT_ID,
				client_secret: appConfig.CLIENT_SECRET
			},
			persistenceService,
			ticketProviderService,
			codeProviderService,
			logger
		)
	})
}
