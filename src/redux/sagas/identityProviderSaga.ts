import { SagaIterator } from 'redux-saga'
import { call, take } from 'redux-saga/effects'
import { AUTH_ACTION } from 'studiokit-auth-js'
import { NET_ACTION } from 'studiokit-net-js'
import persistenceService, { identityProviderKey } from '../../services/persistenceService'
import { ACTION } from '../actions'

export default function* identityProviderSaga(): SagaIterator {
	yield take(
		(action: any) => action.type === NET_ACTION.FETCH_RESULT_RECEIVED && action.modelName === 'identityProviders'
	)
	while (true) {
		const oauthToken = yield call(persistenceService.getPersistedToken)
		let identityProvider = yield call(persistenceService.getItem, identityProviderKey)
		if (oauthToken && identityProvider) {
			yield take(AUTH_ACTION.LOG_OUT_REQUESTED)
			yield call(persistenceService.removeItem, identityProviderKey)
			window.location = identityProvider.data.logoutUrl
		}
		identityProvider = yield take(ACTION.IDENTITY_PROVIDER_LOGIN_REQUESTED)
		yield call(persistenceService.setItem, identityProviderKey, identityProvider)
		window.location = identityProvider.data.loginUrl
	}
}
