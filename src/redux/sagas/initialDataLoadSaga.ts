import { NET_ACTION } from 'studiokit-net-js'
import { dispatchAction } from '../actionCreator'

export default function initialDataLoadSaga() {
	dispatchAction(NET_ACTION.DATA_REQUESTED, {
		modelName: 'configuration',
		period: 1000 * 60 * 60 * 24, // grab configs once a day (for those tab-leaver-openers)
		taskId: 'configuration-refresh'
	})
	dispatchAction(NET_ACTION.PERIODIC_DATA_REQUESTED, {
		modelName: 'externalTerms',
		period: 1000 * 60 * 60 * 24, // grab external terms once a day (for those tab-leaver-openers)
		taskId: 'external-term-refresh'
	})
	dispatchAction(NET_ACTION.DATA_REQUESTED, {
		modelName: 'identityProviders',
		period: 1000 * 60 * 60 * 24, // grab identity providers once a day (for those tab-leaver-openers)
		taskId: 'identity-provider-refresh'
	})
	dispatchAction(NET_ACTION.DATA_REQUESTED, {
		modelName: 'client',
		period: 1000 * 60 * 15, // grab client every 15 minutes
		taskId: 'client-refresh'
	})
}
