import * as Sentry from '@sentry/browser'
import { take } from 'redux-saga/effects'
import { AUTH_ACTION } from 'studiokit-auth-js'
import { NET_ACTION } from 'studiokit-net-js'
import { getAppConfig } from '../../constants/configuration'

export default function* sentrySaga() {
	const appConfig = getAppConfig()
	const sentryDsn = appConfig.SENTRY_DSN
	if (!sentryDsn) {
		return
	}
	Sentry.init({ dsn: sentryDsn, release: appConfig.VERSION })
	while (true) {
		const userInfoResponse = yield take(
			(action: any) => action.type === NET_ACTION.FETCH_RESULT_RECEIVED && action.modelName === 'user.userInfo'
		)
		const userInfo = userInfoResponse.data
		Sentry.configureScope(scope => {
			scope.setUser({
				id: userInfo.id,
				username: userInfo.userName,
				email: userInfo.email
			})
		})
		yield take(AUTH_ACTION.LOG_OUT_REQUESTED)
		Sentry.configureScope(scope => {
			scope.setUser({})
		})
		yield take(AUTH_ACTION.GET_TOKEN_SUCCEEDED)
	}
}
