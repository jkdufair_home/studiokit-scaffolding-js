import { LOCATION_CHANGE, push } from 'connected-react-router'
import _ from 'lodash'
import { parse, stringify } from 'query-string'
import { put, take } from 'redux-saga/effects'
import { AUTH_ACTION } from 'studiokit-auth-js'
import windowService from '../../services/windowService'

export default function* errorSaga() {
	while (true) {
		// check for error messages once at app startup, then on location changes
		yield take((action: any) => action.type === AUTH_ACTION.AUTH_INITIALIZED || action.type === LOCATION_CHANGE)
		const location = windowService.getLocation()
		const parsedSearch = parse(location.search)
		// check possible error message keys
		const message = parsedSearch._ltierrormsg || parsedSearch.error
		if (!_.isNil(message)) {
			yield put(
				push({
					pathname: '/error',
					search: stringify({ message })
				})
			)
		}
	}
}
