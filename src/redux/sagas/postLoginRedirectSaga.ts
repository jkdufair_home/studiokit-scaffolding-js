import { push } from 'connected-react-router'
import { call, put, take } from 'redux-saga/effects'
import { AUTH_ACTION } from 'studiokit-auth-js'
import ltiLaunchProviderService from '../../services/ltiLaunchProviderService'
import persistenceService, { returnUrlKey } from '../../services/persistenceService'

/**
 * Redirect the user after a successful log in.
 * Uses either the query params from an LTI Launch, or the saved `returnUrl`, if any.
 */
export default function* postLoginRedirectSaga() {
	while (true) {
		yield take(AUTH_ACTION.GET_TOKEN_SUCCEEDED)
		const returnUrl = yield call(persistenceService.getItem, returnUrlKey)
		const ltiRedirectUrl = ltiLaunchProviderService.getRedirectUrl()
		const redirectUrl = ltiRedirectUrl || returnUrl
		if (!!redirectUrl) {
			yield put(push(redirectUrl))
			if (!!returnUrl) {
				yield call(persistenceService.removeItem, returnUrlKey)
			}
		}
		yield take(AUTH_ACTION.LOG_OUT_REQUESTED)
	}
}
