import { call, take } from 'redux-saga/effects'
import { NET_ACTION } from 'studiokit-net-js'
import { setShardConfig } from '../../constants/configuration'
import { Configuration } from '../../types'

export default function* configurationSaga() {
	const configurationResult = yield take(
		(action: any) => action.type === NET_ACTION.FETCH_RESULT_RECEIVED && action.modelName === 'configuration'
	)
	const configuration: Configuration = configurationResult.data
	yield call(setShardConfig, configuration)
}
