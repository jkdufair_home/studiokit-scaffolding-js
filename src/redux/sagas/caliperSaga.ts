import { SagaIterator } from 'redux-saga'
import { call, cancel, delay, fork, take } from 'redux-saga/effects'
import { AUTH_ACTION } from 'studiokit-auth-js'
import { NET_ACTION, OAuthToken } from 'studiokit-net-js'
import persistenceService from '../../services/persistenceService'
import { UserInfo } from '../../types'
import {
	endCaliperSession,
	initializeCaliperService,
	setCaliperPerson,
	setToken,
	startCaliperSession
} from '../../utils/caliper'
import { dispatchAction } from '../actionCreator'

function setPersonAndStartCaliperSession(userData: UserInfo) {
	setCaliperPerson(userData)
	startCaliperSession()
}

function* loadToken(): SagaIterator {
	const caliperToken = yield call(persistenceService.getPersistedCaliperToken)
	if (caliperToken) {
		caliperToken['.expires'] = caliperToken.expires
		setToken(caliperToken)
	}
	return caliperToken
}

function* getToken() {
	dispatchAction(NET_ACTION.DATA_REQUESTED, { modelName: 'caliperToken' })
	const caliperToken = yield take(
		(action: any) => action.type === NET_ACTION.FETCH_RESULT_RECEIVED && action.modelName === 'caliperToken'
	)
	setToken(caliperToken.data)
	return caliperToken.data
}

function* loadOrGetToken() {
	let caliperToken = yield call(loadToken)
	if (!caliperToken || new Date(caliperToken['.expires']) <= new Date()) {
		caliperToken = yield call(getToken)
	}
	return caliperToken
}

function* refreshWhenNearlyExpired(caliperToken: OAuthToken) {
	while (true) {
		// Refresh when token is nearly expired
		const tokenAboutToExpireTime = (new Date(caliperToken['.expires']).getTime() - new Date().getTime()) * 0.85
		yield delay(tokenAboutToExpireTime)
		caliperToken = yield call(getToken)
	}
}

export default function* caliperSaga() {
	const configuration = yield take(
		(action: any) => action.type === NET_ACTION.FETCH_RESULT_RECEIVED && action.modelName === 'configuration'
	)
	if (!configuration.data.caliperEnabled) {
		return
	}
	let caliperToken = yield call(loadToken)
	yield call(initializeCaliperService, configuration.data)
	while (true) {
		const userInfo = yield take(
			(action: any) => action.type === NET_ACTION.FETCH_RESULT_RECEIVED && action.modelName === 'user.userInfo'
		)
		caliperToken = yield call(loadOrGetToken)
		const refreshTask = yield fork(refreshWhenNearlyExpired, caliperToken)
		setPersonAndStartCaliperSession(userInfo.data)
		yield take(AUTH_ACTION.LOG_OUT_REQUESTED)
		yield cancel(refreshTask)
		endCaliperSession()
		yield take(AUTH_ACTION.GET_TOKEN_SUCCEEDED)
	}
}
