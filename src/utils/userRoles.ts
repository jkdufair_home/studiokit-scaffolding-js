import _ from 'lodash'
import { UserRole, UserWithDefaultRole } from '../types'
import { textForRole } from './role'

export const applyDefaultRoleToUsers = (role: string) => (userRoles: UserRole[]) => {
	return userRoles.map(userRole => setDefaultRole(userRole, role))
}

// set a single default role on the entityUser, for sorting and removing
export const setDefaultRole = (userRole: UserRole, defaultRole: string) => {
	const userDefaultRole = userRole.roles.indexOf(defaultRole) > -1 ? defaultRole : userRole.roles[0]
	const defaultRoleText = textForRole(userDefaultRole)
	return _.merge({ defaultRole: userDefaultRole, defaultRoleText }, userRole) as UserWithDefaultRole
}
