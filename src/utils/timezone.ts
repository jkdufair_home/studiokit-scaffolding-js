import * as moment from 'moment-timezone'
import { getShardConfig } from '../constants/configuration'

const defaultTimeZoneId: string = 'America/Indianapolis'

export const getDefaultTimeZoneId = () => {
	const shardConfig = getShardConfig()
	return !!shardConfig && !!shardConfig.defaultTimeZoneId ? shardConfig.defaultTimeZoneId : defaultTimeZoneId
}

export const guessTimeZoneId = () => moment.tz.guess()

export default {
	getDefaultTimeZoneId,
	guessTimeZoneId
}
