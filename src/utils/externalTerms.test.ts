import { ExternalTerm } from '../types/External'
import { filterByCurrentAndFuture } from './externalTerms'

const pastTerm: ExternalTerm = {
	id: 1,
	name: 'Past Term',
	startDate: '2018-01-01T05:00:00Z',
	endDate: '2018-05-15T03:59:59Z',
	externalProviderId: 1,
	externalId: '1'
}
const currentTerm: ExternalTerm = {
	id: 2,
	name: 'Current Term',
	startDate: '2018-08-01T05:00:00Z',
	endDate: '2018-12-31T04:59:59Z',
	externalProviderId: 2,
	externalId: '2'
}
const futureTerm: ExternalTerm = {
	id: 3,
	name: 'Future Term',
	startDate: '2019-01-01T05:00:00Z',
	endDate: '2019-05-15T03:59:59Z',
	externalProviderId: 3,
	externalId: '3'
}
const nowUtc = '2018-12-03T05:00:00Z'

describe('filterByCurrentAndFuture', () => {
	it('excludes past term, includes current and future terms', () => {
		const externalTerms = [pastTerm, currentTerm, futureTerm]
		const response = filterByCurrentAndFuture(externalTerms, nowUtc)
		expect(response.map(r => r.id)).toEqual([2, 3])
	})
	it('uses current ISO date as default', () => {
		const response = filterByCurrentAndFuture([pastTerm])
		expect(response.map(r => r.id)).toEqual([])
	})
})
