import { SHARD } from '../constants'
import { defaultAppConfiguration, defaultLocation } from '../constants/mockData'
import TIER from '../constants/tier'
import windowService from '../services/windowService'
import { getApiBaseUrl, getStorageUrl, hasHttpProtocol, isUrlValid, parseHeadersResponse } from './url'

let shardKey: SHARD
jest.mock('./shard', () => {
	return {
		getShardKey: () => shardKey
	}
})

afterAll(() => {
	jest.resetModules()
})

describe('getApiBaseUrl', () => {
	it('returns original url on LOCAL', () => {
		expect(getApiBaseUrl(TIER.LOCAL, 'https://api.studiokit.org')).toEqual('https://api.studiokit.org')
	})
	it('returns original url on Azure', () => {
		expect(getApiBaseUrl(TIER.PROD, 'https://studiokit-api.azurewebsites.net')).toEqual(
			'https://studiokit-api.azurewebsites.net'
		)
	})
	it('defaults to Purdue on Root', () => {
		shardKey = SHARD.ROOT
		expect(getApiBaseUrl(TIER.PROD, 'https://api.studiokit.org')).toEqual('https://purdue.api.studiokit.org')
	})
	it('returns correct Url on PROD', () => {
		shardKey = SHARD.DEMO
		expect(getApiBaseUrl(TIER.PROD, 'https://api.studiokit.org')).toEqual('https://demo.api.studiokit.org')
	})
	it('returns correct Url on DEV', () => {
		shardKey = SHARD.DEMO
		expect(getApiBaseUrl(TIER.DEV, 'https://api.studiokit.org')).toEqual('https://demo.dev.api.studiokit.org')
	})
})

describe('getStorageUrl', () => {
	it('returns original Url if not LOCAL', () => {
		expect(
			getStorageUrl({ ...defaultAppConfiguration, ...{ TIER: TIER.DEV } }, 'https://storage.net/file')
		).toEqual('https://storage.net/file')
	})
	it('returns custom Url if on LOCAL and config.STORAGE_URL is provided', () => {
		expect(
			getStorageUrl(
				{ ...defaultAppConfiguration, ...{ TIER: TIER.LOCAL, STORAGE_URL: 'http://vm:200' } },
				'http://127.0.0.1:10000/file'
			)
		).toEqual('http://vm:200/file')
	})
})

describe('hasHttpProtocol', () => {
	it('returns true if has `https` protocol', () => {
		expect(hasHttpProtocol('https://something.com')).toEqual(true)
	})
	it('returns true if has `http` protocol', () => {
		expect(hasHttpProtocol('http://something.com')).toEqual(true)
	})
	it('returns false if has `ftp` protocol', () => {
		expect(hasHttpProtocol('ftp://something.com')).toEqual(false)
	})
	it('returns false if has no protocol', () => {
		expect(hasHttpProtocol('something.com')).toEqual(false)
	})
})

describe('isUrlValid', () => {
	it('returns true if valid', () => {
		expect(isUrlValid('https://something.com')).toEqual(true)
	})
	it('returns false for other protocols', () => {
		expect(isUrlValid('ftp://something.com')).toEqual(false)
	})
	it('returns false for just letters', () => {
		expect(isUrlValid('aaa')).toEqual(false)
	})
})

describe('parseHeadersResponse', () => {
	beforeAll(() => {
		windowService.setLocation(defaultLocation)
	})
	afterAll(() => {
		windowService.setLocation()
	})

	it('returns defaults if no headers', () => {
		expect(parseHeadersResponse()).toEqual({
			isValid: false,
			iFrameAllowed: undefined
		})
	})
	it('returns isValid = true if has some headers, but no restrictive ones', () => {
		expect(
			parseHeadersResponse({
				'Content-Length': 0
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it("returns iFrameAllowed = true if X-CONTENT-SECURITY-POLICY exists but 'frame-ancestors' is not provided", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ["default-src 'self'"]
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it("returns iFrameAllowed = false if X-CONTENT-SECURITY-POLICY does not contain an acceptable 'frame-ancestors' source", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ["default-src 'self'; frame-ancestors 'none'"]
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: false
		})
	})
	it("returns iFrameAllowed = true if X-CONTENT-SECURITY-POLICY contains protocol matching current location for 'frame-ancestors'", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ['frame-ancestors https:']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it("returns iFrameAllowed = true if X-CONTENT-SECURITY-POLICY contains location (protocol + host) for 'frame-ancestors'", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ['frame-ancestors https://purdue.dev.studiokit.org']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it("returns iFrameAllowed = true if X-CONTENT-SECURITY-POLICY contains a wildcard with no protocol for 'frame-ancestors'", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ['frame-ancestors *.dev.studiokit.org']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it("returns iFrameAllowed = true if X-CONTENT-SECURITY-POLICY contains a wildcard with protocol for 'frame-ancestors'", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ['frame-ancestors https://*.dev.studiokit.org']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it("returns iFrameAllowed = false if X-CONTENT-SECURITY-POLICY contains location.host for 'frame-ancestors'", () => {
		expect(
			parseHeadersResponse({
				'X-CONTENT-SECURITY-POLICY': ['frame-ancestors purdue.dev.studiokit.org']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
	it('returns iFrameAllowed = false with X-FRAME-OPTIONS DENY', () => {
		expect(
			parseHeadersResponse({
				'X-FRAME-OPTIONS': ['DENY']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: false
		})
	})
	it('returns iFrameAllowed = false with X-FRAME-OPTIONS SAMEORIGIN', () => {
		expect(
			parseHeadersResponse({
				'X-FRAME-OPTIONS': ['SAMEORIGIN']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: false
		})
	})
	it('returns iFrameAllowed = false with X-FRAME-OPTIONS ALLOW-FROM a different domain', () => {
		expect(
			parseHeadersResponse({
				'X-FRAME-OPTIONS': ['ALLOW-FROM https://google.com']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: false
		})
	})
	it('returns iFrameAllowed = true with X-FRAME-OPTIONS ALLOW-FROM current domain', () => {
		expect(
			parseHeadersResponse({
				'X-FRAME-OPTIONS': ['ALLOW-FROM https://purdue.dev.studiokit.org']
			})
		).toEqual({
			isValid: true,
			iFrameAllowed: true
		})
	})
})
