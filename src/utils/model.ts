import _ from 'lodash'
import { Model, ModelCollection } from 'studiokit-net-js'

export const getModelArray = <T>(model: ModelCollection<T>, guid?: string): T[] =>
	Object.keys(model)
		.filter(key => key !== '_metadata' && key !== guid && !!model[key])
		.map(key => model[key]) as T[]

export const getModelMinusRelations = <T extends Model>(model: T) =>
	(Object.keys(model) as Array<keyof T>)
		.filter(key => key !== 'guid' && !_.isPlainObject(model[key]) && !_.isArray(model[key]))
		.reduce(
			(retval: T, key) => {
				retval[key] = model[key]
				return retval
			},
			{} as T
		)

export const getModelFetchResult = <T extends Model>(model?: T, nextModel?: T) => {
	// from (no model, empty model, fetching model) to (fetched model), e.g. GET
	const didFetch =
		(!model || !model._metadata || model._metadata.isFetching) &&
		(!!nextModel && !!nextModel._metadata && !nextModel._metadata.isFetching)
	// from (fetching model) to (no model), e.g. DELETED
	const didDelete = !!model && !!model._metadata && model._metadata.isFetching && !nextModel
	const isFinished = didFetch || didDelete
	const isSuccess =
		isFinished && (didDelete || (!!nextModel && !!nextModel._metadata && !nextModel._metadata.hasError))
	const error =
		(isFinished && !didDelete && !!nextModel && !!nextModel._metadata && nextModel._metadata.lastFetchError) ||
		undefined

	return {
		isFinished,
		isSuccess,
		error
	}
}

export default {
	getModelArray,
	getModelMinusRelations,
	getModelFetchResult
}
