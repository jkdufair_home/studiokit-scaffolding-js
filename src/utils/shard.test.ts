import { setAppConfig } from '../constants/configuration'
import { defaultAppConfiguration } from '../constants/mockData'
import SHARD from '../constants/shard'
import { getShardKey } from './shard'

describe('utils/shards', () => {
	describe('getShardKey', () => {
		beforeAll(() => {
			setAppConfig(defaultAppConfiguration)
		})
		it('returns localhostShardKey for localhost if localhostShardKey is set', () => {
			expect(getShardKey('localhost', 'custom')).toEqual('custom')
		})
		it('returns PURDUE for localhost', () => {
			expect(getShardKey('localhost')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for azure web apps', () => {
			expect(getShardKey('studiokit-web.azurewebsites.net')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for ngrok.io', () => {
			expect(getShardKey('ngrok.io')).toEqual(SHARD.PURDUE)
		})
		it('returns ROOT for www subdomain', () => {
			expect(getShardKey('www.studiokit.org')).toEqual(SHARD.ROOT)
		})
		it('returns ROOT for dev subdomain', () => {
			expect(getShardKey('dev.studiokit.org')).toEqual(SHARD.ROOT)
		})
		it('returns ROOT for no subdomain', () => {
			expect(getShardKey('studiokit.org')).toEqual(SHARD.ROOT)
		})
		it('returns correct subdomain on PROD', () => {
			expect(getShardKey('foo.bar.studiokit.org')).toEqual('foo.bar')
		})
		it('returns correct subdomain on DEV', () => {
			expect(getShardKey('foo.bar.dev.studiokit.org')).toEqual('foo.bar')
		})
	})
})
