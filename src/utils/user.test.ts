import ROLE from '../constants/role'
import { UserRole } from '../types'
import {
	displayFirstName,
	displayFirstNamePossessive,
	displayName,
	displayNamePossessive,
	filterUsersByRole
} from './user'

const userWithFullName: UserRole = {
	id: '1',
	firstName: 'Test',
	lastName: 'User',
	email: 'test@test.test',
	roles: [ROLE.GROUP_OWNER]
}

const user2WithFullName: UserRole = {
	id: '2',
	firstName: 'Chris',
	lastName: 'Adams',
	email: 'chris@test.test',
	roles: [ROLE.GROUP_OWNER, ROLE.GROUP_LEARNER]
}

const userWithNoName: UserRole = {
	id: '3',
	email: 'test@test.test',
	roles: [ROLE.GROUP_LEARNER]
}

const user2WithNoName: UserRole = {
	id: '4',
	email: 'test@test.tests',
	roles: [ROLE.GROUP_LEARNER]
}

const users = [userWithFullName, user2WithFullName, userWithNoName, user2WithNoName]

describe('utils/user', () => {
	describe('displayName', () => {
		it("should display the user's full name when not null", () => {
			const result = displayName(userWithFullName)
			expect(result).toBe('Test User')
		})

		it("should display the user's email address when the name is null", () => {
			const result = displayName(userWithNoName)
			expect(result).toBe('test@test.test')
		})

		describe('displayName reverse', () => {
			it("should display the user's full name in reverse when not null", () => {
				const result = displayName(userWithFullName, true)
				expect(result).toBe('User, Test')
			})

			it("should display the user's email address when the name is null", () => {
				const result = displayName(userWithNoName, true)
				expect(result).toBe('test@test.test')
			})
		})
	})

	describe('displayNamePossessive', () => {
		it("should display the user's full name in possessive form when not null", () => {
			const result = displayNamePossessive(userWithFullName)
			expect(result).toBe('Test User’s')

			const result2 = displayNamePossessive(user2WithFullName)
			expect(result2).toBe('Chris Adams’')
		})

		it("should display the user's email address in possessive form when the name is null", () => {
			const result = displayNamePossessive(userWithNoName)
			expect(result).toBe('test@test.test’s')

			const result2 = displayNamePossessive(user2WithNoName)
			expect(result2).toBe('test@test.tests’s')
		})
	})

	describe('displayFirstName', () => {
		it("should display the user's first name if available", () => {
			const result = displayFirstName(userWithFullName)
			expect(result).toBe('Test')
		})

		it("should display the user's email address if name not available", () => {
			const result = displayFirstName(userWithNoName)
			expect(result).toBe('test@test.test')
		})
	})

	describe('displayFirstNamePossessive', () => {
		it("should display the user's first name in possessive form when name is not null", () => {
			const result = displayFirstNamePossessive(userWithFullName)
			expect(result).toBe('Test’s')

			const result2 = displayFirstNamePossessive(user2WithFullName)
			expect(result2).toBe('Chris’')
		})

		it("should display the user's email address in possessive form when name is null", () => {
			const result = displayFirstNamePossessive(userWithNoName)
			expect(result).toBe('test@test.test’s')

			const result2 = displayFirstNamePossessive(user2WithNoName)
			expect(result2).toBe('test@test.tests’s')
		})
	})

	describe('filterUsersByRole', () => {
		it('should include only users with role', () => {
			const filterByOwner = filterUsersByRole(ROLE.GROUP_OWNER)
			const results = filterByOwner(users)
			expect(results.length).toEqual(2)

			const filterByLearners = filterUsersByRole(ROLE.GROUP_LEARNER)
			const results2 = filterByLearners(users)
			expect(results2.length).toEqual(3)
		})
	})
})
