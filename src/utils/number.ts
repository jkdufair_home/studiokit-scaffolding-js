/**
 * Validates that the input is a number and that the number falls within
 * the precision and scale constraints provided.
 * https://docs.microsoft.com/en-us/sql/t-sql/data-types/precision-scale-and-length-transact-sql?view=sql-server-2017
 *
 * @export
 * @param decimalString - The number to validate. Must be passed as a string (i.e. from a web form)
 * @param precision - The number of digits allowed in the value
 * @param scale - The number of digits allowed to the right of the decimal point in the value
 * @returns A boolean indicating whether the number falls within the precision and scale constraints provided
 */
export function hasPrecisionAndScale(decimalString: string, precision: number, scale: number) {
	if (
		typeof decimalString !== 'string' ||
		isNaN(Number(decimalString)) ||
		isNaN(precision) ||
		isNaN(scale) ||
		precision <= 0 ||
		scale < 0
	) {
		return false
	}
	const parts = decimalString.toString().split('.')
	return !!(
		parts[0].match(new RegExp(precision === scale ? '^0$' : '^-?\\d{1,' + (precision - scale) + '}$')) &&
		(parts.length > 1 ? parts[1].match(new RegExp('^\\d{1,' + scale + '}$')) : true)
	)
}

export function modulo(n: number, m: number) {
	return ((n % m) + m) % m
}
