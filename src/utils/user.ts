import { User, UserRole } from '../types'

const makePossessive = (word: string) => (word.slice(-1) === 's' ? `${word}’` : `${word}’s`)

export const displayName = (u: User, shouldReverse = false) =>
	!!u.firstName && !!u.lastName
		? shouldReverse
			? `${u.lastName}, ${u.firstName}`
			: `${u.firstName} ${u.lastName}`
		: u.email

export const displayNamePossessive = (u: User) =>
	!!u.firstName && !!u.lastName ? `${u.firstName} ${makePossessive(u.lastName)}` : `${u.email}’s`

export const displayFirstName = (u: User) => (!!u.firstName ? u.firstName : u.email)

export const displayFirstNamePossessive = (u: User) => (!!u.firstName ? makePossessive(u.firstName) : `${u.email}’s`)

export const filterUsersByRole = (role: string) => (userRoles: UserRole[]) => {
	return userRoles.filter(userRole => userRole.roles.includes(role))
}

export default {
	displayName,
	displayNamePossessive,
	displayFirstName,
	displayFirstNamePossessive,
	filterUsersByRole
}
