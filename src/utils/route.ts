import _ from 'lodash'
import pluralize from 'pluralize'
import { getEndpointMappings } from '../constants/configuration'

export const getModelId = (props: any, modelName: string) => {
	const singularModelName = pluralize.singular(modelName)
	const id = props.match.params[`${singularModelName}Id`]
	if (isNaN(id)) {
		return id
	}
	return parseInt(id, 10)
}

export const getPathParams = (props: any, modelName: string) => {
	const pathParams = modelName.split('.').reduce((retval: any[], levelName) => {
		const levelId = getModelId(props, levelName)
		if (levelId) {
			retval.push(levelId)
		}
		return retval
	}, [])
	return pathParams
}

export const getMinRequiredPathParamsCount = (modelName: string) => {
	const endpointMappings = getEndpointMappings()
	let pathParamCount = 0
	const modelLevelNames = modelName.split('.')
	modelLevelNames.forEach((levelName, index) => {
		const modelLevelName = modelLevelNames.slice(0, index + 1).join('.')
		const modelLevel = modelLevelName ? _.get(endpointMappings, modelLevelName) : {}
		const modelConfig = _.merge({}, modelLevel._config)
		if (modelConfig.isCollection && index + 1 < modelLevelNames.length) {
			pathParamCount++
		}
	})
	return pathParamCount
}

export const getReduxModelName = (pathParams: any[], modelName: string) => {
	const endpointMappings = getEndpointMappings()
	let reduxModelName = modelName
	if (pathParams.length > 0) {
		let pathParamIndex = 0
		const modelLevelNames = modelName.split('.')
		modelLevelNames.forEach((levelName, index) => {
			const modelLevelName = modelLevelNames.slice(0, index + 1).join('.')
			const modelLevel = modelLevelName ? _.get(endpointMappings, modelLevelName) : {}
			const modelConfig = _.merge({}, modelLevel._config)
			if (index === 0) {
				reduxModelName = levelName
			} else {
				reduxModelName = `${reduxModelName}.${levelName}`
			}
			if (pathParams.length - 1 >= pathParamIndex && modelConfig.isCollection) {
				reduxModelName = `${reduxModelName}.${pathParams[pathParamIndex]}`
				pathParamIndex++
			}
		})
	}
	return reduxModelName
}
