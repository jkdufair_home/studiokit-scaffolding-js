import _ from 'lodash'
import { Metadata, ModelCollection } from 'studiokit-net-js'
import ROLE from '../constants/role'
import { Group, GroupUserRole } from '../types'
import { getModelArray } from './model'
import { setDefaultRole } from './userRoles'

export const groupsAsLearner = (groups: ModelCollection<Group>) => {
	return filterGroupsByRole(groups, ROLE.GROUP_LEARNER)
}

export const groupsAsOwner = (groups: ModelCollection<Group>) => {
	return filterGroupsByRole(groups, ROLE.GROUP_OWNER)
}

export const groupsAsGrader = (groups: ModelCollection<Group>) => {
	return filterGroupsByRole(groups, ROLE.GROUP_GRADER)
}

export const groupsAsAnythingButLearner = (groups: ModelCollection<Group>) => {
	return filterGroupsByRoleOtherThanRole(groups, ROLE.GROUP_LEARNER)
}

const groupHasRole = (value: Group | Metadata | null | undefined, key: string) =>
	key !== '_metadata' && !!value && !!(value as Group).id && !!(value as Group).roles

const filterGroupsByRole = (groups: ModelCollection<Group>, role: string) => {
	return _.values(
		_.omitBy(getModelArray(groups), (value, key) => !groupHasRole(value, key) || !_.includes(value.roles, role))
	)
}

const filterGroupsByRoleOtherThanRole = (groups: ModelCollection<Group>, role: string) => {
	return _.values(
		_.omitBy(
			getModelArray(groups),
			(value, key) => !groupHasRole(value, key) || _.every(value.roles, r => r === role)
		)
	)
}

export const filterManualGroupUsers = (groupUsers: GroupUserRole[]) => {
	return groupUsers.filter(groupUser => !groupUser.isExternal)
}

export const filterSyncedGroupUsers = (groupUsers: GroupUserRole[]) => {
	return groupUsers.filter(groupUser => groupUser.isExternal)
}

export default {
	groupsAsAnythingButLearner,
	groupsAsOwner,
	groupsAsGrader,
	groupsAsLearner
}
