import ROLE from '../constants/role'
import { textForRole } from './role'

describe('textForRole', () => {
	it('returns correct value', () => {
		expect(textForRole(ROLE.ADMIN)).toEqual('Admin')
	})
})
