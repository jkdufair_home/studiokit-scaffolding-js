import { hasPrecisionAndScale } from './number'

describe('hasPrecisionAndScale', () => {
	test('should return false with invalid value', () => expect(hasPrecisionAndScale('foo', 1, 1)).toBeFalsy())

	test('should return false with insufficient precision', () =>
		expect(hasPrecisionAndScale('10.0', 1, 1)).toBeFalsy())

	test('should return false with insufficient scale', () => expect(hasPrecisionAndScale('1.11', 3, 1)).toBeFalsy())

	test('should return true, value 0, precision 1, scale 0', () =>
		expect(hasPrecisionAndScale('0', 1, 0)).toBeTruthy())

	test('should return false, value 0, precision 0, scale 1', () =>
		expect(hasPrecisionAndScale('0', 0, 1)).toBeFalsy())

	test('should return false, value 0.0, precision 0, scale 1', () =>
		expect(hasPrecisionAndScale('0.0', 0, 1)).toBeFalsy())

	test('should return true, value 111.11, precision 5, scale 2', () =>
		expect(hasPrecisionAndScale('111.11', 5, 2)).toBeTruthy())

	test('should return false, value 111.111, precision 5, scale 2', () =>
		expect(hasPrecisionAndScale('111.111', 5, 2)).toBeFalsy())

	test('should return true with 16 nines and 2 nines decimal, precision 18, scale 2', () =>
		expect(hasPrecisionAndScale('9999999999999999.99', 18, 2)).toBeTruthy())

	test('should return false with 17 nines and 2 nines decimal, precision 18, scale 2', () =>
		expect(hasPrecisionAndScale('99999999999999999.99', 18, 2)).toBeFalsy())

	test('should return true for -100, precision 3 scale 0', () =>
		expect(hasPrecisionAndScale('-100', 3, 0)).toBeTruthy())

	test('should return true for -100.01, precision 5 scale 2', () =>
		expect(hasPrecisionAndScale('-100.01', 5, 2)).toBeTruthy())

	test('should return false for -100.001, precision 5 scale 2', () =>
		expect(hasPrecisionAndScale('-100.001', 5, 2)).toBeFalsy())
})
