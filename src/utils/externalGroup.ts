import _ from 'lodash'
import EXTERNAL_PROVIDER_TYPES from '../constants/externalProviderType'
import { ExternalGroup, ExternalProvider } from '../types/External'

export const hasAnyUniTimeExternalGroup = (externalGroups: ExternalGroup[], externalProviders: ExternalProvider[]) => {
	const uniTimeExternalProvider = _.find(externalProviders, ep => ep.typename === EXTERNAL_PROVIDER_TYPES.UNITIME)

	if (!uniTimeExternalProvider) {
		return false
	}

	return _.some(externalGroups, eg => eg.externalProviderId === uniTimeExternalProvider.id)
}

export const hasAnyLtiExternalGroup = (externalGroups: ExternalGroup[], externalProviders: ExternalProvider[]) => {
	const ltiExternalProvider = _.find(externalProviders, ep => ep.typename === EXTERNAL_PROVIDER_TYPES.LTI)

	if (!ltiExternalProvider) {
		return false
	}

	return _.some(externalGroups, eg => eg.externalProviderId === ltiExternalProvider.id)
}

export const hasGradePushEnabled = (externalGroups: ExternalGroup[], externalProviders: ExternalProvider[]) => {
	const ltiExternalProvider = _.find(externalProviders, ep => ep.gradePushEnabled)

	if (!ltiExternalProvider) {
		return false
	}

	return _.some(externalGroups, eg => eg.externalProviderId === ltiExternalProvider.id && !!eg.gradesUrl)
}

export default {
	hasAnyUniTimeExternalGroup,
	hasAnyLtiExternalGroup,
	hasGradePushEnabled
}
