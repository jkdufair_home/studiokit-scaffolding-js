import { ExternalTerm } from '../types/External'
import { isNowBeforeDate } from './date'

export const filterByCurrentAndFuture = (externalTerms: ExternalTerm[], nowUtc = new Date().toISOString()) =>
	externalTerms.filter(et => isNowBeforeDate(et.endDate, nowUtc))

export default {
	filterByCurrentAndFuture
}
