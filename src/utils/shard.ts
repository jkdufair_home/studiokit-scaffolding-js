import _ from 'lodash'
import { getAppConfig } from '../constants/configuration'
import SHARD from '../constants/shard'
import windowService from '../services/windowService'

export function isPurdueShard() {
	return getShardKey() === SHARD.PURDUE
}

export function isDemoShard() {
	return getShardKey() === SHARD.DEMO
}

export function getShardKey(host?: string, localhostShardKey?: string) {
	const appConfig = getAppConfig()
	const location = windowService.getLocation()
	const finalHost = host || location.host
	const finalLocalhostShardKey = localhostShardKey || appConfig.LOCALHOST_SHARD_KEY
	const rootDomain = appConfig.ROOT_DOMAIN

	if (_.startsWith(host, 'localhost') && !!finalLocalhostShardKey) {
		return localhostShardKey
	}

	if (_.startsWith(host, 'localhost') || _.includes(host, 'azurewebsites.net') || _.includes(host, 'ngrok.io')) {
		return SHARD.PURDUE
	}

	const urlParts = finalHost.split('.')
	if (urlParts[0] === 'www' || urlParts[0] === 'dev' || (!!rootDomain && urlParts[0] === rootDomain.split('.')[0])) {
		return SHARD.ROOT
	}

	// get subdomain by removing root domain parts
	let shardKey = finalHost.replace(`.${rootDomain}`, '')
	// exclude `.dev` from shardKey
	if (shardKey.indexOf('.dev') === shardKey.length - 4) {
		shardKey = shardKey.replace('.dev', '')
	}
	return shardKey
}
