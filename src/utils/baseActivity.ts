import _ from 'lodash'
import { Model, ModelCollection } from 'studiokit-net-js'
import { BaseReduxState, UserInfo } from '../types'
import { getModelArray } from './model'

export interface ActivityOptions {
	entity?: Model & {
		activities?: string[]
	}
	entities?: ModelCollection
	userInfo?: UserInfo
}

/*
	The functions in this module all are designed as predicates to determine whether a
	user has been granted a set of activities to perform, either at an entity-specific
	level or at a global level.

	Because Javascript doesn’t have named parameters, we use an 'options object' to
	pass the needed items. By ensuring all these functions have the same signature,
	we can use them interchangeably in constructing the ActivityRequiredComponent.

	The options object has the following structure:
	{
		entities: {
			{
				1: {
						activities: [
							"activityName1",
							"activityName2"
							...
						]
					},
					...
			}
		},
		entity: {
				activities: [
					"activityName1",
					"activityName2"
					...
				]
		}
		userInfo: {
			activities: [
				"activityName1",
				"activityName2",
				...
			]
		}
	}

	`entity` is a single entity and `entities` is a set of entities to which a user has been granted a set of activities.
	While we could use the array of entities and eliminate the need for the entity property, the singular property will be
	used in most cases and it prevents to the need to stuff the entity in an array unnecessarily.

	`userInfo` contains a property with an array of activities the user has been granted at a global level
*/

/**
 * Return true if user has activity for the single entity
 *
 * @param {string} requiredActivity The activity required to return true
 * @param {object} options See above
 */
export const canPerformActivityOnEntity = (requiredActivity: string, options: ActivityOptions) => {
	if (!requiredActivity) {
		throw new Error('`requiredActivity` must be provided')
	}
	if (!options || !options.entity) {
		throw new Error('`options.entity` must be provided')
	}
	return _.includes(options.entity.activities, requiredActivity)
}

/**
 * Return true if user has activity on any of the entities
 *
 * @param {string} requiredActivity The activity required to return true
 * @param {object} options See above
 */
export const canPerformActivityOnSomeEntities = (requiredActivity: string, options: ActivityOptions) => {
	if (!requiredActivity) {
		throw new Error('`requiredActivity` must be provided')
	}
	if (!options || !options.entities) {
		throw new Error('`options.entities` must be provided')
	}
	return _.some(getModelArray(options.entities), entity => canPerformActivityOnEntity(requiredActivity, { entity }))
}

/**
 * Return true if user has activity globally
 *
 * This function can accept an null/undefined userInfo property for the
 * options argument. In which case it returns false. This is because userInfo
 * may not be loaded yet. If it is provided however, it must contain an
 * activities array
 *
 * @param {string} requiredActivity The activity required to return true
 * @param {object} options See above
 */
export const canPerformActivityGlobally = (requiredActivity: string, options: ActivityOptions) => {
	if (!requiredActivity) {
		throw new Error('`requiredActivity` must be provided')
	}
	if (
		!!options &&
		!!options.userInfo &&
		!!options.userInfo.activities &&
		!Array.isArray(options.userInfo.activities)
	) {
		throw new Error('`options.userInfo.activities` must be an array')
	}
	return (
		!!options &&
		!!options.userInfo &&
		!!options.userInfo.activities &&
		_.includes(options.userInfo.activities, requiredActivity)
	)
}

/**
 * Return true if user has activity either globally or on a single entity
 *
 * @param {string} requiredActivity The activity required to return true
 * @param {object} options See above
 */
export const canPerformActivityGloballyOrOnEntity = (requiredActivity: string, options: ActivityOptions) =>
	canPerformActivityGlobally(requiredActivity, options) || canPerformActivityOnEntity(requiredActivity, options)

/**
 * Return true if user has activity globally or on any of the entities provided
 *
 * @param {string} requiredActivity The activity required to return true
 * @param {object} options See above
 */
export const canPerformActivityGloballyOrOnSomeEntities = (requiredActivity: string, options: ActivityOptions) => {
	return (
		canPerformActivityGlobally(requiredActivity, options) ||
		canPerformActivityOnSomeEntities(requiredActivity, options)
	)
}

/**
 * Return an options object that can be used in the functions in this module. This
 * encapsulates the default logic of looking in state for a set of entities or
 * looking in props for a single entity.
 *
 * Override or decorate with app specific logic in your app if needed.
 *
 * @param {Object} state The redux state
 * @param {Object} [props] A props object (typically ownProps)
 * @param {string} [modelsProperty] The property used to locate the entity or entities
 * needed for entity-level activity grants. For sets of entities, this function
 * looks in `state.models`. For a single entity, it looks in `props`
 */
export const defaultOptions = (state: BaseReduxState, props?: any, modelsProperty?: string): ActivityOptions => {
	// if implementing in app, override / decorate with app specific logic
	return {
		entity: modelsProperty ? props && props[modelsProperty] : undefined,
		entities: modelsProperty ? (state.models[modelsProperty] as ModelCollection) : undefined,
		userInfo: state.models.user && state.models.user.userInfo
	}
}
