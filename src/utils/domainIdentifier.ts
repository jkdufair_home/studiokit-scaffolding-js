import { isPurdueShard } from './shard'

export const getDomainIdentifierTypeString = () =>
	isPurdueShard() ? 'Purdue career account alias or PUID' : 'Email Address'

export const getDomainIdentifierTypePluralString = () =>
	isPurdueShard() ? 'Purdue career account aliases or PUIDs' : 'Email Addresses'
