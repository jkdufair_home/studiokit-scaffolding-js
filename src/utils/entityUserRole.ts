export const getRootModelName = (props: { modelName: string }) => props.modelName.replace('.entityUserRoles', '')

export default {
	getRootModelName
}
