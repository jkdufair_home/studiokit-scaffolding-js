import { sortByNames } from './sort'

describe('sortByNames', () => {
	const userA = {
		firstName: 'A',
		lastName: 'A',
		id: '1'
	}

	const userB = {
		firstName: 'B',
		lastName: 'B',
		id: '2'
	}

	const userALowerCase = {
		firstName: 'a',
		lastName: 'a',
		id: '3'
	}

	const userBLowerCase = {
		firstName: 'b',
		lastName: 'b',
		id: '4'
	}

	const userEmailA = {
		email: 'a@a.a',
		id: '5'
	}

	const userEmailB = {
		email: 'b@b.b',
		id: '6'
	}

	describe('Same case, both have names', () => {
		it('should swap if both users have a last name and the first sorts before', () => {
			expect(sortByNames(userA, userB)).toBe(-1)
		})

		it('should not swap if both users have a last name and the first sorts after', () => {
			expect(sortByNames(userB, userA)).toBe(1)
		})

		it('should do nothing by name if both users have a last name that are equal', () => {
			expect(sortByNames(userA, userA)).toBe(0)
		})
	})

	describe('Varying case, both have names', () => {
		it('should swap if both users have a last name and the first sorts before', () => {
			expect(sortByNames(userALowerCase, userB)).toBe(-1)
		})

		it('should not swap if both users have a last name and the first sorts after', () => {
			expect(sortByNames(userBLowerCase, userA)).toBe(1)
		})

		it('should do nothing by name if both users have a last name that are equal', () => {
			expect(sortByNames(userALowerCase, userA)).toBe(0)
		})
	})

	describe('Email and name', () => {
		it('should swap if one has name and one has email and the first sorts before', () => {
			expect(sortByNames(userA, userEmailB)).toBe(-1)
		})

		it('should not swap if one has name and one has email and the first sorts after', () => {
			expect(sortByNames(userB, userEmailA)).toBe(1)
		})
	})

	describe('All email', () => {
		it('should swap if both users have only email addresses and the first sorts before', () => {
			expect(sortByNames(userEmailA, userEmailB)).toBe(-1)
		})

		it('should not swap if both users have only email addresses and the first sorts after', () => {
			expect(sortByNames(userEmailB, userEmailA)).toBe(1)
		})

		it('should do nothing by name if both users have only email addresses that are equal', () => {
			expect(sortByNames(userEmailA, userEmailA)).toBe(0)
		})
	})
})
