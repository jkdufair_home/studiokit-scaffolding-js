import { Dictionary } from 'lodash'
import StudioKit from 'studiokit-caliper-js'
import { OAuthToken } from 'studiokit-net-js'
import { getAppConfig, getShardConfig } from '../constants/configuration'
import { Configuration, UserInfo } from '../types'

//#region Private Variables + Configuration

let service: any // TODO: create types for studiokit-caliper-js
let caliperToken: OAuthToken

//#endregion Private Variables + Configuration

//#region Private Methods

function getToken() {
	return new Promise(resolve => {
		resolve({
			accessToken: caliperToken.access_token,
			expires: caliperToken['.expires']
		})
	})
}

//#endregion Private Methods

//#region Public Methods

export function setToken(token: OAuthToken) {
	caliperToken = token
}

export function initializeCaliperService(shardConfig: Configuration) {
	const appConfig = getAppConfig()
	const sensorOptions = Object.assign({}, appConfig.CALIPER_EVENT_STORE_OPTIONS, {
		hostname: shardConfig.caliperEventStoreHostname
	})
	const options = {
		sensorId: appConfig.CALIPER_SENSOR_ID,
		sensorOptions,
		appId: appConfig.CALIPER_SENSOR_APP_ID,
		appName: appConfig.CALIPER_SENSOR_APP_NAME,
		getToken
	}
	service = new StudioKit.BrowserCaliperService(options)
}

export function setCaliperPerson(userData: UserInfo) {
	const appConfig = getAppConfig()
	const shardConfig = getShardConfig()
	const personNamespace = shardConfig.caliperPersonNamespace
	const id = `${personNamespace}${userData.puid}`
	const extensions: Dictionary<any> = {}
	extensions[`${appConfig.CALIPER_EXTENSIONS_NAMESPACE}.userId`] = userData.id
	service.setPerson(id, userData.firstName, userData.lastName, extensions)
	return Promise.resolve()
}

export function startCaliperSession() {
	const appConfig = getAppConfig()
	const extensions: Dictionary<any> = {}
	extensions[`${appConfig.CALIPER_EXTENSIONS_NAMESPACE}.userAgent`] = navigator.userAgent
	service.startSession(extensions)
	return Promise.resolve()
}

export function endCaliperSession() {
	service.endSession()
	return Promise.resolve()
}

//#endregion Public Methods
