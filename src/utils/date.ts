import moment from 'moment-timezone'
import { getDefaultTimeZoneId, guessTimeZoneId } from './timezone'

export const getZonedMomentFromUtc = (dateTimeUtc = new Date().toISOString(), timeZoneId = guessTimeZoneId()) => {
	return moment.utc(dateTimeUtc).tz(timeZoneId)
}

export const getZonedMomentFromUtcInDefaultZone = (dateTimeUtc = new Date().toISOString()) => {
	const timeZoneId = getDefaultTimeZoneId()
	return getZonedMomentFromUtc(dateTimeUtc, timeZoneId)
}

export const getZonedMomentFromLocalDateTime = (localDateTime: Date | string, timeZoneId = guessTimeZoneId()) => {
	const localMoment = moment(localDateTime)
	const zonedMoment = moment.tz(
		[
			localMoment.year(),
			localMoment.month(),
			localMoment.date(),
			localMoment.hour(),
			localMoment.minute(),
			localMoment.second(),
			localMoment.millisecond()
		],
		timeZoneId
	)
	return zonedMoment
}

export const getLocalMomentFromUtc = (dateTimeUtc = new Date().toISOString(), timeZoneId = guessTimeZoneId()) => {
	const zonedMoment = getZonedMomentFromUtc(dateTimeUtc, timeZoneId)
	const localMoment = moment([
		zonedMoment.year(),
		zonedMoment.month(),
		zonedMoment.date(),
		zonedMoment.hour(),
		zonedMoment.minute(),
		zonedMoment.second(),
		zonedMoment.millisecond()
	])
	return localMoment
}

export const getLocalDateTimeFromUtc = (dateTimeUtc: string) => {
	if (!dateTimeUtc) {
		return null
	}
	return getLocalMomentFromUtc(dateTimeUtc).toDate()
}

export const getDateMinusTime = (dateTime: Date) => {
	return new Date(dateTime.getFullYear(), dateTime.getMonth(), dateTime.getDate())
}

export function getEndOfDay(dateTimeUtc: string) {
	return moment(dateTimeUtc)
		.startOf('day')
		.add(1, 'days')
		.subtract(3, 'milliseconds') // must be minus 3 for SQL date
		.toDate()
}

export function getEndOfMinute(dateTimeUtc: string) {
	return moment(dateTimeUtc)
		.startOf('minute')
		.add(1, 'minute')
		.subtract(3, 'milliseconds') // must be minus 3 for SQL date
		.toDate()
}

/**
 * Is the current time, "now", **after or equal to** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **after or equal to** `dateTimeUtc`
 */
export function isNowEqualOrAfterDate(dateTimeUtc: string, nowUtc = new Date().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment >= adjustedDateTimeMoment
}

/**
 * Is the current time, "now", **after** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **after** `dateTimeUtc`.
 */
export function isNowAfterDate(dateTimeUtc: string, nowUtc = new Date().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment > adjustedDateTimeMoment
}

/**
 * Is the current time, "now", **before** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **before** `dateTimeUtc`.
 */
export function isNowBeforeDate(dateTimeUtc: string, nowUtc = new Date().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment < adjustedDateTimeMoment
}

// March 05, 2018 @ 4:05pm EDT
export function getFormattedFullDateAndTime(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'MMMM DD, YYYY @ h:mma z')
}

// Mon, 3/5/2018 @ 4:05pm EDT
export function getFormattedFullDateWithWeek(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'ddd, M/D/YYYY @ h:mma z')
}

// 3/5/18 4:05pm EDT
export function getFormattedNumberedDateAndTime(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D/YY h:mma z')
}

// 3/5 4:05pm EDT
export function getFormattedNumberedDateAndTimeWithoutYear(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D h:mma z')
}

// Mar 05, 2018
export function getFormattedSimplifiedDate(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'MMM DD, YYYY')
}

// 3/5/18
export function getFormattedNumberedDate(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D/YY')
}

// 3/5/2018
export function getFormattedNumberedDateWithFullYear(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D/YYYY')
}

export function getFormattedNumberedDateForInput(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'YYYY-MM-DD')
}

// 3/5
export function getFormattedNumberedDateWithoutYear(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D')
}

// 4:05pm EDT
export function getFormattedNumberedTime(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'h:mma z')
}

// 4:05pm
export function getFormattedNumberedTimeWithoutZone(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'h:mma')
}

// 16:05
export function getFormattedNumberedTimeWithoutZoneOrMeridian(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'HH:mm')
}

// EDT
export function getFormattedTimeZone(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'z')
}

/**
 * Formats a date using the given format. Parses the date as UTC, then converts to the current time zone, then formats.
 * @param {*} dateTimeUtc The date to format. Any valid input for `moment.utc(...)`
 * @param {*} format The format to use
 */
function applyFormat(dateTimeUtc: string, format: string) {
	return getZonedMomentFromUtc(dateTimeUtc).format(format)
}

const isInputTypeSupported = (type: string) => {
	try {
		const test = document.createElement('input')
		test.type = type
		const isSupported = test.type === type
		return isSupported
	} catch (e) {
		return false
	}
}

export const isDateInputSupported = isInputTypeSupported('date')

export const isTimeInputSupported = isInputTypeSupported('time')

export default {
	getZonedMomentFromUtc,
	getZonedMomentFromUtcInDefaultZone,
	getZonedMomentFromLocalDateTime,
	getLocalMomentFromUtc,
	getLocalDateTimeFromUtc,
	getDateMinusTime,
	// comparison
	isNowEqualOrAfterDate,
	isNowAfterDate,
	isNowBeforeDate,
	// formats
	getFormattedFullDateAndTime,
	getFormattedFullDateWithWeek,
	getFormattedNumberedDateAndTime,
	getFormattedSimplifiedDate,
	getFormattedNumberedDate,
	getFormattedNumberedDateWithFullYear,
	getFormattedNumberedDateForInput,
	getFormattedNumberedDateWithoutYear,
	getFormattedNumberedTime,
	getFormattedTimeZone,
	getFormattedNumberedTimeWithoutZone,
	getFormattedNumberedTimeWithoutZoneOrMeridian,
	getFormattedNumberedDateAndTimeWithoutYear,
	// support
	isDateInputSupported,
	isTimeInputSupported
}
