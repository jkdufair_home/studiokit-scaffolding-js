import ROLE from '../constants/role'

export const textForRole = (role: string) => {
	switch (role) {
		case ROLE.SUPER_ADMIN:
			return 'Super Admin'
		case ROLE.ADMIN:
			return 'Admin'
		case ROLE.GROUP_LEARNER:
			return 'Student'
		case ROLE.GROUP_OWNER:
			return 'Instructor'
		case ROLE.GROUP_GRADER:
			return 'Grader'
		default:
			throw new Error('role was not found in switch statement')
	}
}
