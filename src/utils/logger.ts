import { getAppConfig } from '../constants/configuration'

function createLogger() {
	if (getAppConfig().NODE_ENV === 'production' || !window.console || !console) {
		const falseLog = () => {
			return false
		}
		return {
			debug: falseLog,
			info: falseLog,
			warn: falseLog,
			error: falseLog
		}
	}
	return {
		debug: (...params: any) => {
			console.debug(...params)
		},
		info: (...params: any) => {
			console.info(...params)
		},
		warn: (...params: any) => {
			console.warn(...params)
		},
		error: (...params: any) => {
			console.error(...params)
		}
	}
}

export const logger = createLogger()
