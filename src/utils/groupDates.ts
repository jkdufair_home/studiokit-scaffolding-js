import _ from 'lodash'
import { ModelCollection } from 'studiokit-net-js'
import { ExternalTerm } from '../types/External'
import { Group } from '../types/Group'
import { getFormattedNumberedDate } from './date'
import { getModelArray } from './model'

export const getTermOrDatesUIText = (externalTerms: ExternalTerm[], group: Group) => {
	// linked term
	const externalTerm = getTerm(externalTerms, group)
	if (!!externalTerm) {
		return externalTerm.name
	}
	// group dates
	if (!!group.startDate && !!group.endDate) {
		return `${getFormattedNumberedDate(group.startDate)} - ${getFormattedNumberedDate(group.endDate)}`
	}
	// no term or dates
	return 'N/A'
}

export const getTerm = (externalTerms: ExternalTerm[], group: Group) => {
	if (!!group.externalTermId && externalTerms.length > 0) {
		const externalTerm = _.find(externalTerms, t => t.id === group.externalTermId)
		return externalTerm
	}
	return null
}

export const getEndDate = (externalTerms: ExternalTerm[], group: Group) => {
	const externalTerm = getTerm(externalTerms, group)
	if (!!externalTerm) {
		return externalTerm.endDate
	}
	if (!!group.endDate) {
		return group.endDate
	}
	throw new Error('Group without externalTerm is missing endDate')
}

export const filterGroupsByEndDate = (
	groups: Group[],
	externalTerms: ModelCollection<ExternalTerm>,
	filterFunction: (dateTimeUtc: string, nowUtc?: string) => boolean
) => {
	return groups.filter(g => !!g.id && !g.isDeleted && filterFunction(getEndDate(getModelArray(externalTerms), g)))
}

export default {
	getTermOrDatesUIText
}
