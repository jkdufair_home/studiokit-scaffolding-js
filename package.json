{
	"name": "studiokit-scaffolding-js",
	"version": "0.1.3",
	"description": "Common scaffolding for Studio apps at Purdue",
	"repository": "https://gitlab.com/purdue-informatics/studiokit/studiokit-scaffolding-js",
	"license": "MIT",
	"main": "lib/index.js",
	"types": "lib/index.d.ts",
	"files": [
		"lib"
	],
	"scripts": {
		"tslint-check": "tslint-config-prettier-check ./tslint.json",
		"tslint-core": "tslint -c tslint.json -p tsconfig.json",
		"tslint-fix": "yarn tslint-core --fix",
		"lint:ts": "yarn tslint-core 'src/**/*.{ts,tsx}'",
		"lint:css": "stylelint \"src/**/*.css\"",
		"lint": "run-s lint:**",
		"fix:ts": "yarn lint:ts --fix",
		"fix:css": "yarn lint:css --fix",
		"fix": "run-s fix:**",
		"test": "jest",
		"test:ci": "cross-env CI=true yarn test",
		"coverage": "jest --coverage",
		"clean": "rimraf ./lib",
		"build": "yarn clean && tsc -p ./tsconfig.build.json",
		"build:watch": "tsc -p ./tsconfig.build.json -w",
		"prepublishOnly": "yarn build"
	},
	"dependencies": {
		"@material-ui/core": "^3.9.3",
		"@material-ui/icons": "^3.0.2",
		"@redux-saga/types": "^1.0.2",
		"@sentry/browser": "^5.0.7",
		"@types/applicationinsights-js": "^1.0.9",
		"@types/history": "^4.7.2",
		"@types/lodash": "^4.14.123",
		"@types/moment-timezone": "^0.5.12",
		"@types/pluralize": "^0.0.29",
		"@types/react": "^16.8.13",
		"@types/react-bootstrap": "^0.32.17",
		"@types/react-modal": "^3.8.1",
		"@types/react-redux": "7.x",
		"@types/react-router": "^4.4.5",
		"@types/react-router-bootstrap": "^0.24.5",
		"@types/react-router-dom": "^4.3.1",
		"@types/redux-logger": "^3.0.7",
		"@types/redux-sentry-middleware": "^0.1.0",
		"@types/uuid": "^3.4.4",
		"applicationinsights-js": "^1.0.20",
		"bootstrap": "^3.4.1",
		"connected-react-router": "^6.3.2",
		"history": "^4.9.0",
		"local-storage-fallback": "^4.1.1",
		"lodash": "^4.17.11",
		"moment-timezone": "^0.5.23",
		"pluralize": "^7.0.0",
		"prop-types": "^15.7.2",
		"query-string": "^6.4.2",
		"react": "^16.8.6",
		"react-bootstrap": "^0.32.4",
		"react-dom": "^16.8.6",
		"react-ga": "^2.5.7",
		"react-modal": "^3.8.1",
		"react-redux": "6.x",
		"react-router": "^5.0.0",
		"react-router-bootstrap": "^0.25.0",
		"react-router-dom": "^5.0.0",
		"redux": "^4.0.1",
		"redux-devtools-extension": "^2.13.8",
		"redux-logger": "^3.0.6",
		"redux-persist": "^5.10.0",
		"redux-saga": "^1.0.2",
		"redux-sentry-middleware": "^0.0.15",
		"studiokit-auth-js": "2.x",
		"studiokit-caliper-js": "^1.0.18",
		"studiokit-net-js": "3.x",
		"typescript": "^3.4.3",
		"uuid": "^3.3.2"
	},
	"devDependencies": {
		"@types/enzyme": "^3.9.1",
		"@types/enzyme-adapter-react-16": "^1.0.5",
		"@types/jest": "^24.0.11",
		"@types/mockdate": "^2.0.0",
		"codecov": "^3.3.0",
		"cross-env": "^5.2.0",
		"enzyme": "^3.9.0",
		"enzyme-adapter-react-16": "^1.12.1",
		"husky": "^1.3.1",
		"identity-obj-proxy": "^3.0.0",
		"jest": "^24.7.1",
		"jest-each": "^24.7.1",
		"lint-staged": "^8.1.5",
		"mockdate": "^2.0.2",
		"npm-run-all": "^4.1.5",
		"prettier": "^1.16.4",
		"rimraf": "^2.6.3",
		"stylelint": "^9.10.1",
		"stylelint-config-studiokit": "^1.0.1",
		"ts-jest": "^24.0.2",
		"tslint": "^5.15.0",
		"tslint-config-prettier": "^1.18.0",
		"tslint-plugin-prettier": "^2.0.1"
	},
	"prettier": {
		"parser": "typescript",
		"trailingComma": "none",
		"useTabs": true,
		"tabWidth": 4,
		"semi": false,
		"singleQuote": true,
		"printWidth": 120,
		"jsxBracketSameLine": true
	},
	"stylelint": {
		"extends": "stylelint-config-studiokit"
	},
	"husky": {
		"hooks": {
			"pre-commit": "lint-staged",
			"pre-push": "npm run test"
		}
	},
	"lint-staged": {
		"*.ts": [
			"yarn tslint-core --fix",
			"git add"
		],
		"*.tsx": [
			"yarn tslint-core --fix",
			"git add"
		],
		"*.css": [
			"stylelint --fix",
			"git add"
		]
	},
	"jest": {
		"transform": {
			"^.+\\.tsx?$": "ts-jest"
		},
		"collectCoverageFrom": [
			"src/**/*.{ts,tsx}"
		],
		"setupFiles": [
			"./src/setupTests.ts"
		],
		"moduleNameMapper": {
			"\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
			"\\.(css|less)$": "identity-obj-proxy"
		}
	}
}
